import { Component, OnInit } from '@angular/core';
import { CourseService } from 'src/app/services/course.service';
import { SubSink } from 'subsink';

@Component({
  selector: 'eplatform-all-reviews',
  templateUrl: './all-reviews.component.html',
  styleUrls: ['./all-reviews.component.scss']
})
export class AllReviewsComponent implements OnInit {

  private subs = new SubSink();
  public rates = [];
  public loading:boolean = true;

  public showLoadMoreBtn:boolean = false;
  private loadLimit = 6;
  public offset = 0;
  
  constructor(private courseService: CourseService) { }

  ngOnInit() {
    this.loadRates(this.offset, this.loadLimit);
  }

  loadRates(offset, limit) {
    this.subs.sink = this.courseService.getAllRates(offset, limit).subscribe((rates:any = []) => {
      if(this.rates.length > 0) {
        this.rates = [...this.rates, ...rates];
      } else {
        this.rates = rates;
      }
      this.showLoadMoreBtn = rates.length === this.loadLimit;
      this.loading = false;
    });
  }

  loadMore() {
    this.loading = true;
    this.loadRates(this.offset + this.loadLimit, this.loadLimit);
    this.offset = this.offset + this.loadLimit;
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
