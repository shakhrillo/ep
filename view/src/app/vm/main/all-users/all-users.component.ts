import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { UserService } from '../../../services/user.service';
import { selectUserProperty } from '../../../store/selectors/user.selector';
import { SubSink } from 'subsink';

@Component({
  selector: 'eplatform-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.scss']
})
export class AllUsersComponent implements OnInit {
  
  private subs = new SubSink();
  public users = [];
  public user;
  public userType;
  public loading:boolean = true;

  public showLoadMoreBtn:boolean = false;
  private loadLimit = 6;
  public offset = 0;
  
  constructor(
    private store: Store<any>,
    private userService: UserService,
  ) { }

  ngOnInit() {
    this.loadUsers(this.offset, this.loadLimit);

    this.subs.sink = this.store.pipe(select(selectUserProperty)).subscribe(user => {
      this.user = user;
    });
  }

  selectUserType(type?) {
    this.userType = type;
    this.loading = true;
    this.subs.unsubscribe();
    this.loadLimit = 10;
    this.offset = 0;
    this.users = [];
    this.loadUsers(0, 10);
  }

  loadUsers(offset, limit) {
    if(this.userType === 'authors') {
      this.subs.sink = this.userService.getAuthors(offset, limit).subscribe((users:any = []) => {
        if(this.users.length > 0) {
          this.users = [...this.users, ...users];
        } else {
          this.users = users;
        }
        this.showLoadMoreBtn = users.length === this.loadLimit;
        this.loading = false;
      });
    }
    if(!this.userType) {
      this.subs.sink = this.userService.getUsers(offset, limit).subscribe((users:any = []) => {
        if(this.users.length > 0) {
          this.users = [...this.users, ...users];
        } else {
          this.users = users;
        }
        this.showLoadMoreBtn = users.length === this.loadLimit;
        this.loading = false;
      });
    }
  }

  loadMore() {
    this.loading = true;
    this.loadUsers(this.offset + this.loadLimit, this.loadLimit);
    this.offset = this.offset + this.loadLimit;
  }

  deleted(uid) {
    this.users = this.users.filter(user => user.uid !== uid);
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
