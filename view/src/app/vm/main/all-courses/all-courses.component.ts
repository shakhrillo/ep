import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { SubSink } from 'subsink';
import { CourseService } from '../../../services/course.service';
import { selectUserProperty } from '../../../store/selectors/user.selector';

@Component({
  selector: 'eplatform-all-courses',
  templateUrl: './all-courses.component.html',
  styleUrls: ['./all-courses.component.scss']
})
export class AllCoursesComponent implements OnInit {
  
  private subs = new SubSink();
  public user;
  public courses = [];
  public categories = [];
  public loading:boolean = true;

  public showLoadMoreBtn:boolean = false;
  private loadLimit = 10;
  public offset = 0;

  public filterForm = new FormGroup({
    filter: new FormControl(null),
  });
  
  constructor(
    private store: Store<any>,
    private courseService: CourseService,
  ) { }

  ngOnInit() {
    this.loadCourses(this.offset, this.loadLimit);

    this.subs.sink = this.store.pipe(select(selectUserProperty)).subscribe(user => {
      this.user = user;
    });

    this.subs.sink = this.courseService.getCategories().subscribe((categories:any) => {
      this.categories = categories;
    });

  }

  selectedCategory;
  selectChange(e) {
    if(!this.selectedCategory && !e) return;
    this.loading = true;
    this.subs.unsubscribe();
    this.selectedCategory = e && e['id'];
    this.loadLimit = 10;
    this.offset = 0;
    this.courses = [];
    this.loadCourses(0, 10);
  }

  loadCourses(offset, limit) {
    this.filterForm.disable();
    if(this.selectedCategory) {
      this.subs.sink = this.courseService.getCategoryCourses(this.selectedCategory, offset, limit).subscribe((courses:any = []) => {
        if(this.courses.length > 0) {
          this.courses = [...this.courses, ...courses];
        } else {
          this.courses = courses;
        }
        this.showLoadMoreBtn = courses.length === this.loadLimit;
        this.loading = false;
        this.filterForm.enable();
      });
    } else {
      this.subs.sink = this.courseService.getAllCourses(offset, limit).subscribe((courses:any = []) => {
        if(this.courses.length > 0) {
          this.courses = [...this.courses, ...courses];
        } else {
          this.courses = courses;
        }
        this.showLoadMoreBtn = courses.length === this.loadLimit;
        this.loading = false;
        this.filterForm.enable();
      });
    }
  }

  loadMore() {
    this.loading = true;
    this.loadCourses(this.offset + this.loadLimit, this.loadLimit);
    this.offset = this.offset + this.loadLimit;
  }

  deleteCourse(id) {
    this.courses = this.courses.filter(course => course.id !== id);
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
