import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { select, Store } from '@ngrx/store';
import { UserService } from '../../../services/user.service';
import { GiveReviewComponent } from '../../../modals/give-review/give-review.component';
import { selectUserProperty } from '../../../store/selectors/user.selector';

@Component({
  selector: 'eplatform-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  public loading: boolean = true;
  public completed_courses = [];
  public in_progress_courses = [];

  public user;

  constructor(
    private userService: UserService,
    private modalService: NgbModal,
    private store: Store<any>
  ) { }

  ngOnInit() {
    this.subs.sink = this.userService.getLandingPage().subscribe(({completed_courses, in_progress_courses}: any) => {
      this.completed_courses = completed_courses.filter(e => e);
      this.in_progress_courses = in_progress_courses.filter(e => e);
      this.loading = false;
    });

    // GET USER
    this.subs.sink = this.store.pipe(select(selectUserProperty)).subscribe(user => {
      this.user = user;
    });
  }

  openReview(courseId) {
    const modalRef = this.modalService.open(GiveReviewComponent, {beforeDismiss: () => false});
    modalRef.componentInstance.courseId = courseId;
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
