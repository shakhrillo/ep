import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main.component';
import { CourseLandingComponent } from './course-landing/course-landing.component';
import { CourseVideosComponent } from './course-videos/course-videos.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { AllCoursesComponent } from './all-courses/all-courses.component';
import { AllReviewsComponent } from './all-reviews/all-reviews.component';
import { CategoryComponent } from './category/category.component';
import { AllUsersComponent } from './all-users/all-users.component';

const routes: Routes = [{
  path: '',
  component: MainComponent,
  children: [{
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  }, {
    path: 'home',
    component: LandingPageComponent,
    children: [{
      path: '',
      redirectTo: 'courses',
      pathMatch: 'full'
    }, {
      path: 'courses',
      component: AllCoursesComponent
    }, {
      path: 'users',
      component: AllUsersComponent
    }, {
      path: 'reviews',
      component: AllReviewsComponent
    }]
  }, {
    path: 'profile/:id',
    component: UserProfileComponent
  }, {
    path: 'course/:id',
    component: CourseLandingComponent
  }, {
    path: 'course/:courseId/:videoId',
    component: CourseVideosComponent
  }, {
    path: 'category/:category',
    component: CategoryComponent
  }, {
    path: 'terms-and-conditions',
    component: TermsComponent
  }, {
    path: 'privacy-policy',
    component: PrivacyComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }