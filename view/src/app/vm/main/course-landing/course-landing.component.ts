import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SubSink } from 'subsink';
import { CourseService } from 'src/app/services/course.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'eplatform-course-landing',
  templateUrl: './course-landing.component.html',
  styleUrls: ['./course-landing.component.scss']
})
export class CourseLandingComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  public course;
  public loading: boolean = true;
  public error;
  public rates = [];

  public showLoadMoreBtn:boolean = false;
  private loadLimit = 3;
  public offset = 0;

  constructor(
    private courseService: CourseService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.subs.sink = this.activatedRoute.params.pipe(first()).subscribe(({id}) => {
      this.subs.sink = this.courseService.getCourse(id).subscribe(course => {
        this.course = course;
        this.loading = false;
        this.loadRates(this.offset, this.loadLimit);
      }, error => {
        this.error = error;
      })
    });


  }

  loadRates(offset, limit) {
    this.subs.sink = this.courseService.getCourseRates(this.course.id, offset, limit).subscribe((rates:any = []) => {
      if(this.rates.length > 0) {
        this.rates = [...this.rates, ...rates];
      } else {
        this.rates = rates;
      }
      this.showLoadMoreBtn = rates.length === this.loadLimit;
      this.loading = false;
    });
  }

  loadMore() {
    this.loading = true;
    this.loadRates(this.offset + this.loadLimit, this.loadLimit);
    this.offset = this.offset + this.loadLimit;
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
