import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CourseService } from 'src/app/services/course.service';
import { SubSink } from 'subsink';

@Component({
  selector: 'eplatform-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  public courses = [];
  public loading:boolean = true;
  public category = {};

  public showLoadMoreBtn:boolean = false;
  private loadLimit = 10;
  public offset = 0;
  
  constructor(
    private courseService: CourseService,
    private activatedRoute: ActivatedRoute
  ) { }

  private query;
  ngOnInit() {
    this.subs.sink = this.activatedRoute.params.subscribe(query => {
      this.query = query;
      this.loadCourses(this.query.category, this.offset, this.loadLimit);
    });
  }

  loadCourses(id, offset, limit) {
    this.subs.sink = this.courseService.getCategoryCourses(id, offset, limit).subscribe(({courses = [], category = {}}:any) => {
      this.category = category;
      if(this.courses.length > 0) {
        this.courses = [...this.courses, ...courses];
      } else {
        this.courses = courses;
      }
      this.showLoadMoreBtn = courses.length === this.loadLimit;
      this.loading = false;
    });
  }

  loadMore() {
    this.loading = true;
    this.loadCourses(this.query.category, this.offset + this.loadLimit, this.loadLimit);
    this.offset = this.offset + this.loadLimit;
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
