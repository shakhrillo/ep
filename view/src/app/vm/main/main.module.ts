import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { MainRoutingModule } from './main.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { CourseLandingComponent } from './course-landing/course-landing.component';
import { CourseVideosComponent } from './course-videos/course-videos.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { AllCoursesComponent } from './all-courses/all-courses.component';
import { AllReviewsComponent } from './all-reviews/all-reviews.component';
import { CategoryComponent } from './category/category.component';
import { AllUsersComponent } from './all-users/all-users.component';

@NgModule({
  declarations: [
    MainComponent, 
    CourseLandingComponent, 
    CourseVideosComponent, 
    LandingPageComponent, 
    UserProfileComponent, 
    TermsComponent, 
    PrivacyComponent, 
    AllCoursesComponent, 
    AllReviewsComponent, 
    CategoryComponent, 
    AllUsersComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    SharedModule
  ]
})
export class MainModule { }
