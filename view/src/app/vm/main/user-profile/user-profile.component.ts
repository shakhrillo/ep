import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SubSink } from 'subsink';
import { select, Store } from '@ngrx/store';
import { CourseService } from '../../../services/course.service';
import { UserService } from '../../../services/user.service';
import { selectUserProperty } from '../../../store/selectors/user.selector';

@Component({
  selector: 'eplatform-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  public courses = [];
  public loading:boolean = true;
  public user;
  public currentUser;

  public showLoadMoreBtn:boolean = false;
  private loadLimit = 9;
  public offset = 0;
  
  constructor(
    private activatedRoute: ActivatedRoute,
    private courseService: CourseService,
    private userService: UserService,
    private store: Store<any>,
  ) { }

  private userUID;
  ngOnInit() {
    this.subs.sink = this.activatedRoute.params.subscribe(({id}) => {
      this.showLoadMoreBtn = false;
      this.loadLimit = 9;
      this.offset = 0;
      this.loadCourses(id, this.offset, this.loadLimit);
      this.userService.getUser(id).subscribe(user => this.user = user);
      this.userUID = id;
    });

    this.subs.sink = this.store.pipe(select(selectUserProperty)).subscribe(user => {
      this.currentUser = user;
    });
  }

  loadCourses(uid, offset, limit) {
    this.subs.sink = this.courseService.getUserCourse(uid, offset, limit).subscribe((courses:any = []) => {
      if(this.courses.length > 0) {
        this.courses = [...this.courses, ...courses];
      } else {
        this.courses = courses;
      }
      this.showLoadMoreBtn = courses.length === this.loadLimit;
      this.loading = false;
    });
  }

  loadMore() {
    this.loading = true;
    this.loadCourses(this.userUID, this.offset + this.loadLimit, this.loadLimit);
    this.offset = this.offset + this.loadLimit;
  }

  deleteCourse(id) {
    this.courses = this.courses.filter(course => course.id !== id);
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
  
}
