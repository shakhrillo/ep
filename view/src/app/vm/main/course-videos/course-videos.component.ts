import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SubSink } from 'subsink';
import { Store, select } from '@ngrx/store';
import { CourseService } from 'src/app/services/course.service';
import { selectUserProperty } from 'src/app/store/selectors/user.selector';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
declare let vooAPI:any;

@Component({
  selector: 'eplatform-course-videos',
  templateUrl: './course-videos.component.html',
  styleUrls: ['./course-videos.component.scss']
})
export class CourseVideosComponent implements OnInit {

  @ViewChild('player') player: ElementRef;
  private subs = new SubSink();
  private subsCheck = new SubSink();
  public user;
  public lesson;
  public course;
  public loading: boolean = true;
  public author;

  constructor(
    private store: Store<any>,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private courseService: CourseService,
  ) { }

  public actions = {};
  public query;
  public playerTime: number = 0;

  private initLoaded:boolean = false;

  private initVideoId;
  private watchPlayerActive$ = new Subject();
  ngOnInit() {
    this.watchPlayerActive$.pipe(debounceTime(400)).subscribe(() => {
      this.videoEnded();
    });
    this.activatedRoute.params.subscribe(query => {
      this.query = query;

      if(!this.initLoaded) {
        this.subs.sink = this.courseService.getCourse(query.courseId).subscribe((course) => {
          this.course = course;
          this.lesson = this.course.lessons.filter(video => video.id === query.videoId)[0];
          this.loading = false;
          if(this.lesson.videoURL) {
            this.initVideoId = this.lesson.videoURL.split('publish/')[1];
            setTimeout(() => {
              this.watchPlayer(this.initVideoId);
            }, 200);
          }
          setTimeout(() => {
            (<any>document.getElementById('courseCompleted')).checked = this.lesson.completed;
          }, 300);
        }, error => {
          alert(error);
        });

        this.initLoaded = true;
      } else {
        this.lesson = this.course.lessons.filter(video => video.id === this.query.videoId)[0];
        if(this.initVideoId) {
          const newId = this.lesson.videoURL.split('publish/')[1];
          this.changeVideo(this.initVideoId, newId);
        }
        (<any>document.getElementById('courseCompleted')).checked = this.lesson.completed;
      }
    });

    this.subs.sink = this.store.pipe(select(selectUserProperty)).subscribe(user => {
      this.user = user;
    });
  }

  videoEnded() {
    const currentIndex = this.course.lessons.findIndex(video => video.id === this.query.videoId) + 1;
    if(!this.lesson['completed'] && !this.course.completed) {
      this.completeCourseLesson(true);
    }
    if(this.course.lessons.length > currentIndex) {
      this.router.navigate(['course/' + this.query.courseId + '/' + this.course.lessons[currentIndex]['id']]);
    }
  }

  watchPlayer(id) {
    document.addEventListener('vooPlayerReady', (e) => {
      vooAPI(id, 'play');
      vooAPI(id, 'onEnded', null, () => {
        this.watchPlayerActive$.next(true);
      });
    });
  }

  changeVideo(oldVideo, newVideo) {
    if(oldVideo && newVideo) {
      vooAPI(oldVideo, 'source', [newVideo, 0, 'play']);
    }
  }

  completeCourseLesson(status) {
    this.subsCheck.unsubscribe();
    this.loading = true;
    this.subsCheck.sink = this.courseService.completeCourseLesson(this.query.courseId, this.query.videoId, { status }).subscribe(() => {
      this.course.lessons = this.course.lessons.map(lesson => {
        if(lesson.id === this.query.videoId) lesson.completed = status;
        return lesson; 
      });
      this.loading = false;
    }, error => {
      alert(error);
      console.log(error);
      this.loading = false;
    });
    
  }

  setVideoTime(time: number) {
    const id = this.lesson.videoURL.split('publish/')[1];
    vooAPI(id, 'currentTime', [time + '']);
    window.scrollTo(0, 0);
  }
  
  ngOnDestroy() {
    this.subsCheck.unsubscribe();
    this.subs.unsubscribe();
  }

}
