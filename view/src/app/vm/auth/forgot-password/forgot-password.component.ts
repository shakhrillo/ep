import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { SubSink } from 'subsink';
import { Router } from '@angular/router';

@Component({
  selector: 'eplatform-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  public error;
  public loading: boolean;

  public forgotPasswordForm = new FormGroup({
    email: new FormControl('', Validators.email),
  });

  constructor(
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit() {}

  submit() {
    console.log('mr.jhon1327@gmail.com')
    this.forgotPasswordForm.disable();
    this.loading = true;
    this.authService.resetPassword(this.forgotPasswordForm.get('email').value).then(() => {
      alert('Reseting password sent');
      this.router.navigate(['auth/login']);
      this.loading = false;
      this.forgotPasswordForm.enable();
    }).catch(error => {
      this.error = error;
      this.loading = false;
      this.forgotPasswordForm.enable();
    });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
