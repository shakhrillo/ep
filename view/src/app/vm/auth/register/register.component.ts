import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'eplatform-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public error: string;
  public loading: boolean;

  public registerForm = new FormGroup({
    email: new FormControl('', Validators.email),
    password: new FormControl('', Validators.required),
    confirmPassword: new FormControl(''),
    terms: new FormControl(true, Validators.requiredTrue),
    privacy: new FormControl(true, [Validators.requiredTrue]),
  }, { validators: this.checkPasswords});

  checkPasswords(group: FormGroup) {
    let password = group.get('password').value;
    let confirmPassword = group.get('confirmPassword').value;

    return password === confirmPassword ? null : { notSame: true }     
  }

  constructor(
    private router: Router,
    private auth: AuthService
  ) { }

  ngOnInit() { }
  
  register() {
    this.loading = true;
    this.auth.register(this.registerForm.get('email').value, this.registerForm.get('password').value).then(() => {
      this.router.navigate(['']);
    }).catch((error) => {
      this.error = error;
      this.loading = false;
    });
  }

}
