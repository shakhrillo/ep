import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'eplatform-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  public error = null;
  public loading: boolean;

  public loginForm = new FormGroup({
    email: new FormControl('', Validators.email),
    password: new FormControl('', Validators.required),
  });

  constructor(
    private router: Router,
    private auth: AuthService
  ) { }

  ngOnInit() {}

  login() {
    this.loading = true;
    this.auth.login(this.loginForm.get('email').value, this.loginForm.get('password').value).then(() => {
      this.router.navigate(['']);
    }).catch((error) => {
      this.error = error;
      this.loading = false;
    });
  }

  ngOnDestroy() { }

}
