import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';
import { SubscriptionService } from 'src/app/services/subscription.service';

@Component({
  selector: 'eplatform-payment-history',
  templateUrl: './payment-history.component.html',
  styleUrls: ['./payment-history.component.scss']
})
export class PaymentHistoryComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  public invoices = [];
  public loading:boolean = true;

  public showLoadMoreBtn:boolean = false;
  private loadLimit = 9;
  public offset = 0;
  
  constructor(
    private subscriptionService: SubscriptionService
  ) { }

  ngOnInit() {
    this.loadCourses(this.offset, this.loadLimit);
  }

  loadCourses(offset, limit) {
    this.subs.sink = this.subscriptionService.getInvoices(offset, limit).subscribe((invoices:any = []) => {
      if(this.invoices.length > 0) {
        this.invoices = [...this.invoices, ...invoices];
      } else {
        this.invoices = invoices;
      }
      this.showLoadMoreBtn = invoices.length === this.loadLimit;
      this.loading = false;
    });
  }

  loadMore() {
    this.loading = true;
    this.loadCourses(this.offset + this.loadLimit, this.loadLimit);
    this.offset = this.offset + this.loadLimit;
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
