import { Component, OnInit } from '@angular/core';
import { CourseService } from 'src/app/services/course.service';
import { SubSink } from 'subsink';

@Component({
  selector: 'eplatform-inprogress-courses',
  templateUrl: './inprogress-courses.component.html',
  styleUrls: ['./inprogress-courses.component.scss']
})
export class InprogressCoursesComponent implements OnInit {

  private subs = new SubSink();
  public user;
  public courses = [];
  public loading:boolean = true;

  public showLoadMoreBtn:boolean = false;
  private loadLimit = 9;
  public offset = 0;
  
  constructor(
    private courseService: CourseService,
  ) { }

  ngOnInit() {
    this.loadCourses(this.offset, this.loadLimit);
  }

  loadCourses(offset, limit) {
    this.subs.sink = this.courseService.unCompletedCourses(offset, limit).subscribe((courses:any = []) => {
      if(this.courses.length > 0) {
        this.courses = [...this.courses, ...courses].filter(e => e);
      } else {
        this.courses = courses.filter(e => e);
      }
      this.showLoadMoreBtn = courses.length === this.loadLimit;
      this.loading = false;
    });
  }

  loadMore() {
    this.loading = true;
    this.loadCourses(this.offset + this.loadLimit, this.loadLimit);
    this.offset = this.offset + this.loadLimit;
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
