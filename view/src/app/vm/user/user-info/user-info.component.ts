import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { select, Store } from '@ngrx/store';
import { AuthService } from '../../../services/auth.service';
import { SubSink } from 'subsink';
import { selectUserProperty } from '../../../store/selectors/user.selector';
import { DeleteUserComponent } from '../modals/delete-user/delete-user.component';
import { UpdateEmailComponent } from '../modals/update-email/update-email.component';
import { UpdatePasswordComponent } from '../modals/update-password/update-password.component';
import { UpdateUserComponent } from '../modals/update-user/update-user.component';
import { Router } from '@angular/router';

@Component({
  selector: 'eplatform-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {

  private subs = new SubSink();
  public user;

  constructor(
    private router: Router,
    private modalService: NgbModal,
    private authService: AuthService,
    private store: Store<any>
  ) {}

  ngOnInit() {
    this.subs.sink = this.store.pipe(
      select(selectUserProperty)
    ).subscribe(user => {
      this.user = user;
    });

  }

  editPassword() {
    this.modalService.open(UpdatePasswordComponent);
  }

  editEmail() {
    this.modalService.open(UpdateEmailComponent);
  }

  editUser() {
    this.modalService.open(UpdateUserComponent);
  }

  deleteUser() {
    this.modalService.open(DeleteUserComponent);
  }

  async logOut() {
    await this.authService.logout();
    this.router.navigate(['auth']);
  }
}
