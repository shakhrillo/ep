import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GiveReviewComponent } from 'src/app/modals/give-review/give-review.component';
import { CourseService } from 'src/app/services/course.service';
import { SubSink } from 'subsink';

@Component({
  selector: 'eplatform-completed-courses',
  templateUrl: './completed-courses.component.html',
  styleUrls: ['./completed-courses.component.scss']
})
export class CompletedCoursesComponent implements OnInit {

  private subs = new SubSink();
  public user;
  public courses = [];
  public loading:boolean = true;

  public showLoadMoreBtn:boolean = false;
  private loadLimit = 9;
  public offset = 0;
  
  constructor(
    private courseService: CourseService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.loadCourses(this.offset, this.loadLimit);
  }

  loadCourses(offset, limit) {
    this.subs.sink = this.courseService.completedCourses(offset, limit).subscribe((courses:any = []) => {
      if(this.courses.length > 0) {
        this.courses = [...this.courses, ...courses].filter(e => e);
      } else {
        this.courses = courses.filter(e => e);
      }
      this.showLoadMoreBtn = courses.length === this.loadLimit;
      this.loading = false;
    });
  }

  loadMore() {
    this.loading = true;
    this.loadCourses(this.offset + this.loadLimit, this.loadLimit);
    this.offset = this.offset + this.loadLimit;
  }

  openReview(courseId) {
    const modalRef = this.modalService.open(GiveReviewComponent, {beforeDismiss: () => false});
    modalRef.componentInstance.courseId = courseId;
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
