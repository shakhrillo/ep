import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { select, Store } from '@ngrx/store';
import { selectUserProperty } from 'src/app/store/selectors/user.selector';
import { SubSink } from 'subsink';
import { RequestBecomeAuthorComponent } from '../modals/request-become-author/request-become-author.component';

@Component({
  selector: 'eplatform-become-author',
  templateUrl: './become-author.component.html',
  styleUrls: ['./become-author.component.scss']
})
export class BecomeAuthorComponent implements OnInit {

  public user;
  private subs = new SubSink();

  constructor(
    private store: Store<any>,
    private modalService: NgbModal,
  ) { }

  ngOnInit() {
    this.subs.sink = this.store.pipe(select(selectUserProperty)).subscribe(user => {
      this.user = user;
    });
  }

  openModal() {
    this.modalService.open(RequestBecomeAuthorComponent)
  }

}
