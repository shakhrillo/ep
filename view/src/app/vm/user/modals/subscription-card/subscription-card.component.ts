import { Component, AfterContentInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SubscriptionService } from '../../../../services/subscription.service';
import { environment } from '../../../../../environments/environment';
declare var Stripe:any;

@Component({
  selector: 'eplatform-subscription-card',
  templateUrl: './subscription-card.component.html',
  styleUrls: ['./subscription-card.component.scss']
})
export class SubscriptionCardComponent implements AfterContentInit {

  @Input() id;

  private stripe;
  public card;
  public error;
  public loading: boolean = false;
  
  constructor(
    public activeModal: NgbActiveModal,
    private subscriptionService: SubscriptionService
  ) { }

  ngAfterContentInit() {
    // ***********
    // INIT <STRIPE CARD>
    // ***********
    this.init();
  }

  init() {
    this.stripe = Stripe(environment.stripe.publishable_key);
    var elements = this.stripe.elements();
    this.card = elements.create('card');
    this.card.mount("#card-element");
  }

  // ***********
  // ADD CARD <DEFAULT SUBSCRIPTION PAYMENT METHOD>
  // ***********
  async add() {
    this.loading = true;
    this.error = null;
    try {
      const {paymentMethod, error} = await this.stripe.createPaymentMethod({
        type: 'card',
        card: this.card
      });
      paymentMethod && await this.subscriptionService.updateCard(this.id, {paymentMethod: paymentMethod.id}).toPromise();
      if(error) {
        console.log(error);
        this.error = error.message;
      } else {
        this.activeModal.close();
      }
      this.loading = false;
    } catch ({error}) {
      this.error = error.message;
      this.loading = false;
    }
  }
}
