import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { SubSink } from 'subsink';
import { Store, select } from '@ngrx/store';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../../../services/auth.service';
import { selectUserProperty } from '../../../../store/selectors/user.selector';

@Component({
  selector: 'eplatform-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.scss']
})
export class DeleteUserComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  public user;
  public loading: boolean = false;
  public error;

  // ***********
  // FORM <DELETE USER>
  // ***********
  public deleteUserForm = new FormGroup({
    password: new FormControl(''),
  });

  constructor(
    private store: Store<any>,
    private authService: AuthService,
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit() {
    this.subs.sink = this.store.pipe(select(selectUserProperty)).subscribe(user => user && (this.user = user));
  }

  // ***********
  // DELETE <USER>
  // ***********
  async deleteUser() {
    try {
      this.loading = true;
      const { user } = await this.authService.login(this.user.email, this.deleteUserForm.get('password').value)
      await user.delete();
      location.href = 'auth';
    } catch (error) {
      this.loading = false;
      this.error = error;
    }
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
