import { Component, OnDestroy, OnInit } from '@angular/core';
import { SubSink } from 'subsink';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../../../services/auth.service';
import { select, Store } from '@ngrx/store';
import { selectUserErrorProperty, selectUserLoadingProperty, selectUserProperty } from '../../../../store/selectors/user.selector';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'eplatform-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.scss']
})
export class UpdatePasswordComponent implements OnInit, OnDestroy {
  
  private subs = new SubSink();
  
  public loading: boolean;
  public error: any;
  public user;

  // ***********
  // FORM <USER PASSWORD>
  // ***********
  public userUpdatePasswordForm = new FormGroup({
    currentPassword: new FormControl('', Validators.required),
    newPassword: new FormControl('', Validators.required),
    confirmPassword: new FormControl(''),
  }, { validators: this.checkPasswords});

  checkPasswords(group: FormGroup) {
    let pass = group.get('newPassword').value;
    let confirmPassword = group.get('confirmPassword').value;
    return pass === confirmPassword ? null : { notSame: true }     
  }

  constructor(
    private store: Store<any>,
    private authService: AuthService,
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit() {
    this.subs.sink = this.store.pipe(select(selectUserProperty)).subscribe(user => user && (this.user = user));
  }

  // ***********
  // CHANGE PASSWORD <USER>
  // ***********
  async updatePassword() {
    this.error = null;
    this.loading = true;

    try {
      const { user } = await this.authService.login(this.user.email, this.userUpdatePasswordForm.get('currentPassword').value)
      await user.updatePassword(this.userUpdatePasswordForm.get('newPassword').value);
      this.loading = false;
      alert('Password updated!');
      this.activeModal.close();
      await this.authService.logout();
    } catch (error) {
      this.loading = false;
      this.error = error;
    }
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
