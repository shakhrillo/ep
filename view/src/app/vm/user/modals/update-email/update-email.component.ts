import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { Store, select } from '@ngrx/store';
import { SubSink } from 'subsink';
import { AuthService } from '../../../../services/auth.service';
import { selectUserLoadingProperty, selectUserErrorProperty, selectUserProperty } from '../../../../store/selectors/user.selector';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'eplatform-update-email',
  templateUrl: './update-email.component.html',
  styleUrls: ['./update-email.component.scss']
})
export class UpdateEmailComponent implements OnInit, OnDestroy {
  
  private subs = new SubSink();
  
  public loading: boolean;
  public error: any;
  public user;

  // ***********
  // FORM <USER EMAIL>
  // ***********
  public userUpdateEmailForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required),
  });

  constructor(
    private auth: AngularFireAuth,
    private store: Store<any>,
    private authService: AuthService,
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit() {
    this.subs.sink = this.store.pipe(select(selectUserProperty)).subscribe(user => user && (this.user = user));
  }

  // ***********
  // CHANGE EMAIL <USER>
  // ***********
  async updateEmail() {
    this.error = null;
    this.loading = true;

    try {
      const { user } = await this.authService.login(this.user.email, this.userUpdateEmailForm.get('password').value)
      await user.verifyBeforeUpdateEmail(this.userUpdateEmailForm.get('email').value);
      this.loading = false;
      alert('We send confirmation link to your email. Please confirm to update');
    } catch (error) {
      this.loading = false;
      this.error = error;
    }
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
