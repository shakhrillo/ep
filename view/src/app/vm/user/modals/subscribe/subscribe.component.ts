import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';
import { FormGroup, FormControl } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SubscriptionService } from '../../../../services/subscription.service';
import { selectUserProperty } from '../../../../store/selectors/user.selector';
import { environment } from '../../../../../environments/environment';
import { AdminService } from '../../../../services/admin.service';
declare var Stripe:any;

@Component({
  selector: 'eplatform-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.scss']
})
export class SubscribeComponent implements OnInit, OnDestroy {
  
  private subs = new SubSink();
  public user;
  public plans = [];
  public loading: boolean = true;
  public membership = [];
  public pro = {};

  // ***********
  // FORM <SUBSCRIPTION>
  // ***********
  public subscriptionForm = new FormGroup({
    yearly: new FormControl(true),
  });

  constructor(
    private store: Store<any>,
    private subscriptionService: SubscriptionService,
    public activeModal: NgbActiveModal,
    private adminService: AdminService
  ) { }

  ngOnInit() {

    // ***********
    // GET <USER>
    // ***********
    this.subs.sink = this.store.pipe(select(selectUserProperty)).subscribe(user => {
      this.user = user;
      // user && user.isPro && (this.router.navigate(['user/subscription']));
    });

    // ***********
    // GET <SUBSCRIPTION PLAN>
    // ***********
    this.subs.sink = this.subscriptionService.getPlans().subscribe((plans:any) => {
      this.plans = plans;

      plans.map(plan => {
        this.pro[plan.interval] = plan;
      });
      this.pro['discount'] = (((this.pro['month']['amount'] * 12) - this.pro['year']['amount']) / this.pro['year']['amount']) * 100;
    });

    this.loadMembership();
  }

  loadMembership() {
    this.subs.sink = this.adminService.getMembership().subscribe((membership: any = []) => {
      this.membership = membership; 
      this.membership.sort((a, b) => a.isPro - b.isPro);
      this.loading = false;
    });
  }

  // ***********
  // PAY <SUBSCRIPTION>
  // ***********
  async pay(id) {
    this.loading = true;
    const stripe = Stripe(environment.stripe.publishable_key);
    try {
      const { sessionId } = <any> await this.subscriptionService.createPaymentMethod({
        plan: id
      }).toPromise();
      await stripe.redirectToCheckout({ sessionId });
    } catch (error) {
      alert(error.message);
      this.loading = false;
      console.error(error);
    }
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}