import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { SubSink } from 'subsink';
import { Store, select } from '@ngrx/store';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { selectUserProperty } from '../../../../store/selectors/user.selector';
import { UserService } from '../../../../services/user.service';

@Component({
  selector: 'eplatform-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.scss']
})
export class UpdateUserComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  user;
  loading: boolean = false;
  error;

  // ***********
  // FORM <USER>
  // ***********
  public userEditForm = new FormGroup({
    photoURL: new FormControl(''),
    displayName: new FormControl(''),
    country: new FormControl(''),
    city: new FormControl(''),
    info: new FormControl(''),
  });

  constructor(
    public userService: UserService,
    public activeModal: NgbActiveModal,
    private store: Store<any>
  ) { }

  ngOnInit() {
    this.subs.sink = this.store.pipe(
      select(selectUserProperty)
    ).subscribe(user => {
      if(user) {
        this.user = user;
        const { displayName='', country = '', city = '', photoURL = '', info = '' } = user;
        this.userEditForm.setValue({displayName, country, city, photoURL, info});
      }
    });
  }

  private image;
  attach(event) {
    this.image = event.target.files[0];
  }

  submit() {
    this.userEditForm.disable();
    this.loading = true;
    let formData = new FormData();
    Object.keys(this.userEditForm.value).map(_ => formData.append(_, this.userEditForm.value[_] || ''));
    this.image && formData.append('image', this.image);
    this.subs.sink = this.userService.updateUser(formData).subscribe(() => {
      this.activeModal.close();
      this.loading = false;
      this.userEditForm.enable();
    }, error => {
      this.error = error;
      this.loading = false;
      this.userEditForm.enable();
    })
  }
  
  async deleteUserPhoto() {
    this.loading = true;
    this.userEditForm.disable();
    try {
      await this.userService.deleteUserPhoto().toPromise();
      this.loading = false;
      this.userEditForm.enable();
    } catch (error) {
      this.error = error;
      this.loading = false;
      this.userEditForm.enable();
    }
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
