import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { SubSink } from 'subsink';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SubscribeComponent } from './modals/subscribe/subscribe.component';
import { selectUserProperty } from '../../store/selectors/user.selector';
import { UpdateUserComponent } from './modals/update-user/update-user.component';

@Component({
  selector: 'eplatform-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  public user;

  constructor(
    private modalService: NgbModal,
    private store: Store<any>
  ) {}

  ngOnInit() {

    // GET USER
    this.subs.sink = this.store.pipe(select(selectUserProperty)).subscribe(user => {
      this.user = user;
    });
  }

  openSubscribe() {
    this.modalService.open(SubscribeComponent);
  }

  editUser() {
    this.modalService.open(UpdateUserComponent);
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
