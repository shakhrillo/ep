import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { SubSink } from 'subsink';
import { FormGroup, FormControl } from '@angular/forms';
import { selectUserProperty } from '../../../store/selectors/user.selector';
import { SubscriptionService } from '../../../services/subscription.service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../../../services/user.service';
import { SubscriptionCardComponent } from '../modals/subscription-card/subscription-card.component';

@Component({
  selector: 'eplatform-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.scss']
})
export class SubscriptionComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  public user;
  public error;
  public loading: boolean = true;
  public subscription;

  // ***********
  // FORM <YEARLY TOGGLE VALUE>
  // ***********
  public subscriptionForm = new FormGroup({
    yearly: new FormControl(true),
  });

  constructor(
    private store: Store<any>,
    private router: Router,
    private userService: UserService,
    private subscriptionService: SubscriptionService,
    private modalService: NgbModal
  ) { }

  currentSubscription;
  currentSubscriptionCard;
  
  ngOnInit() {
    // ***********
    // GET <USER>
    // ***********
    this.subs.sink = this.store.pipe(select(selectUserProperty)).subscribe(user => {
      this.user = user;
      user && !user.pro && (this.router.navigate(['user/subscribe']));
    });

    this.getSubscription();
  }
  
  getSubscription() {
    this.subs.sink = this.userService.getUserSubscription().subscribe(({subscription = [], paymenth_method = {}}:any) => {
      this.currentSubscription = subscription;
      this.currentSubscriptionCard = paymenth_method;
      this.loading = false;
    });
  }

  // ***********
  // UNSUBSCRIBE <ACTIVE SUBSCRIPTION>
  // ***********
  unsubscribe() {
    const unsubs = confirm('Do you really want to unsubscribe?');
    if(unsubs) {
      this.loading = true;
      this.subs.sink = this.subscriptionService.cancelSubscription(this.currentSubscription.id).subscribe(() => {
        this.currentSubscription.cancel_at_period_end = true;
        this.loading = false;
      }, error => {
        alert(error);
        this.loading = false;
        this.error = error;
      });
    }
  }

  // ***********
  // RE-ACTIVATE CANCELED SUBSCRIPTION <CANCELED SUBSCRIPTION>
  // ***********
  reActivateSubscribe() {
    this.loading = true;
    this.subs.sink = this.subscriptionService.reActivateSubscription(this.currentSubscription.id).subscribe(() => {
      this.currentSubscription.cancel_at_period_end = false;
      this.loading = false;
    }, error => {
      alert(error);
      this.loading = false;
      this.error = error;
    });
  }

  // ***********
  // OPEN MODAL TO UPDATE ATTACHED CARD <CURRENT SUBSCRIPTION CARD>
  // ***********
  addCard() {
    const modalRef = this.modalService.open(SubscriptionCardComponent);
    modalRef.componentInstance.id = this.currentSubscription.id;
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}