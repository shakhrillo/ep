import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SubSink } from 'subsink';
import { select, Store } from '@ngrx/store';
import { EditCourseComponent } from '../../../modals/edit-course/edit-course.component';
import { CourseService } from '../../../services/course.service';
import { selectUserProperty } from '../../../store/selectors/user.selector';

@Component({
  selector: 'eplatform-uploaded-courses',
  templateUrl: './uploaded-courses.component.html',
  styleUrls: ['./uploaded-courses.component.scss']
})
export class UploadedCoursesComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  public user;
  public courses = [];
  public loading:boolean = true;

  public showLoadMoreBtn:boolean = false;
  private loadLimit = 9;
  public offset = 0;
  
  constructor(
    private store: Store<any>,
    private modalService: NgbModal,
    private courseService: CourseService,
  ) { }

  ngOnInit() {
    this.loadCourses(this.offset, this.loadLimit);

    this.subs.sink = this.store.pipe(select(selectUserProperty)).subscribe(user => {
      this.user = user;
    });
  }

  loadCourses(offset, limit) {
    this.subs.sink = this.courseService.getUserCourse(localStorage.getItem('uid'), offset, limit).subscribe((courses:any = []) => {
      if(this.courses.length > 0) {
        this.courses = [...this.courses, ...courses];
      } else {
        this.courses = courses;
      }
      this.showLoadMoreBtn = courses.length === this.loadLimit;
      this.loading = false;
    });
  }

  loadMore() {
    this.loading = true;
    this.loadCourses(this.offset + this.loadLimit, this.loadLimit);
    this.offset = this.offset + this.loadLimit;
  }

  deleteCourse(id) {
    this.courses = this.courses.filter(course => course.id !== id);
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
