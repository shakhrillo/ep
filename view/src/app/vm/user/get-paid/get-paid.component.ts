import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { select, Store } from '@ngrx/store';
import { UserService } from 'src/app/services/user.service';
import { selectUserProperty } from 'src/app/store/selectors/user.selector';
import { SubSink } from 'subsink';

@Component({
  selector: 'eplatform-get-paid',
  templateUrl: './get-paid.component.html',
  styleUrls: ['./get-paid.component.scss']
})
export class GetPaidComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  public loading: boolean = true;
  public error;
  public user;
  balance = '0';

  public payoutAuthorForm = new FormGroup({
    payoutEmail: new FormControl('', [Validators.required, Validators.email]),
  });

  constructor(
    private store: Store<any>,
    private modalService: NgbModal,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.subs.sink = this.store.pipe(select(selectUserProperty)).subscribe(user => {
      this.user = user;
    });

    const statisticsId = new Date().getMonth() + '_' + new Date().getFullYear();
    this.userService.getEarning(statisticsId).subscribe(({earnings}:any) => {
      this.balance = (<Number>earnings).toFixed(2);
      this.loading = false;
    });
  }

  open(content) {
    this.modalService.open(content);
  }

  async submit() {
    this.payoutAuthorForm.disable();
    this.loading = true;
    try {
      await this.userService.updateUserPayoutEmail(this.payoutAuthorForm.value).toPromise();
      this.loading = false;
      this.payoutAuthorForm.enable();
      this.modalService.dismissAll();
    } catch (error) {
      this.error = error;
      this.loading = false;
      this.payoutAuthorForm.enable()
    }
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
