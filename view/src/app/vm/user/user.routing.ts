import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserComponent } from './user.component';
import { SubscriptionComponent } from './subscription/subscription.component';
import { GetPaidComponent } from './get-paid/get-paid.component';
import { PaymentHistoryComponent } from './payment-history/payment-history.component';
import { UploadedCoursesComponent } from './uploaded-courses/uploaded-courses.component';
import { PendingCoursesComponent } from './pending-courses/pending-courses.component';
import { BecomeAuthorComponent } from './become-author/become-author.component';
import { UserInfoComponent } from './user-info/user-info.component';
import { CompletedCoursesComponent } from './completed-courses/completed-courses.component';
import { InprogressCoursesComponent } from './inprogress-courses/inprogress-courses.component';

const routes: Routes = [{
  path: '',
  component: UserComponent,
  children: [{
    path: '',
    redirectTo: 'info',
    pathMatch: 'full'
  }, {
    path: 'info',
    component: UserInfoComponent
  }, {
    path: 'subscription',
    component: SubscriptionComponent
  }, {
    path: 'get-paid',
    component: GetPaidComponent
  }, {
    path: 'invoices',
    component: PaymentHistoryComponent
  }, {
    path: 'uploaded-courses',
    component: UploadedCoursesComponent
  }, {
    path: 'pending-courses',
    component: PendingCoursesComponent
  }, {
    path: 'become-author',
    component: BecomeAuthorComponent
  }, {
    path: 'completed-courses',
    component: CompletedCoursesComponent
  }, {
    path: 'inprogress-courses',
    component: InprogressCoursesComponent
  }]
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }