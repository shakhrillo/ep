// ANGULAR
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// ROUTING
import { UserRoutingModule } from './user.routing';

// SHARED MODULE
import { SharedModule } from '../../shared/shared.module';

// COMPONENTS
import { UserComponent } from './user.component';
import { SubscriptionComponent } from './subscription/subscription.component';
import { GetPaidComponent } from './get-paid/get-paid.component';
import { PaymentHistoryComponent } from './payment-history/payment-history.component';
import { UploadedCoursesComponent } from './uploaded-courses/uploaded-courses.component';
import { PendingCoursesComponent } from './pending-courses/pending-courses.component';
import { UserInfoComponent } from './user-info/user-info.component';
import { BecomeAuthorComponent } from './become-author/become-author.component';

// MODALS
import { UpdateUserComponent } from './modals/update-user/update-user.component';
import { UpdatePasswordComponent } from './modals/update-password/update-password.component';
import { DeleteUserComponent } from './modals/delete-user/delete-user.component';
import { UpdateEmailComponent } from './modals/update-email/update-email.component';
import { SubscribeComponent } from './modals/subscribe/subscribe.component';
import { RequestBecomeAuthorComponent } from './modals/request-become-author/request-become-author.component';
import { SubscriptionCardComponent } from './modals/subscription-card/subscription-card.component';
import { CompletedCoursesComponent } from './completed-courses/completed-courses.component';
import { InprogressCoursesComponent } from './inprogress-courses/inprogress-courses.component';

@NgModule({
  declarations: [UserComponent, UpdatePasswordComponent, DeleteUserComponent, SubscriptionComponent, UpdateUserComponent, GetPaidComponent, PaymentHistoryComponent, UploadedCoursesComponent, SubscribeComponent, SubscriptionCardComponent, PendingCoursesComponent, RequestBecomeAuthorComponent, UpdateEmailComponent, UserInfoComponent, BecomeAuthorComponent, CompletedCoursesComponent, InprogressCoursesComponent],
  imports: [
    CommonModule,
    SharedModule,
    UserRoutingModule
  ]
})
export class UserModule { }
