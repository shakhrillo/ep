import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { select, Store } from '@ngrx/store';
import { SubSink } from 'subsink';
import { selectUserProperty } from '../../../store/selectors/user.selector';
import { RequestBecomeAuthorComponent } from '../modals/request-become-author/request-become-author.component';

@Component({
  selector: 'eplatform-author-request',
  templateUrl: './author-request.component.html',
  styleUrls: ['./author-request.component.scss']
})
export class AuthorRequestComponent implements OnInit {
  public user;
  private subs = new SubSink();

  constructor(
    private store: Store<any>,
    private modalService: NgbModal,
  ) { }

  ngOnInit() {
    this.subs.sink = this.store.pipe(select(selectUserProperty)).subscribe(user => {
      this.user = user;
    });
  }

  openModal() {
    this.modalService.open(RequestBecomeAuthorComponent)
  }
}
