import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { PaymentComponent } from './payment/payment.component';
import { PaymentDueComponent } from './payment-due/payment-due.component';
import { CategoriesComponent } from './categories/categories.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthorsPendingComponent } from './authors-pending/authors-pending.component';
import { MembershipComponent } from './membership/membership.component';
import { CoursesPendingComponent } from './courses-pending/courses-pending.component';
import { AuthorsComponent } from './authors/authors.component';
import { UsersComponent } from './users/users.component';
import { CoursesComponent } from './courses/courses.component';
import { ReviewsComponent } from './reviews/reviews.component';
import { CommentsComponent } from './comments/comments.component';
import { CourseCreateComponent } from './course-create/course-create.component';
import { AuthorRequestComponent } from './author-request/author-request.component';

const routes: Routes = [{
  path: '',
  component: AdminComponent,
  children: [{
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  }, {
    path: 'dashboard',
    component: DashboardComponent
  }, {
    path: 'membership',
    component: MembershipComponent
  }, {
    path: 'courses',
    component: CoursesComponent
  }, {
    path: 'courses-pending',
    component: CoursesPendingComponent
  }, {
    path: 'course-create',
    component: CourseCreateComponent
  }, {
    path: 'users',
    component: UsersComponent
  }, {
    path: 'authors',
    component: AuthorsComponent
  }, {
    path: 'author-request',
    component: AuthorRequestComponent
  }, {
    path: 'authors-pending',
    component: AuthorsPendingComponent
  }, {
    path: 'payment',
    component: PaymentComponent
  }, {
    path: 'payment-due',
    component: PaymentDueComponent
  }, {
    path: 'categories',
    component: CategoriesComponent
  }, {
    path: 'statistics',
    component: StatisticsComponent
  }, {
    path: 'reviews',
    component: ReviewsComponent
  }, {
    path: 'comments',
    component: CommentsComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }