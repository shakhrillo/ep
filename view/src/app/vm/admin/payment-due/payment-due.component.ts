import { Component, OnInit } from '@angular/core';
import { AdminService } from 'src/app/services/admin.service';
import { Parser } from 'json2csv';
import { ColumnMode } from '@swimlane/ngx-datatable';
import {
  faDownload
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'eplatform-payment-due',
  templateUrl: './payment-due.component.html',
  styleUrls: ['./payment-due.component.scss']
})
export class PaymentDueComponent implements OnInit {

  public authorsEarnings = [];
  public loading: boolean = true;

  public faDownload = faDownload;
  public columnMode = ColumnMode;
  public rows = [];

  constructor(
    private adminService: AdminService
  ) {}

  ngOnInit() {
    const id = (new Date().getMonth() - 1) + '_' + new Date().getFullYear();
    this.adminService.getEarnings(id).subscribe((authorsEarnings:any) => {
      this.authorsEarnings = authorsEarnings;
      if(Array.isArray(authorsEarnings)) {
        this.authorsEarnings = authorsEarnings.map(earning => {
          earning['earnings'] = Number(earning['earnings']).toFixed(2);
          return earning;
        });

        this.rows = authorsEarnings.map(earning => {
          return {
            author: {
              author: earning['author'],
              displayName: earning['displayName'],
              email: earning['email']
            },
            earnings: earning['earnings']
          }
        });
      }
      this.loading = false;
    })
  }

  downloadCSV() {
    const json2csvParser = new Parser();
    const tsv = json2csvParser.parse(this.authorsEarnings);
   
    var a = document.createElement('a');
    var event = document.createEvent("HTMLEvents");
    event.initEvent("click");
    a.download = 'earnings.csv';
    a.href = 'data:text/csv;charset=UTF-8,' + encodeURIComponent(tsv);
    a.click()
  }
}
