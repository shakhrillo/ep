import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { SubSink } from 'subsink';
import { AdminService } from '../../../services/admin.service';

@Component({
  selector: 'eplatform-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent {

  // private subs = new SubSink();
  // public loading: boolean = false;
  // public products;
  // public plans;

  // public productForm = new FormGroup({
  //   name: new FormControl(''),
  //   description: new FormControl(''),
  // });
  // public planForm = new FormGroup({
  //   title: new FormControl(''),
  //   description: new FormControl(''),
  //   color: new FormControl(''),
  //   product: new FormControl(''),
  //   monthly_amount: new FormControl(''),
  //   monthly_currency: new FormControl(''),
  //   yearly_amount: new FormControl(''),
  //   yearly_currency: new FormControl(''),
  // });

  // constructor(
  //   private adminService: AdminService,
  //   private modalService: NgbModal
  // ) { }

  // ngOnInit() {
  //   this.subs.sink = this.adminService.getProducts().subscribe(({data}:any) => {
  //     this.products = data;
  //     this.productForm.setValue({
  //       name: this.products[0].name,
  //       description: this.products[0].description
  //     });

  //     this.planForm.setValue({
  //       title: '',
  //       description: '',
  //       color: 'success',
  //       product: this.products[0].id,
  //       monthly_amount: '',
  //       monthly_currency: 'usd',
  //       yearly_amount: '',
  //       yearly_currency: 'usd',
  //     });
  //   });

  //   this.subs.sink = this.adminService.getPlans().subscribe((plans) => {
  //     this.plans = plans;
  //   });
  // }

  // open(content, data?) {
  //   this.modalService.open(content);
  //   if(data) {
  //     let {plan, title, description, color} = data;
  //     this.planForm.setValue({
  //       title: title,
  //       description: description,
  //       color: color,
  //       product: plan.product,
  //       monthly_amount: '',
  //       monthly_currency: '',
  //       yearly_amount: '',
  //       yearly_currency: '',
  //     });
  //   }
  // }

  // async submitPlan() {
  //   this.loading = true;
  //   let data = new FormData();
  //   Object.keys(this.planForm.value).forEach((_:any) => {
  //     data.append(_, this.planForm.value[_]);
  //   });
  //   try {
  //     await this.adminService.createPlan(data).toPromise();
  //     location.reload();
  //   } catch (error) {
  //     console.error(error);
  //   }
  //   this.loading = false;
  // }

  // async deletePlan(id) {
  //   this.loading = true;
  //   await this.adminService.deletePlan(id).toPromise();
  //   location.reload();
  // }

  // async submitProduct() {
  //   this.loading = true;
  //   try {
  //     await this.adminService.updateProduct({id: this.products[0].id, data: this.productForm.value}).toPromise();
  //   } catch (error) {
  //     alert(error);
  //     console.error(error);
  //   }
  //   this.loading = false;
  // }

  // ngOnDestroy() {
  //   this.subs.unsubscribe();
  // }
}