import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { selectUserProperty } from 'src/app/store/selectors/user.selector';
import { SubSink } from 'subsink';
import { 
  faSmile,
  faAngleRight,
  faChartBar,
  faList,
  faMoneyBill,
  faLock,
  faUser,
  faUserPlus,
  faUserAstronaut,
  faPlus,
  faFolder,
  faFolderPlus,
  faComments,
  faStar,
  faEnvelope,
  faBell,
  faReceipt,
  faDollarSign,
  faMoneyBillWave,
  faUserCog,
  faUserNinja,
  faUsers,
  faAngleUp,
  faBars
} from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter, take } from 'rxjs/operators';

@Component({
  selector: 'eplatform-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  public faSmile = faSmile;
  public faAngleRight = faAngleRight;
  public faAngleUp = faAngleUp;
  public faChartBar = faChartBar;
  public faList = faList;
  public faMoneyBill = faMoneyBill;
  public faLock = faLock;
  public faUser = faUser;
  public faUserPlus = faUserPlus;
  public faUserAstronaut = faUserAstronaut;
  public faPlus = faPlus;
  public faFolder = faFolder;
  public faFolderPlus = faFolderPlus;
  public faComments = faComments;
  public faStar = faStar;
  public faEnvelope = faEnvelope;
  public faBell = faBell;
  public faReceipt = faReceipt;
  public faMoneyBillWave = faMoneyBillWave;
  public faDollarSign = faDollarSign;
  public faUserCog = faUserCog;
  public faUsers = faUsers;
  public faUserNinja = faUserNinja;
  public faBars = faBars;

  private subs = new SubSink();
  public user;
  public statistics;

  constructor(
    private router: Router,
    private store: Store<any>
  ) { }

  ngOnInit() {
    // ***********
    // GET <USER>
    // ***********
    this.subs.sink = this.store.pipe(select(selectUserProperty)).subscribe(user => {
      this.user = user;
    });
  }

}
