import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { SubSink } from 'subsink';
import { Router } from '@angular/router';
import { HttpEventType } from '@angular/common/http';
import { CourseService } from '../../../services/course.service';

@Component({
  selector: 'eplatform-course-create',
  templateUrl: './course-create.component.html',
  styleUrls: ['./course-create.component.scss']
})
export class CourseCreateComponent implements OnInit {

  private subs = new SubSink();
  public error;
  public loading;
  public categories = [];

  public tabIndex = 1;
  public activeIdForTranscription = 0;
  public activeIdForVideos = 0;

  // ***********
  // FORM <COURSE>
  // ***********
  public courseFormInfo = new FormGroup({
    title: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    imageBy: new FormControl(''),
  });

  public courseFormTopicAndTags = new FormGroup({
    topic: new FormControl([], Validators.required),
    tags: new FormControl('', Validators.required),
  });

  public courseFormVideos = new FormGroup({
    videos: new FormArray([], Validators.required)
  });


  public formVideos = [];
  get videos(): FormArray {
    return this.courseFormVideos.get('videos') as FormArray;
  }

  constructor(
    private router: Router,
    private courseService: CourseService,
  ) { }

  // ***********
  // ATTACH IMAGE, VIDEOS <COURSE>
  // ***********
  uploadedVideos = [];
  uploadVideo(event, index) {
    this.uploadedVideos.push(event.target.files[0]);
  }

  courseResource;
  uploadCourseResource(event) {
    this.courseResource = event.target.files;
  }

  courseImage;
  uploadcourseImage(event) {
    this.courseImage = event.target.files[0];
  }

  ngOnInit() {
    this.addFormVideos();

    this.subs.sink = this.courseService.getCategories().subscribe((categories:any) => {
      this.categories = categories;
    });

  }

  addFormVideos() {
    this.formVideos.push({
      transcripts: [{}]
    });
    this.videos.push(new FormGroup({
      videoURL: new FormControl(''),
      videoTitle: new FormControl('', Validators.required),
      videoDescription: new FormControl('', Validators.required),
      videoTranscript: new FormArray([
        new FormGroup({
          videoTranscriptSec: new FormControl('', Validators.required),
          videoTranscriptInfo: new FormControl('', Validators.required)
        })
      ], Validators.required)
    }));
    this.activeIdForVideos = this.formVideos.length - 1;
  }

  addFormVideosTranscript(index) {
    this.formVideos[index]['transcripts'].push({});

    const videos = this.courseFormVideos.get('videos') as FormArray;
    const videoTranscript = videos.at(index).get('videoTranscript') as FormArray;
    videoTranscript.push(new FormGroup({
      videoTranscriptSec: new FormControl('', Validators.required),
      videoTranscriptInfo: new FormControl('', Validators.required)
    }));

    this.activeIdForTranscription = this.formVideos[index]['transcripts'].length - 1;
  }

  removeFormVideos(index) {
    this.formVideos.splice(index, 1);
    this.uploadedVideos.splice(index, 1);
    this.videos.removeAt(index);
    this.activeIdForVideos = index;
  }

  removeFormVideosTranscript(index, i) {
    this.formVideos[index]['transcripts'].splice(i, 1);

    const videos = this.courseFormVideos.get('videos') as FormArray;
    const videoTranscript = videos.at(index).get('videoTranscript') as FormArray;
    videoTranscript.removeAt(i);

    this.activeIdForTranscription = i;
  }

  // ***********
  // PUBLISH <COURSE>
  // ***********
  public uploading = 0;
  submit() {
    this.uploading = 1;
    this.error = null;
    this.loading = true;
    this.courseFormInfo.disable();
    this.courseFormTopicAndTags.disable();
    this.courseFormVideos.disable();
    let data = new FormData();
    
    // IMAGE
    data.append('course_image', this.courseImage);
    
    // RESOURCES
    if(this.courseResource && Array.from(this.courseResource).length > 0) {
      Array.from(this.courseResource).forEach((resource:any) => {
        data.append('course_resource', resource);
      });
    }
    
    // VIDEOS
    if(this.uploadedVideos.length > 0) {
      this.uploadedVideos.map(video => {
        data.append('video_files', video);
      });
    }

    data.append('videos', JSON.stringify(this.courseFormVideos.get('videos').value));
    data.append('tags', JSON.stringify(this.courseFormTopicAndTags.get('tags').value));
    data.append('topic', this.courseFormTopicAndTags.get('topic').value);

    Object.keys(this.courseFormInfo.value).forEach((form_key) => {
      data.append(form_key, this.courseFormInfo.value[form_key]);
    });

    this.subs.sink = this.courseService.submitCourse(data).subscribe((res) => {
      if (res.type === HttpEventType.Response) {
        this.router.navigate(['/admin/courses']);
      }
      if (res.type === HttpEventType.UploadProgress) {
        const percentDone = Math.round(100 * res.loaded / res.total);
        this.uploading = percentDone;
      } 
    }, error => {
      this.courseFormInfo.enable();
      this.courseFormTopicAndTags.enable();
      this.courseFormVideos.enable();
      alert(error);
      this.error = error;
      this.loading = false;
      this.uploading = 0;
    })
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
