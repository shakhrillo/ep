import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../../../../services/user.service';

@Component({
  selector: 'eplatform-request-become-author',
  templateUrl: './request-become-author.component.html',
  styleUrls: ['./request-become-author.component.scss']
})
export class RequestBecomeAuthorComponent implements OnInit {

  public loading: boolean;
  public error;
  
  public userBecomeAuthorForm = new FormGroup({
    info: new FormControl(''),
  });

  constructor(
    private userService: UserService,
    public activeModal: NgbActiveModal
  ) { }

  public userIdentity;
  public taxDocument;
  upload($event, obj) {
    this[obj] = $event.target.files[0];
  }

  async submit() {
    this.error = null;
    this.userBecomeAuthorForm.disable();
    this.loading = true;

    try {
      let data = new FormData();
      data.append('userIdentity', this.userIdentity);
      data.append('taxDocument', this.taxDocument);
      data.append('info', this.userBecomeAuthorForm.get('info').value);
  
      await this.userService.becomeAuthor(data).toPromise();
      this.activeModal.close();
    } catch (error) {
      alert(error);
      this.userBecomeAuthorForm.enable();
      this.error = error;
      console.error(error);
    }
  }

  ngOnInit() { }

}
