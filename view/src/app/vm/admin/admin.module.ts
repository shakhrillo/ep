import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { SharedModule } from '../../shared/shared.module';
import { AdminRoutingModule } from './admin.routing';
import { PaymentComponent } from './payment/payment.component';
import { PaymentDueComponent } from './payment-due/payment-due.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { CategoriesComponent } from './categories/categories.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthorsPendingComponent } from './authors-pending/authors-pending.component';
import { MembershipComponent } from './membership/membership.component';
import { AuthorsComponent } from './authors/authors.component';
import { UsersComponent } from './users/users.component';
import { CoursesComponent } from './courses/courses.component';
import { CoursesPendingComponent } from './courses-pending/courses-pending.component';
import { ReviewsComponent } from './reviews/reviews.component';
import { CommentsComponent } from './comments/comments.component';
import { CourseCreateComponent } from './course-create/course-create.component';
import { MessagesComponent } from './messages/messages.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { UserComponent } from './user/user.component';
import { AuthorRequestComponent } from './author-request/author-request.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { SubscriptionComponent } from './subscription/subscription.component';
import { GetPaidComponent } from './get-paid/get-paid.component';

// MODALS
import { UpdateUserComponent } from './modals/update-user/update-user.component';
import { UpdatePasswordComponent } from './modals/update-password/update-password.component';
import { DeleteUserComponent } from './modals/delete-user/delete-user.component';
import { UpdateEmailComponent } from './modals/update-email/update-email.component';
import { SubscribeComponent } from './modals/subscribe/subscribe.component';
import { RequestBecomeAuthorComponent } from './modals/request-become-author/request-become-author.component';
import { SubscriptionCardComponent } from './modals/subscription-card/subscription-card.component';


@NgModule({
  declarations: [
    AdminComponent, 
    PaymentComponent, 
    PaymentDueComponent, 
    StatisticsComponent, 
    CategoriesComponent, 
    DashboardComponent, 
    AuthorsPendingComponent, 
    MembershipComponent, 
    AuthorsComponent, 
    UsersComponent, 
    CoursesComponent, 
    CoursesPendingComponent, 
    ReviewsComponent, 
    CommentsComponent, 
    CourseCreateComponent, 
    MessagesComponent, 
    NotificationsComponent, 
    UserComponent, 
    AuthorRequestComponent, 
    InvoicesComponent, 
    SubscriptionComponent, 
    GetPaidComponent,

    UpdateUserComponent,
    UpdatePasswordComponent,
    DeleteUserComponent,
    UpdateEmailComponent,
    SubscribeComponent,
    RequestBecomeAuthorComponent,
    SubscriptionCardComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule
  ]
})
export class AdminModule { }
