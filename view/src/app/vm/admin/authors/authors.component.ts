import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { SubSink } from 'subsink';
import { selectUserProperty } from '../../../store/selectors/user.selector';
import { AdminService } from '../../../services/admin.service';
import { ColumnMode } from '@swimlane/ngx-datatable';
import {
  faPlus,
  faCheck,
  faTimes
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'eplatform-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.scss']
})
export class AuthorsComponent implements OnInit {
  private subs = new SubSink();
  public users = [];
  public user;
  public loading:boolean = true;
  public columnMode = ColumnMode;

  public showLoadMoreBtn:boolean = false;
  private loadLimit = 6;
  public offset = 0;
  
  constructor(
    private store: Store<any>,
    private adminService: AdminService
  ) { }

  ngOnInit() {
    this.loadUsers(this.offset, this.loadLimit);

    this.subs.sink = this.store.pipe(select(selectUserProperty)).subscribe(user => {
      this.user = user;
    });
  }

  loadUsers(offset, limit) {
    this.subs.sink = this.adminService.getAuthors(offset, limit).subscribe((users:any = []) => {
      if(this.users.length > 0) {
        this.users = [...this.users, ...users];
      } else {
        this.users = users;
      }
      this.showLoadMoreBtn = users.length === this.loadLimit;
      this.loading = false;
    });
  }

  loadMore() {
    this.loading = true;
    this.loadUsers(this.offset + this.loadLimit, this.loadLimit);
    this.offset = this.offset + this.loadLimit;
  }

  deleted(uid) {
    this.users = this.users.filter(user => user.uid !== uid);
  }
  
  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
