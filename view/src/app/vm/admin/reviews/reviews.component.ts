import { Component, OnInit } from '@angular/core';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { SubSink } from 'subsink';
import { CourseService } from '../../../services/course.service';

@Component({
  selector: 'eplatform-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.scss']
})
export class ReviewsComponent implements OnInit {

  private subs = new SubSink();
  public rates = [];
  public loading:boolean = true;

  public columnMode = ColumnMode;

  public showLoadMoreBtn:boolean = false;
  private loadLimit = 6;
  public offset = 0;
  
  constructor(private courseService: CourseService) { }

  ngOnInit() {
    this.loadRates(this.offset, this.loadLimit);
  }

  loadRates(offset, limit) {
    this.subs.sink = this.courseService.getAllRates(offset, limit).subscribe((rates:any = []) => {
      if(this.rates.length > 0) {
        this.rates = [...this.rates, ...rates];
      } else {
        this.rates = rates;
      }
      this.showLoadMoreBtn = rates.length === this.loadLimit;
      this.loading = false;
    });
  }

  loadMore() {
    this.loading = true;
    this.loadRates(this.offset + this.loadLimit, this.loadLimit);
    this.offset = this.offset + this.loadLimit;
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
