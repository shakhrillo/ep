import { Component, OnInit } from '@angular/core';
import { AdminService } from 'src/app/services/admin.service';
import {
  faCalendar,
  faDollarSign,
  faList,
  faUsers,
  faUserAstronaut,
  faBookOpen,
  faEnvelope,
  faStar,
  faMoneyBill,
  faComments,
  faSms
} from '@fortawesome/free-solid-svg-icons';
import { curveCatmullRom } from 'd3-shape';

@Component({
  selector: 'eplatform-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit{
  faCalendar = faCalendar;
  faDollarSign = faDollarSign;
  faList = faList;
  faSms = faSms;
  faUsers = faUsers;
  faBookOpen = faBookOpen;
  faUserAstronaut = faUserAstronaut;
  curve = curveCatmullRom;
  faEnvelope = faEnvelope;
  faStar = faStar;
  faMoneyBill = faMoneyBill;
  faComments = faComments;

  private chartData = [{"name": "Jan"}, {"name": "Feb"}, {"name": "Mar"}, {"name": "Apr"},{"name": "May"}, {"name": "Jun"}, {"name": "Jul"}, {"name": "Aug"}, {"name": "Sep"}, {"name": "Oct"}, {"name": "Nov"}, {"name": "Dec"}]
  public registeredUsers = [];
  public earningsAmount = [];
  public becomePro = [];
  public watchedLessons = [];
  public acceptedCourses = [];
  public authors = [];

  constructor(private adminService: AdminService) {

  }

  ngOnInit() {
    this.adminService.getStatistics().subscribe((statistics:any) => {
      statistics.map((statistic, index) => {
        const statisticKeys = Object.keys(statistic);
        statisticKeys.map(statisticKey => {
          if(statisticKey === 'registered_users') {
            this.registeredUsers.push({
              name: this.chartData[Number(statistic['id'].split('_')[0])].name,
              value: statistic[statisticKey]
            });
          }
          if(statisticKey === 'become_pro') {
            this.becomePro.push({
              name: this.chartData[Number(statistic['id'].split('_')[0])].name,
              value: statistic[statisticKey]
            });
          }
          if(statisticKey === 'earnings_amount') {
            this.earningsAmount = [
              {
                "name": "Germany",
                "series": [
                  {
                    "name": "1990",
                    "value": 0
                  },
                  {
                    "name": "2010",
                    "value": 10
                  },
                  {
                    "name": "2011",
                    "value": 5
                  },
                  {
                    "name": "2015",
                    "value": 15
                  },
                  {
                    "name": "2020",
                    "value": 10
                  }
                ]
              }
            ]
            // this.earningsAmount.push({
            //   name: this.chartData[Number(statistic['id'].split('_')[0])].name,
            //   series: [{
            //     name: ''
            //     value: statistic[statisticKey] / 100
            //   }]
            // });
          }
          if(statisticKey === 'watched_lessons') {
            this.watchedLessons.push({
              name: this.chartData[Number(statistic['id'].split('_')[0])].name,
              value: statistic[statisticKey]
            });
          }
          if(statisticKey === 'accepted_courses') {
            this.acceptedCourses.push({
              name: this.chartData[Number(statistic['id'].split('_')[0])].name,
              value: statistic[statisticKey]
            });
          }
          if(statisticKey === 'authors') {
            this.authors.push({
              name: this.chartData[Number(statistic['id'].split('_')[0])].name,
              value: statistic[statisticKey]
            });
          }
        })
      })
    })
  }
}
