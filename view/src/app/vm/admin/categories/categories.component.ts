import { Component, OnInit } from '@angular/core';
import { SubSink } from 'subsink';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AdminService } from 'src/app/services/admin.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ColumnMode } from '@swimlane/ngx-datatable';
import {
  faPlus
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'eplatform-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  faPlus = faPlus;
  private subs = new SubSink();
  public loading:boolean = true;
  public categories = [];
  public error = '';

  public columnMode = ColumnMode;

  rows = [];
  
  // ***********
  // FORM <CATEGORY>
  // ***********
  public categoryForm = new FormGroup({
    category: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required)
  });
  
  constructor(
    private adminService: AdminService,
    private modalService: NgbModal
  ) {}

  ngOnInit() {
    this.categoryForm.disable();
    this.subs.sink = this.adminService.getCategories().subscribe((categories:any) => {
      this.rows = categories.map(category => {
        return {
          image: category['image'],
          name: category['category'],
          description: category['description']
        }
      });
      this.loading = false;
      this.categoryForm.enable();
    });
  }

  categoryImage;
  uploadCategoryImage(event) {
    this.categoryImage = event.target.files[0];
  }
  
  async addCategory() {
    this.loading = true;
    this.categoryForm.disable();

    const data = new FormData();
    data.append('category', this.categoryForm.get('category').value);
    data.append('description', this.categoryForm.get('description').value);
    data.append('image', this.categoryImage);

    const category = await this.adminService.createCategory(data).toPromise();
    this.categories.unshift(category);
    this.categoryForm.enable();
    this.categoryForm.setValue({
      category: '',
      description: ''
    });
    this.categoryImage = null;
    this.loading = false;
    (<HTMLInputElement>document.getElementById('categoryImage')).value = '';
    this.modalService.dismissAll();
  }

  async deleteCategory(data) {
    this.loading = true;
    this.categoryForm.disable();
    await this.adminService.deleteCategory(data.id).toPromise();
    this.categories = this.categories.filter(e => e.id !== data.id);
    this.categoryForm.enable();
    this.loading = false;
  }

  openModal(content) {
    this.modalService.open(content);
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
