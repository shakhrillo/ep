import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { SubSink } from 'subsink';
import { CourseService } from '../../../services/course.service';
import { selectUserProperty } from '../../../store/selectors/user.selector';
import { ColumnMode } from '@swimlane/ngx-datatable';
import {
  faPlus
} from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditCourseComponent } from 'src/app/modals/edit-course/edit-course.component';

@Component({
  selector: 'eplatform-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit {
  private subs = new SubSink();
  public user;
  public courses = [];
  public loading:boolean = true;

  public faPlus = faPlus;
  public columnMode = ColumnMode;
  public showLoadMoreBtn:boolean = false;
  private loadLimit = 9;
  public offset = 0;
  
  constructor(
    private modalService: NgbModal,
    private courseService: CourseService,
    private store: Store<any>
  ) { }

  ngOnInit() {
    this.loadCourses(this.offset, this.loadLimit);

    this.subs.sink = this.store.pipe(select(selectUserProperty)).subscribe(user => {
      this.user = user;
    });
  }

  loadCourses(offset, limit) {
    this.subs.sink = this.courseService.getAllCourses(offset, limit).subscribe((courses:any = []) => {
      if(this.courses.length > 0) {
        this.courses = [...this.courses, ...courses];
      } else {
        this.courses = courses;
      }
      this.showLoadMoreBtn = courses.length === this.loadLimit;
      this.loading = false;
    });
  }

  loadMore() {
    this.loading = true;
    this.loadCourses(this.offset + this.loadLimit, this.loadLimit);
    this.offset = this.offset + this.loadLimit;
  }

  editCourse(courseId) {
    let index = this.courses.findIndex(course => course.id === courseId);
    let course = this.courses[index];
    const modalRef = this.modalService.open(EditCourseComponent, {beforeDismiss: () => false});
    modalRef.componentInstance.courseData = course;
    modalRef.result.then(_course => {
      if(_course) {
        course = {...course, ..._course};
        this.courses[index] = course;
        this.courses = [...this.courses];
      }
    });
  }

  deleteCourse(id) {
    this.courses = this.courses.filter(course => course.id !== id);
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
