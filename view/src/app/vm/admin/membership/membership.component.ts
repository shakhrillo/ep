import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminService } from 'src/app/services/admin.service';
import { ColumnMode } from '@swimlane/ngx-datatable';
import {
  faPlus,
  faCheck,
  faTimes
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'eplatform-membership',
  templateUrl: './membership.component.html',
  styleUrls: ['./membership.component.scss']
})
export class MembershipComponent implements OnInit {

  public loading: boolean = true;
  public membership = [];

  public faPlus = faPlus;
  public faCheck = faCheck;
  public faTimes = faTimes;
  public columnMode = ColumnMode;

  // ***********
  // FORM <MEMBERSHIP PROS>
  // ***********
  public membershipForm = new FormGroup({
    pros: new FormControl('', Validators.required),
    isPro: new FormControl(true, Validators.required)
  });

  constructor(
    private adminService: AdminService,
    private modalService: NgbModal
  ) {}

  open(content) {
    this.modalService.open(content);
  }

  async create() {
    this.membershipForm.disable();
    this.loading = true;
    await this.adminService.createMembership(this.membershipForm.value).toPromise();
    this.membership.push(this.membershipForm.value);
    this.loading = false;
    this.membershipForm.enable();
    this.membershipForm.setValue({
      pros: '',
      isPro: ''
    });
    this.modalService.dismissAll();

    this.membership = [...this.membership];
  }

  async delete(id) {
    this.loading = true;
    await this.adminService.deleteMembership(id).toPromise();
    this.membership = this.membership.filter(e => e.id !== id);
    this.loading = false;

    this.membership = [...this.membership];
  }

  async ngOnInit() {
    this.membership = <any>await this.adminService.getMembership().toPromise();
    this.loading = false;
  }

  
}
