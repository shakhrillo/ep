import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { SubSink } from 'subsink';
import { CourseService } from '../../../services/course.service';
import { selectUserProperty } from '../../../store/selectors/user.selector';
import { ColumnMode } from '@swimlane/ngx-datatable';

@Component({
  selector: 'eplatform-courses-pending',
  templateUrl: './courses-pending.component.html',
  styleUrls: ['./courses-pending.component.scss']
})
export class CoursesPendingComponent implements OnInit {

  private subs = new SubSink();
  public user;
  public courses = [];
  public loading:boolean = true;

  public columnMode = ColumnMode;
  public showLoadMoreBtn:boolean = false;
  private loadLimit = 9;
  public offset = 0;
  
  constructor(
    private courseService: CourseService,
    private store: Store<any>
  ) { }

  ngOnInit() {
    this.loadCourses(this.offset, this.loadLimit);

    this.subs.sink = this.store.pipe(select(selectUserProperty)).subscribe(user => {
      this.user = user;
    });
  }

  loadCourses(offset, limit) {
    this.subs.sink = this.courseService.getAllPendingCourses(offset, limit).subscribe((courses:any = []) => {
      if(this.courses.length > 0) {
        this.courses = [...this.courses, ...courses];
      } else {
        this.courses = courses;
      }
      this.showLoadMoreBtn = courses.length === this.loadLimit;
      this.loading = false;
    });
  }

  loadMore() {
    this.loading = true;
    this.loadCourses(this.offset + this.loadLimit, this.loadLimit);
    this.offset = this.offset + this.loadLimit;
  }

  deleteCourse(id) {
    this.courses = this.courses.filter(course => course.id !== id);
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
