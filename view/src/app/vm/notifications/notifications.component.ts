import { Component, OnInit } from '@angular/core';
import { SubSink } from 'subsink';
import { Store } from '@ngrx/store';
import { UserService } from '../../services/user.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RenderCommentComponent } from 'src/app/shared/render-comment/render-comment.component';

@Component({
  selector: 'eplatform-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

  private subs = new SubSink();
  public notifications = [];
  public loading:boolean = true;

  public showLoadMoreBtn:boolean = false;
  private loadLimit = 10;
  public offset = 0;
  
  constructor(
    private modalService: NgbModal,
    private userService: UserService,
  ) { }

  ngOnInit() {
    this.loadNotifications(this.offset, this.loadLimit);
  }

  reloadNotification() {
    this.showLoadMoreBtn = true;
    this.loading = true;
    this.loadLimit = 10;
    this.offset = 0;
    this.notifications = [];
    this.loadNotifications(0, 10);  
  }

  loadNotifications(offset, limit) {
    this.subs.sink = this.userService.getNotifications(offset, limit).subscribe((notifications:any = []) => {
      if(this.notifications.length > 0) {
        this.notifications = [...this.notifications, ...notifications];
      } else {
        this.notifications = notifications;
      }
      this.showLoadMoreBtn = notifications.length === this.loadLimit;
      this.loading = false;
    });
  }

  loadMore() {
    this.loading = true;
    this.loadNotifications(this.offset + this.loadLimit, this.loadLimit);
    this.offset = this.offset + this.loadLimit;
  }

  openComment(comment, courseId, videoId, repliedTo?) {
    const modalRef = this.modalService.open(RenderCommentComponent);
    modalRef.componentInstance.comment = {...comment, repliedTo};
    modalRef.componentInstance.courseId = courseId;
    modalRef.componentInstance.videoId = videoId;
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
