import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MessagesService } from '../../services/messages.service';
import { SubSink } from 'subsink';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'eplatform-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  user;
  public rooms = [];
  loading: boolean = false;
  error;
  public wrapperHeight;
  public loadLimit = 10;

  // ***********
  // FORM <MESSAGE>
  // ***********
  public commentsForm = new FormGroup({
    message: new FormControl(''),
  });

  public messageSelected = false;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private messagesService: MessagesService
  ) { }
  
  public currentURL;
  ngOnInit() {
    this.wrapperHeight = window.innerHeight;
    this.currentURL = location.pathname.split('/').slice(-1)[0];

    this.getRooms(null);
  }

  initialCall;
  getRooms(next) {
    this.loading = true;
    this.subs.unsubscribe();
    if(!next) {
      this.initialCall = true;
    } else {
      this.initialCall = false;
    }
    this.subs.sink = this.messagesService.joinUsers(this.messagesService.getRooms(localStorage.getItem('uid'), next && next.slice(-1)[0]['lastDoc'], this.loadLimit)).subscribe(rooms => {
      this.rooms = rooms;
      this.loading = false;
    });
  }

  selectRoom(room) {
    this.currentURL = room.id;
    this.router.navigate([`messages/${room.id}`]);
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
