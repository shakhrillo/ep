import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessagesComponent } from './messages.component';
import { SharedModule } from '../../shared/shared.module';
import { MessagesRoutingModule } from './messages.routing';
import { RoomMessagesComponent } from './room-messages/room-messages.component';

@NgModule({
  declarations: [
    MessagesComponent, RoomMessagesComponent],
  imports: [
    CommonModule,
    SharedModule,
    MessagesRoutingModule,
  ]
})
export class MessagesModule { }
