import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MessagesComponent } from './messages.component';
import { RoomMessagesComponent } from './room-messages/room-messages.component';

const routes: Routes = [{
  path: '',
  component: MessagesComponent,
  children: [{
    path: ':id',
    component: RoomMessagesComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessagesRoutingModule { }