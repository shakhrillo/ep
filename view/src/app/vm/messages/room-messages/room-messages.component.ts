import { ChangeDetectorRef, Component, NgZone, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { MessagesService } from '../../../services/messages.service';
import { SubSink } from 'subsink';
import { NgScrollbar } from 'ngx-scrollbar';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { collectionData } from 'rxfire/firestore';

@Component({
  selector: 'eplatform-room-messages',
  templateUrl: './room-messages.component.html',
  styleUrls: ['./room-messages.component.scss']
})
export class RoomMessagesComponent implements OnInit, OnDestroy {
  @ViewChild(NgScrollbar) scrollable: NgScrollbar;

  private subs = new SubSink();
  public user;
  public messages = [];
  public currentRoom = {};
  public loading: boolean;

  private recieverUID;
  public messagesWrapperHeight;

  // ***********
  // FORM <MESSAGE>
  // ***********
  public commentsForm = new FormGroup({
    message: new FormControl(''),
  });

  constructor(
    private activatedRoute: ActivatedRoute,
    private store: Store<any>,
    private messagesService: MessagesService,
    private ref: ChangeDetectorRef,
    private ngZone: NgZone
  ) {
    this.subs.sink = this.activatedRoute.params.pipe(
      switchMap(query => {
        this.recieverUID = query.id;
        this.initialCall = true;
        const uid = localStorage.getItem('uid');
        this.loading = true;
        const messages$ = this.messagesService.getMessages(uid, query.id);

        return collectionData(messages$);
      })
    ).subscribe((messages) => {
      this.ngZone.run(() => {
        this.messages = messages;
        
        this.loadMoreBtn = true;
        this.commentsForm.enable();
        
        
        if(messages.length > 0 && (this.justSentMessage || this.initialCall)) {
          const interval = setInterval(() => {
            let index = document.getElementById('lastIndex');
            if(index) {
              clearInterval(interval);
              this.scrollable.scrollTo({
                top: document.getElementById('lastIndex').offsetTop,
                duration: 0
              });
              this.reachedBottom();
              this.commentsForm.reset();
            }
          }, 10);
        }
        
        this.justSentMessage = false;
        this.initialCall = false;
        this.loading = false;
      });
    });
  }

  private initialCall = true;
  ngOnInit() {
    this.messagesWrapperHeight = window.innerHeight - 150;
    this.commentsForm.disable();
  }

  public loadMoreBtn = true;
  public loadingMore = false;
  private loadMoreLimit = 5;
  async loadMore() {
    this.loadingMore = true;

    const messages = await this.messagesService.loadMessages(localStorage.getItem('uid'), this.recieverUID, this.messages[0].created, this.loadMoreLimit).get();
    let _messages = [];
    messages.forEach(_ => {
      _messages.push(_.data());
    });
    if(_messages.length > 0) {
      const scrollTo = _messages.length < this.loadMoreLimit ? 0 : this.loadMoreLimit;
      this.messages = [..._messages, ...this.messages];

      const interval = setInterval(() => {
        let index = document.getElementById(`scroll-${scrollTo}`);
        if(index) {
          clearInterval(interval);
          this.scrollable.scrollTo({
            top: index.offsetTop,
            duration: 100
          });
        }
      }, 10);
    } else {
      this.loadMoreBtn = false;
    }
    this.loadingMore = false;
  }

  async reachedBottom() {
    await this.messagesService.asReadMessage(this.recieverUID).toPromise();
  }

  reachedTop() {
    this.loadMoreBtn && this.loadMore();
  }

  public justSentMessage = false;
  async sendMsg() {
    this.justSentMessage = true;
    this.commentsForm.disable();
    const recieverUID = location.pathname.split('/').slice(-1)[0];
    let data = new FormData();
    data.append('message', this.commentsForm.get('message').value);
    await this.messagesService.sendMessage(recieverUID, data).toPromise();
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
