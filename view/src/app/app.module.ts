import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './store/reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { environment } from '../environments/environment';
import { CustomRouteSerializer } from './store/serailizer/route.serializer';
import { AngularFireModule } from '@angular/fire';
import { HttpConfigInterceptor } from './services/interceptor.service';
import { SharedModule } from './shared/shared.module';

// MODALS
import { UploadCourseComponent } from './modals/upload-course/upload-course.component';
import { EditCourseComponent } from './modals/edit-course/edit-course.component';
import { GiveReviewComponent } from './modals/give-review/give-review.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    AppComponent,
    UploadCourseComponent,
    EditCourseComponent,
    GiveReviewComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    AppRoutingModule,
    
    AngularFireModule.initializeApp(environment.firebase),
    
    
    StoreModule.forRoot(reducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true
      }
    }),

    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    StoreRouterConnectingModule.forRoot({
      serializer: CustomRouteSerializer
    }),
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
