import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AngularFireAuthGuard, hasCustomClaim, redirectUnauthorizedTo, redirectLoggedInTo } from '@angular/fire/auth-guard';

const adminOnly = () => hasCustomClaim('admin');
const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['auth/login']);
const redirectLoggedInToItems = () => redirectLoggedInTo(['user']);
const belongsToAccount = (next) => hasCustomClaim(`account-${next.params.id}`);

const routes: Routes = [{
  path: '',
  redirectTo: '',
  pathMatch: 'full'
}, {
  path: '',
  loadChildren: () => import('./vm/main/main.module').then((({MainModule}) => MainModule))
}, {
  path: 'messages',
  canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin },
  loadChildren: () => import('./vm/messages/messages.module').then((({MessagesModule}) => MessagesModule))
}, {
  path: 'notifications',
  canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin },
  loadChildren: () => import('./vm/notifications/notifications.module').then((({NotificationsModule}) => NotificationsModule))
}, {
  path: 'admin',
  canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin },
  loadChildren: () => import('./vm/admin/admin.module').then((({AdminModule}) => AdminModule))
}, {
  path: 'user',
  canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin },
  loadChildren: () => import('./vm/user/user.module').then((({UserModule}) => UserModule))
}, {
  path: 'auth',
  canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectLoggedInToItems },
  loadChildren: () => import('./vm/auth/auth.module').then((({AuthModule}) => AuthModule))
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
