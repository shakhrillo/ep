import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { CourseService } from '../../services/course.service';
import { selectUserProperty } from '../../store/selectors/user.selector';
import { SubSink } from 'subsink';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RenderCommentComponent } from '../render-comment/render-comment.component';

@Component({
  selector: 'eplatform-render-comments',
  templateUrl: './render-comments.component.html',
  styleUrls: ['./render-comments.component.scss']
})
export class RenderCommentsComponent implements OnInit, OnChanges {

  @Input('query') query;
  private subs = new SubSink();
  public user;
  public comments = [];
  public loading: boolean = false;
  public error;

  public showLoadMoreBtn: boolean = false;

  public editCommentModel;
  public replyComment;

  // ***********
  // FORM <COMMENT>
  // ***********
  public commentsForm = new FormGroup({
    comment: new FormControl(''),
  });

  public commentsFormEdit = new FormGroup({
    comment: new FormControl(''),
  });

  public commentsFormReply = new FormGroup({
    comment: new FormControl(''),
  });

  constructor(
    private store: Store<any>,
    private courseService: CourseService,
    private modalService: NgbModal
  ) { }

  private offsetLoadLimit = 5;
  ngOnInit() {
    this.subs.sink = this.store.pipe(select(selectUserProperty)).subscribe(user => {
      this.user = user;
    });

    this.loadCourseComments();
  }

  ngOnChanges() { }

  private loadLimit = 5;
  async loadCourseComments() {
    this.commentsForm.enable();
    this.commentsForm.setValue({
      comment: ''
    });
    this.loading = true;
    const comments = <any>await this.courseService.getCourseLessonComments(this.query.courseId, this.query.videoId, 0, this.loadLimit).toPromise();
    this.comments = comments;
    this.loading = false;
    if(comments.length === this.offsetLoadLimit) this.showLoadMoreBtn = true;
    
  }
  
  public offset = 0;
  async loadMoreCourseComments() {
    this.loading = true;
    const comments = <any>await this.courseService.getCourseLessonComments(this.query.courseId, this.query.videoId, this.offset + this.offsetLoadLimit, this.loadLimit).toPromise();
    this.comments = [...comments, ...this.comments];
    this.offset = this.offset + this.offsetLoadLimit;
    this.loading = false;
    if(comments.length !== this.loadLimit) this.showLoadMoreBtn = false;
  }

  async createCourseLessonComment() {
    this.commentsForm.disable()
    this.loading = true;
    let data = new FormData();
    data.append('comment', this.commentsForm.get('comment').value);

    await this.courseService.createCourseLessonComments(this.query.courseId, this.query.videoId, data).toPromise();
    this.loadCourseComments();
  }

  selectedReply;
  openReplyModal(replyEditModalContent, comment) {
    this.modalService.open(replyEditModalContent);
    this.selectedReply = comment;
    this.commentsFormReply.setValue({
      comment: ''
    });
  }

  openComment(comment, repliedTo?) {
    const modalRef = this.modalService.open(RenderCommentComponent);
    modalRef.componentInstance.comment = {...comment, repliedTo};
    modalRef.componentInstance.courseId = this.query.courseId;
    modalRef.componentInstance.videoId = this.query.videoId;
  }

  async replyCourseLessonComment() {
    this.commentsForm.disable();
    this.commentsFormReply.disable();
    this.loading = true;
    let data = new FormData();
    data.append('comment', this.commentsFormReply.get('comment').value);
    await this.courseService.replyCourseLessonComments(this.query.courseId, this.query.videoId, this.selectedReply.commentId, data).toPromise();
    this.modalService.dismissAll();
    this.commentsFormReply.enable();
    this.loadCourseComments();
  }

  selectedEdit;
  openEditModal(openEditModalContent, comment) {
    this.modalService.open(openEditModalContent);
    this.selectedEdit = comment;
    this.commentsFormEdit.setValue({
      comment: comment.comment
    })
  }

  async updateCourseLessonComment() {
    let data = new FormData();
    data.append('comment', this.commentsFormEdit.get('comment').value);
    this.commentsForm.disable();
    this.commentsFormEdit.disable();
    this.loading = true;
    await this.courseService.editCourseLessonComments(this.query.courseId, this.query.videoId, this.selectedEdit.commentId, data).toPromise();
    this.selectedEdit.comment = this.commentsFormEdit.get('comment').value;
    this.modalService.dismissAll();
    this.loading = false;
    this.commentsFormEdit.enable();
  }

  async deleteCourseLessonComment(comment, index) {
    this.commentsForm.disable();
    this.loading = true;
    await this.courseService.deleteCourseLessonComments(this.query.courseId, this.query.videoId, comment.commentId).toPromise();
    this.comments[index]['hide'] = true;
    this.loading = false;
  }

  async likeComment(comment, index) {
    this.commentsForm.disable();
    this.loading = true;
    await this.courseService.likeCourseLessonComments(this.query.courseId, this.query.videoId, comment.commentId).toPromise();
    this.comments[index]['liked'] = true;
    this.comments[index]['like'] = Number(this.comments[index]['like'] || 0) + 1;
    this.loading = false;
  }
  
  async unLikeComment(comment, index) {
    this.commentsForm.disable();
    this.loading = true;
    await this.courseService.unLikeCourseLessonComments(this.query.courseId, this.query.videoId, comment.commentId).toPromise();
    this.comments[index]['liked'] = false;
    this.comments[index]['like'] = Number(this.comments[index]['like'] || 1) - 1;
    this.loading = false;
  }


}
