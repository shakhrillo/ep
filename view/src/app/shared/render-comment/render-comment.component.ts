import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { SubSink } from 'subsink';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CourseService } from '../../services/course.service';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'eplatform-render-comment',
  templateUrl: './render-comment.component.html',
  styleUrls: ['./render-comment.component.scss']
})
export class RenderCommentComponent implements OnInit, OnDestroy {

  @Input() comment;
  @Input() courseId;
  @Input() videoId;
  private subs = new SubSink();
  public replies = [];
  public loading: boolean = true;

  public showLoadMoreBtn:boolean = false;
  private loadLimit = 10;
  public offset = 0;

  public commentsForm = new FormGroup({
    comment: new FormControl(''),
  });

  constructor(
    public activeModal: NgbActiveModal,
    private courseService: CourseService,
  ) {}

  ngOnInit() {
    this.loadComments(this.offset, this.loadLimit);
  }

  reloadComments(comment) {
    this.subs.unsubscribe();
    this.replies = [];
    this.loading = true;
    this.offset = 0;
    this.loadLimit = 10;
    this.comment = comment;
    this.loadComments(this.offset, this.loadLimit);  
  }

  loadComments(offset, limit) {
    this.subs.sink = this.courseService.getCourseLessonComment(this.courseId, this.videoId, this.comment.commentId || this.comment.repliedTo, offset, limit)
    .subscribe(comment => {
      this.comment = comment;
      if(this.replies.length > 0) {
        this.replies = [...this.replies, ...comment['replies']];
      } else {
        this.replies = comment['replies'];
      }
      this.showLoadMoreBtn = comment['replies'].length === this.loadLimit;
      this.loading = false;
    });
  }

  loadMore() {
    this.loading = true;
    this.loadComments(this.offset + this.loadLimit, this.loadLimit);
    this.offset = this.offset + this.loadLimit;
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  
  async createCourseLessonComment() {
    this.commentsForm.disable()
    this.loading = true;
    let data = new FormData();
    data.append('comment', this.commentsForm.get('comment').value);
    
    this.commentsForm.setValue({
      comment: ''
    })
    this.commentsForm.enable()
    await this.courseService.replyCourseLessonComments(this.courseId, this.videoId, this.comment.commentId, data).toPromise();
    this.offset = 0;
    this.loadLimit = 10;
    this.replies = [];
    this.loading = true;
    this.loadComments(0, 10);
  }
}
