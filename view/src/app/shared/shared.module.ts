import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { QuillModule } from 'ngx-quill';
import { NgSelectModule } from '@ng-select/ng-select';
import { MomentModule } from 'ngx-moment';
import { NavbarComponent } from './navbar/navbar.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { SafePipe } from '../pipe/safe.pipe';
import { RendererPipe } from '../pipe/render-message.pipe';
import { SecondsPipe } from '../pipe/render-seconds.pipe';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { NgScrollbarReachedModule } from 'ngx-scrollbar/reached-event';
import { RenderVideoComponent } from './render-video/render-video.component';
import { RenderCommentsComponent } from './render-comments/render-comments.component';
import { RenderCommentComponent } from './render-comment/render-comment.component';
import { SmoothScrollModule } from 'ngx-scrollbar/smooth-scroll';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { RenderCommentStaticComponent } from './render-comment-static/render-comment-static.component';
import { RenderCourseStaticComponent } from './render-course-static/render-course-static.component';
import { RenderUserStaticComponent } from './render-user-static/render-user-static.component';
import { RenderReviewStaticComponent } from './render-review-static/render-review-static.component';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

const sharedModules = [
  FormsModule,
  ReactiveFormsModule,
  NgbModule,
  NgSelectModule,
  MomentModule,
  HttpClientModule,
  NgScrollbarModule,
  NgScrollbarReachedModule,
  SmoothScrollModule,
  NgxChartsModule,
  FontAwesomeModule,
  NgxDatatableModule
];

const sharedComponents = [
  NavbarComponent,
  SafePipe,
  RendererPipe,
  SecondsPipe,
  RenderVideoComponent,
  RenderCommentsComponent,
  RenderCommentComponent,
  RenderCommentStaticComponent,
  RenderCourseStaticComponent,
  RenderUserStaticComponent,
  RenderReviewStaticComponent,
];

@NgModule({
  declarations: [
    ...sharedComponents,
  ],
  imports: [
    CommonModule,
    RouterModule,
    QuillModule.forRoot({
      modules: {
        toolbar: [
          ['bold', 'italic', 'underline', 'strike'],
          [{ 'list': 'ordered'}, { 'list': 'bullet' }],
          ['link'],
          ['code-block'],
        ]
      }
    }),
    ...sharedModules
  ],
  exports: [
    ...sharedModules,
    ...sharedComponents,
    QuillModule,
  ]
})
export class SharedModule { }
