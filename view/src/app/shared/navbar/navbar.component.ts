import { Component } from '@angular/core';

@Component({
  selector: 'eplatform-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {}