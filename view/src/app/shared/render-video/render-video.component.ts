import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, Renderer2 } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { UserService } from '../../services/user.service';
declare let vooAPI:any;

@Component({
  selector: 'eplatform-render-video',
  templateUrl: './render-video.component.html',
  styleUrls: ['./render-video.component.scss']
})
export class RenderVideoComponent implements OnInit, OnChanges, OnDestroy {

  @Input('courseImg') courseImg;
  @Input('courseId') courseId;
  @Input('author') author;
  @Input('videoURL') videoURL;
  @Input('time') time;
  @Output('videoEnded') videoEnded = new EventEmitter();
  private watchPlayerActive$ = new Subject();
  private playerId;
  private playing: boolean;
  public loading: boolean = true;

  private initialWatchCall: boolean = false;

  constructor(
    private renderer: Renderer2,
    private userService: UserService
  ) {
    this.watchPlayerActive$.pipe(debounceTime(200), distinctUntilChanged()).subscribe((playing: boolean) => {
      this.playing = playing;

      if(playing && !this.initialWatchCall) {
        this.initialWatchCall = true;
        this.watchTimeInitial();
      }
    });
  }

  ngOnInit() {
    this.playerId = this.videoURL.split('publish/')[1];
    this.renderVideo(this.videoURL, this.playerId);
  }

  ngOnChanges() {}

  renderVideo(url, playerId) {
    const iframe: HTMLIFrameElement = document.createElement('iframe');
    iframe.setAttribute('data-playerId', playerId);
    iframe.setAttribute('id', 'voo-player-wrapper');
    iframe.setAttribute('allow', 'autoplay');
    iframe.setAttribute('class', 'vooplayer');
    iframe.setAttribute('allowtransparency', 'true');
    iframe.setAttribute('allowfullscreen', 'true');
    iframe.setAttribute('frameborder', '0');
    iframe.setAttribute('scrolling', 'no');
    iframe.setAttribute('src', url + '?fallback=true');
    iframe.setAttribute('style', 'width:1px; min-width:100%; height: 100%; position:absolute');

    let appendPlayer = setInterval(() => {
      const player = document.getElementById('player');
      if(player) {
        player.innerHTML = '';
        this.renderer.appendChild(player, iframe);
        this.watchPlayer(playerId);
        clearInterval(appendPlayer);
      }
    }, 10);

  }

  private watchTimeCheck;
  watchPlayer(id) {
    document.addEventListener('vooPlayerReady', (e) => {
      this.loading = false;
      vooAPI(id, 'play');
      vooAPI(id, 'onEnded', null, () => {
        this.watchPlayerActive$.next(false);
      });
      vooAPI(id, 'onPause', null, () => {
        this.watchPlayerActive$.next(false);
      });
      vooAPI(id, 'onPlay', null, () => {
        this.watchPlayerActive$.next(true);
      });
      vooAPI(id, 'onSeeked', null, () => {
        vooAPI(id, 'pause');
        this.watchPlayerActive$.next(false);
      });

      this.watchTimeCheck = setInterval(() => {
        if(this.playing) this.watchTime();
      }, 30000);
    });
  }

  async watchTime() {
    await this.userService.watchTime(this.author.uid, this.courseId).toPromise();
  }

  async watchTimeInitial() {
    await this.userService.initialWatchTime(this.author.uid, this.courseId).toPromise();
  }

  ngOnDestroy(){
    clearInterval(this.watchTimeCheck);
  }
}
