import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'eplatform-render-review-static',
  templateUrl: './render-review-static.component.html',
  styleUrls: ['./render-review-static.component.scss']
})
export class RenderReviewStaticComponent implements OnInit {

  @Input('review') review;
  
  constructor() { }

  ngOnInit() {
  }

}
