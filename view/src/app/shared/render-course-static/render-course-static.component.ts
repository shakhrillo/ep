import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditCourseComponent } from 'src/app/modals/edit-course/edit-course.component';
import { CourseService } from 'src/app/services/course.service';

@Component({
  selector: 'eplatform-render-course-static',
  templateUrl: './render-course-static.component.html',
  styleUrls: ['./render-course-static.component.scss']
})
export class RenderCourseStaticComponent implements OnInit {

  @Output('deleted') deleted = new EventEmitter();
  @Input('course') course;
  @Input('user') user;

  public loading: boolean = false;

  constructor(
    private modalService: NgbModal,
    private courseService: CourseService
  ) { }

  ngOnInit() {
  }

  async deleteCourse() {
    this.loading = true;
    await this.courseService.deleteCourse(this.course.id).toPromise();
    this.deleted.emit(this.course.id);
    this.loading = false;
  }
  
  async acceptCourse() {
    this.loading = true;
    await this.courseService.acceptCourse(this.course.id).toPromise();
    this.deleted.emit(this.course.id);
    this.loading = false;
  }

  editCourse() {
    const modalRef = this.modalService.open(EditCourseComponent, {beforeDismiss: () => false});
    modalRef.componentInstance.courseData = this.course;
    modalRef.result.then(course => {
      if(course) {
        this.course = {...this.course, ...course};
      }
    });
  }

}
