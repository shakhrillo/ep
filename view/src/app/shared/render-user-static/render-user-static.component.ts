import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AdminService } from 'src/app/services/admin.service';

@Component({
  selector: 'eplatform-render-user-static',
  templateUrl: './render-user-static.component.html',
  styleUrls: ['./render-user-static.component.scss']
})
export class RenderUserStaticComponent implements OnInit {

  @Output('deleted') deleted = new EventEmitter();
  @Input('currentUser') currentUser;
  @Input('user') user;

  constructor( private adminService: AdminService ) { }

  ngOnInit() {
  }

  
  async rejectAuthor() {
    await this.adminService.rejectAuthor(this.user.uid).toPromise();
    this.deleted.emit(this.user.uid);
  }
  
  async acceptAuthor() {
    await this.adminService.acceptAuthor(this.user.uid).toPromise();
    this.deleted.emit(this.user.uid);
  }
  
  async deleteUser() {
    await this.adminService.deleteUser(this.user.uid).toPromise();
    this.deleted.emit(this.user.uid);
  }

}
