import { Component, EventEmitter, HostBinding, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { CourseService } from 'src/app/services/course.service';

@Component({
  selector: 'eplatform-render-comment-static',
  templateUrl: './render-comment-static.component.html',
  styleUrls: ['./render-comment-static.component.scss']
})
export class RenderCommentStaticComponent implements OnInit {

  @HostBinding('class.d-none') hide: boolean = false;
  @Input('query') query;
  @Input('loading') loading;
  @Input('comment') comment;
  @Output('openComment') openComment = new EventEmitter();
  public edit:boolean = false;

  public commentsForm = new FormGroup({
    comment: new FormControl(''),
  });

  constructor(
    private courseService: CourseService
  ) { }

  ngOnInit() {
  }

  async updateCourseLessonComment() {
    let data = new FormData();
    data.append('comment', this.commentsForm.get('comment').value);
    
    this.commentsForm.disable();
    this.loading = true;
    
    await this.courseService.editCourseLessonComments(this.query.courseId, this.query.videoId, this.comment.commentId, data).toPromise();
    this.edit = false;
    this.comment.comment = this.commentsForm.get('comment').value;
    this.loading = false;
    this.commentsForm.enable();
  }

  async deleteCourseLessonComment(comment) {
    this.commentsForm.disable();
    this.loading = true;
    await this.courseService.deleteCourseLessonComments(this.query.courseId, this.query.videoId, comment.commentId).toPromise();
    this.hide = true;
    this.loading = false;
    this.commentsForm.enable();
  }

  async likeComment(comment) {
    this.commentsForm.disable();
    this.loading = true;
    await this.courseService.likeCourseLessonComments(this.query.courseId, this.query.videoId, comment.commentId).toPromise();
    this.comment['liked'] = true;
    this.comment['like'] = Number(this.comment['like'] || 0) + 1;
    this.loading = false;
    this.commentsForm.enable();
  }
  
  async unLikeComment(comment) {
    this.commentsForm.disable();
    this.loading = true;
    await this.courseService.unLikeCourseLessonComments(this.query.courseId, this.query.videoId, comment.commentId).toPromise();
    this.comment['liked'] = false;
    this.comment['like'] = Number(this.comment['like'] || 1) - 1;
    this.loading = false;
    this.commentsForm.enable();
  }

  editOpen() {
    this.edit = true;
    this.commentsForm.setValue({
      comment: this.comment.comment
    });
  }

}
