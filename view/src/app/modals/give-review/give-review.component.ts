import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CourseService } from 'src/app/services/course.service';

@Component({
  selector: 'eplatform-give-review',
  templateUrl: './give-review.component.html',
  styleUrls: ['./give-review.component.scss']
})
export class GiveReviewComponent implements OnInit {

  @Input() courseId;

  public loading: boolean = false;
  public error;

  public reviewForm = new FormGroup({
    review: new FormControl('', Validators.required),
    rate: new FormControl('', Validators.required),
  });

  constructor(
    public activeModal: NgbActiveModal,
    private courseService: CourseService,
  ) {}

  ngOnInit(): void {
  }

  async submit() {
    this.loading = true;
    this.reviewForm.disable();
    try {
      await this.courseService.giveRate(this.courseId, this.reviewForm.value).toPromise();
      this.activeModal.close();
      this.reviewForm.enable();
    } catch (error) {
      this.loading = false;
      alert(error);
      this.error = error.message;
      this.reviewForm.enable();
    }
  }

}
