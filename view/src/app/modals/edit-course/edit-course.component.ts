import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { SubSink } from 'subsink';
import { CourseService } from '../../services/course.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'eplatform-edit-course',
  templateUrl: './edit-course.component.html',
  styleUrls: ['./edit-course.component.scss']
})
export class EditCourseComponent implements OnInit {
  
  @Input() courseData;
  
  private subs = new SubSink();
  public error;
  public loading;
  public categories = [];

  public tabIndex = 1;
  public activeIdForTranscription = 0;
  public activeIdForVideos = 0;

  // ***********
  // FORM <COURSE>
  // ***********

  public courseFormInfo = new FormGroup({
    title: new FormControl('', Validators.required),
    image: new FormControl('', Validators.required),
    imageBy: new FormControl(''),
    description: new FormControl('', Validators.required),
  });

  public courseFormTopicAndTags = new FormGroup({
    topic: new FormControl('', Validators.required),
    tags: new FormControl([], Validators.required),
  });

  public courseFormVideos = new FormGroup({
    videos: new FormArray([], Validators.required)
  });

  courseImage;
  uploadcourseImage(event) {
    this.courseImage = event.target.files[0];
  }

  public formVideos = [];
  get videos(): FormArray {
    return this.courseFormVideos.get('videos') as FormArray;
  }

  constructor(
    public activeModal: NgbActiveModal,
    private courseService: CourseService,
  ) { }

  ngOnInit() {
    this.courseFormInfo.setValue({
      title: this.courseData.title,
      image: this.courseData.image,
      imageBy: this.courseData.imageBy || '',
      description: this.courseData.description,
    });

    const videos = this.courseData.lessons.map(lesson => {
      this.addFormVideos(lesson.videoTranscript);
      let videoTranscript = [{
        videoTranscriptSec: '',
        videoTranscriptInfo: ''
      }];
      if(lesson.videoTranscript) {
        videoTranscript = lesson.videoTranscript.map(transcript => {
          return {
            videoTranscriptSec: transcript.videoTranscriptSec,
            videoTranscriptInfo: transcript.videoTranscriptInfo
          }
        })
      } 
      return {
        lessonId: lesson.id || '',
        videoURL: lesson.videoURL || '',
        videoTitle: lesson.videoTitle || '',
        videoDescription: lesson.videoDescription || '',
        videoTranscript: videoTranscript
      }
    });

    this.courseFormVideos.setValue({
      videos
    });

    this.courseFormTopicAndTags.setValue({
      topic: this.courseData.topic,
      tags: this.courseData.tags
    });

    this.subs.sink = this.courseService.getCategories().subscribe((categories:any) => {
      this.categories = categories;
    });

  }

  addFormVideos(transcripts) {
    this.formVideos.push({
      transcripts: []
    });
    this.videos.push(new FormGroup({
      lessonId: new FormControl(''),
      videoURL: new FormControl(''),
      videoTitle: new FormControl('', Validators.required),
      videoDescription: new FormControl('', Validators.required),
      videoTranscript: new FormArray([], Validators.required),
    }));
    transcripts.map(() => {
      this.addFormVideosTranscript(this.videos.length - 1);
    });
    this.activeIdForVideos = this.formVideos.length - 1;
  }

  addFormVideosTranscript(index) {
    this.formVideos[index]['transcripts'].push({});

    const videos = this.courseFormVideos.get('videos') as FormArray;
    const videoTranscript = videos.at(index).get('videoTranscript') as FormArray;
    videoTranscript.push(new FormGroup({
      videoTranscriptSec: new FormControl('', Validators.required),
      videoTranscriptInfo: new FormControl('', Validators.required)
    }));

    this.activeIdForTranscription = this.formVideos[index]['transcripts'].length - 1;
  }

  removeFormVideosTranscript(index, i) {
    this.formVideos[index]['transcripts'].splice(i, 1);

    const videos = this.courseFormVideos.get('videos') as FormArray;
    const videoTranscript = videos.at(index).get('videoTranscript') as FormArray;
    videoTranscript.removeAt(i);

    this.activeIdForTranscription = i;
  }

  // ***********
  // PUBLISH <COURSE>
  // ***********
  submit() {
    this.courseFormInfo.disable();
    this.courseFormTopicAndTags.disable();
    this.courseFormVideos.disable();
    this.error = null;
    this.loading = true;
    let data = new FormData();

    // IMAGE
    this.courseImage && data.append('course_image', this.courseImage);
    
    data.append('videos', JSON.stringify(this.courseFormVideos.get('videos').value));
    data.append('tags', JSON.stringify(this.courseFormTopicAndTags.get('tags').value));
    data.append('topic', this.courseFormTopicAndTags.get('topic').value);

    Object.keys(this.courseFormInfo.value).forEach((form_key) => {
      data.append(form_key, this.courseFormInfo.value[form_key]);
    });

    this.subs.sink = this.courseService.updateCourse(this.courseData.id, data).subscribe((course) => {
      this.activeModal.close({...this.courseData, ...course});
    }, error => {
      this.courseFormInfo.enable();
      this.courseFormTopicAndTags.enable();
      this.courseFormVideos.enable();
      alert(error);
      this.error = error;
      this.loading = false;
    })
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
