import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'seconds',
  pure: true
})
export class SecondsPipe implements PipeTransform {

  public transform(value: string) {
    return this.renderSecond(value);
  }

  renderSecond(sec) {
    var date = new Date(0);
    date.setSeconds(sec);
    var timeString = date.toISOString().substr(11, 8);
    return timeString;
  }
}