import { Pipe, PipeTransform } from '@angular/core';
import * as CryptoJs from 'crypto-js';

@Pipe({
  name: 'render',
  pure: true
})
export class RendererPipe implements PipeTransform {

  public transform(value: string) {
    return this.renderMessage(value);
  }

  renderMessage(id) {
    let bytes = CryptoJs.AES.decrypt(id, '**');
    return bytes.toString(CryptoJs.enc.Utf8);
  }
}