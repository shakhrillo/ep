import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({providedIn: 'root'})
export class AdminService {
  private URL:string = environment.api.url;

  constructor(
    private angularFirestore: AngularFirestore,
    private http: HttpClient
  ) { }

  // EARNINGS
  getEarnings(id) {
    return this.http.get(`${this.URL}/earnings/${id}`);
  }

  // STATISTICS
  getStatistics() {
    return this.http.get(`${this.URL}/statistics`);
  }

  // MEMBERSHIP
  getMembership() {
    return this.http.get(`${this.URL}/membership`);
  }
  createMembership(data) {
    return this.http.post(`${this.URL}/membership`, data);
  }
  deleteMembership(id) {
    return this.http.delete(`${this.URL}/membership/${id}`);
  }

  // CATEGORIES
  getCategories() {
    return this.http.get(`${this.URL}/categories`);
  }
  createCategory(data) {
    return this.http.post(`${this.URL}/category`, data);
  }
  deleteCategory(id) {
    return this.http.delete(`${this.URL}/category/${id}`);
  }

  getUsers(offset, limit) {
    return this.http.get(`${this.URL}/users?offset=${offset}&limit=${limit}`);
  }
  getAuthors(offset, limit) {
    return this.http.get(`${this.URL}/authors?offset=${offset}&limit=${limit}`);
  }
  getPendingAuthors(offset, limit) {
    return this.http.get(`${this.URL}/authors-pending?offset=${offset}&limit=${limit}`);
  }
  acceptAuthor(id) { 
    return this.http.post(`${this.URL}/author/${id}`, {});
  }
  rejectAuthor(id) {
    return this.http.delete(`${this.URL}/author/${id}`);
  }
  deleteUser(id) {
    return this.http.delete(`${this.URL}/user/${id}`);
  }


  // -------------

  
  getPendingCourses() {
    return this.http.get(`${this.URL}/course/pending-courses`);
  }

  getPendingCourse(id) {
    return this.http.get(`${this.URL}/course/pending-course?id=${id}`);
  }

  acceptCourse(data) {
    return this.http.post(`${this.URL}/course/accept-course`, data);
  }

  
  getProducts() {
    return this.http.get(`${this.URL}/stripe/products`);
  }

  getProduct() {
    return this.http.get(`${this.URL}/stripe/product`);
  }

  createProduct(data) {
    return this.http.post(`${this.URL}/stripe/product`, data);
  }

  updateProduct(data) {
    return this.http.put(`${this.URL}/stripe/product`, data);
  }

  deleteProduct(id) {
    return this.http.delete(`${this.URL}/stripe/product?id=${id}`);
  }

  getPlans() {
    return this.http.get(`${this.URL}/stripe/plans`);
  }

  getPlan() {
    return this.http.get(`${this.URL}/stripe/plan`);
  }

  createPlan(data) {
    return this.http.post(`${this.URL}/stripe/plan`, data);
  }

  updatePlan(data) {
    return this.http.put(`${this.URL}/stripe/plan`, data);
  }

  deletePlan(id) {
    return this.http.delete(`${this.URL}/stripe/plan?id=${id}`);
  }
}