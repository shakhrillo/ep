import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private URL:string = environment.api.url;

  loadedUsers = {}

  constructor(
    private http: HttpClient,
    private auth: AngularFireAuth,
    private angularFirestore: AngularFirestore
  ) { }

  // LANDING PAGE
  getLandingPage() { return this.http.get(`${this.URL}/landing-page`); }
  
  // EARNINGS
  getEarning(id) { return this.http.get(`${this.URL}/earning/${id}`); }

  // AUTHOR
  becomeAuthor(data) { return this.http.post(`${this.URL}/author`, data); }

  // USER
  getUser(id) { return this.http.get(`${this.URL}/user/${id}`); }
  getUsers(offset, limit) { return this.http.get(`${this.URL}/users?offset=${offset}&limit=${limit}`); }
  getAllUsers(offset, limit) { return this.http.get(`${this.URL}/users/all?offset=${offset}&limit=${limit}`); }
  getAuthors(offset, limit) { return this.http.get(`${this.URL}/authors?offset=${offset}&limit=${limit}`); }

  // NOTIFICATION
  getNotifications(offset, limit) { return this.http.get(`${this.URL}/feeds?offset=${offset}&limit=${limit}`); }
  readNotifications(uid) { return this.angularFirestore.collection('_users').doc(uid).collection('board').doc('statistics').set({notifications: 0}) }
  gotNotifications(uid) { return this.angularFirestore.collection('_users').doc(uid).collection('alerts').doc('data').valueChanges() }
  
  // SUBSCRIPTION
  getUserSubscription() { return this.http.get(`${this.URL}/user/subscription`); }
  
  // ADMIN
  deleteUser(id) { return this.http.delete(`${this.URL}/user/${id}`); }
  
  // USER
  deleteUserPhoto() { return this.http.delete(`${this.URL}/user/photo`); }
  updateUser(data) { return this.http.put(`${this.URL}/user`, data);}
  updateUserPayoutEmail(data) { return this.http.post(`${this.URL}/user/payout-email`, data);}
  
  // COURSE WATCH
  watchTime(uid, courseId) { return this.http.put(`${this.URL}/watch_time/${uid}/${courseId}`, {}); }
  initialWatchTime(uid, courseId) { return this.http.post(`${this.URL}/watch_time/${uid}/${courseId}`, {}); }
}
