import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class SubscriptionService {

  private URL:string = environment.api.url;
  
  constructor(
    private http: HttpClient
  ) { }
  
  getPlans() {
    return this.http.get(`${this.URL}/subscription/plans`);
  }

  getInvoices(offset, limit) {
    return this.http.get(`${this.URL}/subscription/invoices?offset=${offset}&limit=${limit}`);
  }

  reActivateSubscription(id) {
    return this.http.put(`${this.URL}/subscription/${id}`, {});
  }

  cancelSubscription(id) {
    return this.http.delete(`${this.URL}/subscription/${id}`);
  }

  updateCard(id, data) {
    return this.http.put(`${this.URL}/subscription/${id}/card`, data);
  }

  createPaymentMethod(data) {
    return this.http.post(`${this.URL}/subscription/checkout`, data);
  }
}