import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFireAuth } from '@angular/fire/auth';
import { switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public userToken;

  constructor(
    private http: HttpClient,
    private auth: AngularFireAuth,
  ) { }

  authState() {
    return this.auth.authState.pipe(
      switchMap(async (user) => {
        if(user) {
          const {uid, email, emailVerified, displayName, photoURL} = user;
          const token = await user.getIdToken();
          return {
            uid, email, emailVerified, displayName, photoURL, token
          }
        }
        
        return user;
      })
    );
  }

  getToken() {
    return this.auth.idToken;
  }

  login(email, password) {
    return this.auth.signInWithEmailAndPassword(email, password);
  }
  
  register(email, password) {
    return this.auth.createUserWithEmailAndPassword(email, password);
  }

  resetPassword(email) {
    return this.auth.sendPasswordResetEmail(email);
  }

  logout() {
    return this.auth.signOut();
  }

}
