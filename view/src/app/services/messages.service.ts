import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';
import { environment } from '../../environments/environment';
import { combineLatest, Observable, of, Subject } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class MessagesService {
  
  private URL:string = environment.api.url;

  public loadedMessages = {}
  public loadedRoom = {}

  public unReads = new Subject();

  constructor(
    private http: HttpClient,
    private angularFirestore: AngularFirestore,
  ) { }

  getRoomId(uid) {
    return this.http.post(`${this.URL}/message-room/${uid}`, {}); 
  }
 
  loadMessages(uid, id, date, limit) {
    return this.angularFirestore.firestore.collection('_users').doc(uid).collection('messages').doc(id).collection('messages').orderBy('created').endBefore(date).limitToLast(limit);
  }

  getMessages(uid, id) {
    return this.angularFirestore.firestore.collection('_users').doc(uid).collection('messages').doc(id).collection('messages').orderBy('created').limitToLast(10);
  }

  gotMessage(uid, id) {
    return this.angularFirestore.firestore.collection('_users').doc(uid).collection('messages').doc(id);
  }

  sendMessage(uid, data) { 
    return this.http.post(`${this.URL}/message/${uid}`, data);
  }

  asReadMessage(uid) {
    return this.http.put(`${this.URL}/message/${uid}`, {});
  }

  asReadMessageMain(uid) {
    return this.http.delete(`${this.URL}/message/${uid}`);
  }

  readMessage(id) {
    return this.angularFirestore.collection('_message_aes').doc(id);
  }
  
  getRooms(uid, next, loadLimit) {
    if(!next) {
      return this.angularFirestore.collection('_users').doc(uid).collection('messages', 
        ref => ref.orderBy('created', 'desc').limit(loadLimit)
      ).snapshotChanges();
    } else {
      return this.angularFirestore.collection('_users').doc(uid).collection('messages', 
        ref => ref.orderBy('created', 'desc').startAfter(next).limit(loadLimit)
      ).snapshotChanges();
    }
  }

  joinUsers(chat$: Observable<any>) {
    let chat;
    const joinKeys = {};
    let lastDoc;

    return chat$.pipe(
      switchMap(c => {
        
        if(c.length) {
          lastDoc = c.slice(-1)[0].payload.doc;
          
          c = c.map(e => {
            return {...e.payload.doc.data(), id: e.payload.doc.id};
          });
        }

        // Unique User IDs
        chat = c;
        const uids = Array.from(new Set(c.map(v => v.id)));

        // Firestore User Doc Reads
        const userDocs = uids.map(u =>
          this.angularFirestore.doc(`_users/${u}`).valueChanges()
        );

        return userDocs.length ? combineLatest(userDocs) : of([]);
      }),
      map(arr => {
        arr.filter(e => e).forEach(v => (joinKeys[(<any>v).uid] = v));
        chat = chat.map(v => {
          return { ...v, user: joinKeys[v.id] };
        });

        return [...chat, {lastDoc}];
      })
    );
  }
}