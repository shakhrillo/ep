import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class CourseService {
  private URL:string = environment.api.url;
  
  constructor( 
    private angularFirestore: AngularFirestore,
    private http: HttpClient
  ) { }

  getCategories() {
    return this.http.get(`${this.URL}/categories`);
  }
  getCategoryCourses(id, offset, limit) {
    return this.http.get(`${this.URL}/category/${id}/courses?offset=${offset}&limit=${limit}`);
  }
  submitCourse(data) {
    return this.http.post(`${this.URL}/course`, data, {
      observe: 'events',
      reportProgress: true
    });
  }
  updateCourse(id, data) {
    return this.http.put(`${this.URL}/course/${id}`, data);
  }
  getAllCourses(offset, limit) {
    return this.http.get(`${this.URL}/courses?offset=${offset}&limit=${limit}`);
  }
  getAllPendingCourses(offset, limit) {
    return this.http.get(`${this.URL}/courses-pending?offset=${offset}&limit=${limit}`);
  }
  getCourses(offset, limit) {
    return this.http.get(`${this.URL}/courses/user?offset=${offset}&limit=${limit}`);
  }
  getPendingCourses(offset, limit) {
    return this.http.get(`${this.URL}/pending-courses/user?offset=${offset}&limit=${limit}`);
  }
  getUserCourse(uid, offset, limit) {
    return this.http.get(`${this.URL}/courses/${uid}?offset=${offset}&limit=${limit}`);
  }
  getCourse(id) {
    return this.http.get(`${this.URL}/course/${id}`);
  }
  getCourseLesson(id) {
    return this.http.get(`${this.URL}/courses/lesson/${id}`);
  }
  completeCourseLesson(courseId, videoId, data) {
    return this.http.put(`${this.URL}/courses/lesson/${courseId}/${videoId}`, data);
  }
  completeCourse(courseId, data) {
    return this.http.put(`${this.URL}/courses/lesson/${courseId}`, data);
  }

  // USER PROGRESS
  completedCourses(offset, limit) {
    return this.http.get(`${this.URL}/courses/watched?offset=${offset}&limit=${limit}`);
  }
  unCompletedCourses(offset, limit) {
    return this.http.get(`${this.URL}/courses/watching?offset=${offset}&limit=${limit}`);
  }

  // COMMENTS
  getCourseLessonComments(courseId, videoId, offset, limit) {
    return this.http.get(`${this.URL}/courses/lesson/${courseId}/${videoId}/comments?offset=${offset}&limit=${limit}`);
  }
  getCourseLessonComment(courseId, videoId, commentId, offset, limit) {
    return this.http.get(`${this.URL}/courses/lesson/${courseId}/${videoId}/comments/${commentId}?offset=${offset}&limit=${limit}`);
  }
  createCourseLessonComments(courseId, videoId, data) {
    return this.http.post(`${this.URL}/courses/lesson/${courseId}/${videoId}/comments`, data);
  }
  replyCourseLessonComments(courseId, videoId, id, data) {
    return this.http.post(`${this.URL}/courses/lesson/${courseId}/${videoId}/comments/${id}`, data);
  }
  editCourseLessonComments(courseId, videoId, id, data) {
    return this.http.put(`${this.URL}/courses/lesson/${courseId}/${videoId}/comments/${id}`, data);
  }
  deleteCourseLessonComments(courseId, videoId, id) {
    return this.http.delete(`${this.URL}/courses/lesson/${courseId}/${videoId}/comments/${id}`);
  }
  likeCourseLessonComments(courseId, videoId, id) {
    return this.http.post(`${this.URL}/courses/lesson/${courseId}/${videoId}/comments/${id}/like`, {});
  }
  unLikeCourseLessonComments(courseId, videoId, id) {
    return this.http.post(`${this.URL}/courses/lesson/${courseId}/${videoId}/comments/${id}/unlike`, {});
  }


  acceptCourse(id) {
    return this.http.post(`${this.URL}/courses/${id}/accept`, {});
  }
  deleteCourse(id) {
    return this.http.delete(`${this.URL}/course/${id}`);
  }

  // RATE
  giveRate(courseId, data) {
    return this.http.post(`${this.URL}/rate/${courseId}`, data);
  }
  getAllRates(offset, limit) {
    return this.http.get(`${this.URL}/rates?offset=${offset}&limit=${limit}`);
  }
  getCourseRates(courseId, offset, limit) {
    return this.http.get(`${this.URL}/rates/${courseId}?offset=${offset}&limit=${limit}`);
  }
}
