import { createAction, props } from '@ngrx/store';
export const getUserActionSuccess = createAction('[USER] Get success', props<{user: any}>());
export const userActionLoading = createAction('[USER] Loading', props<{loading: boolean}>());
export const userActionError = createAction('[USER] Error', props<{error: any}>());