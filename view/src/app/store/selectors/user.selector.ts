import { createSelector } from '@ngrx/store';
import { State } from '../reducers';

const selectUser = ({user}:State) => user;

export const selectUserProperty = createSelector(
  selectUser,
  (state) => state.user,
);
export const selectUserErrorProperty = createSelector(
  selectUser,
  (state) => state.error,
);
export const selectUserLoadingProperty = createSelector(
  selectUser,
  (state) => state.loading,
);