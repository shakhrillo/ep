import { createReducer, on, Action } from "@ngrx/store";
import { 
  getUserActionSuccess, 
  userActionError, 
  userActionLoading, 
} from "../actions/user.action";

export interface UserState {
  user?;
  error?;
  loading?: boolean;
}

const initialState: UserState = {
  loading: true,
  user: null,
  error: null
}

const _userReducer = createReducer(
  initialState,
  on(getUserActionSuccess, (state, { user }) => ({ ...state, user, loading: false })),
  on(userActionError, (state, { error }) => ({ ...state, error, loading: false })),
  on(userActionLoading, (state, { loading }) => ({ ...state, loading })),
);

export function userReducer(state: UserState | undefined, action: Action) {
  return _userReducer(state, action);
}