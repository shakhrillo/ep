import {
  ActionReducer,
  ActionReducerMap,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../../environments/environment';
import { routerReducer } from '@ngrx/router-store';
import { userReducer } from './user.reducer';

export function debug(reducer: ActionReducer<any>): ActionReducer<any> {
  return function(state, action) {
    return reducer(state, action);
  };
}

export interface State {
  router: any,
  user: any,
}

export const reducers: ActionReducerMap<State> = {
  router: routerReducer,
  user: userReducer
};

export const metaReducers: MetaReducer<State>[] = !environment.production ? [debug] : [];