import { Component, OnInit, OnDestroy } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from './services/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import * as firebase from 'firebase/app';
import { SubSink } from 'subsink';
import { Store } from '@ngrx/store';
import { getUserActionSuccess } from './store/actions/user.action';
import { Router } from '@angular/router';
import { UserService } from './services/user.service';
import { MessagesService } from './services/messages.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UploadCourseComponent } from './modals/upload-course/upload-course.component';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { SubscribeComponent } from './vm/user/modals/subscribe/subscribe.component';

@Component({
  selector: 'eplatform-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  public user;
  public loading: boolean = true;
  public messages: number = 0;
  public categories: Array<any> = [];
  public languages: Array<any> = [];
  public notification: number = 0;
  public authors = [];

  public megaMenu:boolean = false;
  public megaMenuToggle$ = new Subject();
  public megaMenuToggle(value) {
    this.megaMenuToggle$.next(value);
  }

  constructor(
    private fireAuth: AngularFireAuth,
    private fireStore: AngularFirestore,
    private fireDatabase: AngularFireDatabase,
    private auth: AuthService,
    private userService: UserService,
    private store: Store<any>,
    private messagesService: MessagesService,
    private router: Router,
    private modalService: NgbModal,
  ) {
    this.megaMenuToggle$.pipe(debounceTime(160)).subscribe((megaMenu:boolean) => this.megaMenu = megaMenu);
    this.fireAuth.onAuthStateChanged(async (state) => {
      if(state) {
        const token = await state.getIdToken();
        this.auth.userToken = token;
        localStorage.setItem('uid', state.uid);

        // GET <User>
        this.subs.add(this.fireStore.collection('_users').doc(state.uid).valueChanges().subscribe(async user => {
          this.user = user;
          this.store.dispatch(getUserActionSuccess({user}));          
        }));

        // STATUS <Online&Offline>
        this.checkUserStatus(state.uid);

        // GOT NOTIFICATION
        this.subs.sink = this.userService.gotNotifications(state.uid).subscribe(notification => {
          if(notification) {
            this.notification = notification['notifications'];
            this.messages = notification['messages']
          }
        });

      } else {
        this.user = null;
        this.subs.unsubscribe();
      }

      this.loading = false;
    });
  }

  private statusCheckRef$;
  private userStatusDatabaseRef;
  checkUserStatus(uid) {
    this.userStatusDatabaseRef = this.fireDatabase.database.ref('/status/' + uid);
    this.statusCheckRef$ = (snapshot) => {
      if (snapshot.val() == false) return;
      this.userStatusDatabaseRef.onDisconnect().set({
        state: 'offline',
        last_changed: firebase.database.ServerValue.TIMESTAMP,
      }).then(async () => {
        await this.userStatusDatabaseRef.set({
          state: 'online',
          last_changed: firebase.database.ServerValue.TIMESTAMP,
        });
      });
    }

    this.fireDatabase.database.ref('.info/connected').on('value', this.statusCheckRef$);
  }

  async logOut() {
    try {
      await this.userStatusDatabaseRef.set({
        state: 'offline',
        last_changed: firebase.database.ServerValue.TIMESTAMP,
      });
      await this.fireAuth.signOut();
      this.subs.unsubscribe();
      this.fireDatabase.database.ref('.info/connected').off('value', this.statusCheckRef$);
    } catch (error) {
      alert(error);
      console.error(error);
      await this.userStatusDatabaseRef.set({
        state: 'online',
        last_changed: firebase.database.ServerValue.TIMESTAMP,
      });
    }
  }

  ngOnInit() {}

  openUploadCourse() {
    if(!this.user) {
      this.router.navigate(['auth/login']);
      return;
    }
    this.modalService.open(UploadCourseComponent, {beforeDismiss: () => false});
  }

  openSubscribe() {
    this.modalService.open(SubscribeComponent);
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
