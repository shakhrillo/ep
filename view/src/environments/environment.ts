// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  stripe: {
    publishable_key: 'pk_test_qeBGYuGwNEHe9TpPgmI1JVxa'
  },
  api: {
    // url: 'http://localhost:3000/api'
    url: 'https://eplatform.club/api'
    // ssh root@134.122.22.69
  },
  firebase: {
    apiKey: "AIzaSyC2CIASVmsmcB_AP9DRmhVZoZ1Yh24xqzM",
    authDomain: "migrants-724c1.firebaseapp.com",
    databaseURL: "https://migrants-724c1.firebaseio.com",
    projectId: "migrants-724c1",
    storageBucket: "migrants-724c1.appspot.com",
    messagingSenderId: "884077861723",
    appId: "1:884077861723:web:3a4a12f87583b4c2a7187e",
    measurementId: "G-09QBNSTW91"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
