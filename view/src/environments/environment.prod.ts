export const environment = {
  production: true,
  stripe: {
    publishable_key: 'pk_test_qeBGYuGwNEHe9TpPgmI1JVxa'
  },
  api: {
    url: 'https://eplatform.club/api'
  },
  firebase: {
    apiKey: "AIzaSyC2CIASVmsmcB_AP9DRmhVZoZ1Yh24xqzM",
    authDomain: "migrants-724c1.firebaseapp.com",
    databaseURL: "https://migrants-724c1.firebaseio.com",
    projectId: "migrants-724c1",
    storageBucket: "migrants-724c1.appspot.com",
    messagingSenderId: "884077861723",
    appId: "1:884077861723:web:3a4a12f87583b4c2a7187e",
    measurementId: "G-09QBNSTW91"
  }
};
