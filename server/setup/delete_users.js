const admin = require("firebase-admin");

const deleteAllUsers = async () => {
  const { users } = await admin.auth().listUsers();
  if(users && users.length > 0) {
    await Promise.all(users.map(async (user) => {
      await admin.auth().deleteUser(user.uid);
    }));
  }

  console.log('Users cleared');
}

module.exports = {
  deleteAllUsers
}