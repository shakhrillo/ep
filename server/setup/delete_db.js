const admin = require("firebase-admin");

const deleteDB = async () => {
  await admin.database().ref('status').set({});
  console.log('Database cleared');
}

module.exports = {
  deleteDB
}