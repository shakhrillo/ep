const config = require('../src/config/config');
const admin = require("firebase-admin");

const createAdmin = async () => {
  const { email, uid, displayName } = await admin.auth().createUser({email: config.admin.email, uid: 'admin', displayName:'ADMIN', password: config.admin.password, emailVerified: true});
  await admin.firestore().collection('_users').doc('admin').set({
    email,
    uid,
    displayName,
    role: 'admin',
    author: 'active',
    createdAt: admin.firestore.FieldValue.serverTimestamp()
  }, {merge: true});
  console.log('Admin created');
}

module.exports = {
  createAdmin
}