const admin = require("firebase-admin");
const data = [{
  image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Angular_full_color_logo.svg/375px-Angular_full_color_logo.svg.png',
  category: 'Angular',
  description: 'Angular (commonly referred to as "Angular 2+" or "Angular v2 and above") is a TypeScript-based open-source web application framework led by the Angular Team at Google and by a community of individuals and corporations. Angular is a complete rewrite from the same team that built AngularJS.',
  id: 'angular'
}, {
  image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/330px-Node.js_logo.svg.png',
  category: 'Node.js',
  description: 'Node.js is an open-source, cross-platform, back-end, JavaScript runtime environment that executes JavaScript code outside a web browser.',
  id: 'node-js'
}];

const createCategories = async () => {
  await Promise.all(data.map(async _ => {
    await admin.firestore().collection('categories').doc(_.id).set(_);
  }));

  console.log('Categories created');
}

module.exports = {
  createCategories
}