const admin = require("firebase-admin");
const deleteCollection = async (path) => {
  const documents = await admin.firestore().collection(path).listDocuments();
  if(documents.length > 0) {
    await Promise.all(documents.map(async (document) => {
      console.log('Deleted path: ', document.path);
      await document.delete();
      await deleteNestedFirestore(document.path);
    }));
  }
}

const deleteNestedFirestore = async (path) => {
  const documents = await (await admin.firestore().doc(path).listCollections()).map(col => col.id);
  if(documents.length > 0) {
    await Promise.all(documents.map(async (_path) => {
      await deleteCollection(path + '/' + _path);
    }));
  }
}

const deleteFirestore = async () => {
  const documents = await (await admin.firestore().listCollections()).map(col => col.id);
  if(documents.length > 0) {
    await Promise.all(documents.map(async (path) => {
      await deleteCollection(path);
    }));
  }
  console.log('Firestore cleared');
}

module.exports = {
  deleteFirestore
}