const config = require('../src/config/config');
const stripe = require('stripe')(config.stripe.secret_key);
const admin = require("firebase-admin");
const reviews = [
  `My first earn money add click site highly recommend auto payout working well. 100% true ad click site. best of the world. thank you starclicks`,
  `From the best gaming sites, actually i found on GAMIVO the best prices for my favorite games. I recommend every body to try it`,
  `I love, love, had a hard time using zip files but, I did manage to unzip them. I’m headed to michaels tomorrow to buy card stock $$`,
  `I was led to believe that I would be sent products to test, not fill out survey after survey with no product reference.`,
  `Really excellent value for money and great service. Have bought from them many times and will continue to do so`,
  `It's the opportunity to learn from the best in an intimate and concentrated fashion. Short videos make it easy to study and meet the teachers who share with levity and excitement.`,
  `Good service, good cheap product.`,
  `Fair price and easy to use website - thanks`,
  `Fast delivery for online shopping.`,
  `They arrived on time and tidied up before they left. In between they were polite and unobtrusive. They answered our questions and showed us how the new system w...`,
  `Doesn't give you the option to deposit with e-wallet meaning i had to deposit and withdraw with card and my current withdraw request has been authorised for 3 d...`
];
const users = [{
  role: 'user',
  email: 'steve.thompson@example.com',
  uid: 'steve_thompson',
  displayName: 'Steve Thompson',
  password: 'aa12345678',
  emailVerified: true,
  country: 'USA',
  city: '8029 Washington Ave',
  info: 'Those who would give up essential liberty to purchase a little temporary safety deserve neither liberty nor safety.',
  photoURL: 'https://randomuser.me/api/portraits/men/73.jpg',
  author: 'active'
}, {
  role: 'user',
  email: 'victoria.bates@example.com',
  uid: 'victoria_bates',
  displayName: 'Victoria Bates',
  password: 'aa12345678',
  emailVerified: true,
  country: 'UK',
  city: '1844 E Little York Rd',
  info: 'Those who would give up essential liberty to purchase a little temporary safety deserve neither liberty nor safety.',
  photoURL: 'https://randomuser.me/api/portraits/women/60.jpg',
  author: 'active'
}, {
  role: 'user',
  email: 'russell.hale@example.com',
  uid: 'russell_hale',
  displayName: 'Russell Hale',
  password: 'aa12345678',
  emailVerified: true,
  country: 'Swiss',
  city: '3971 First Street',
  info: 'Misery no longer loves company. Nowadays it insists on it.',
  photoURL: 'https://randomuser.me/api/portraits/men/28.jpg',
  author: 'active'
}, {
  role: 'user',
  email: 'duane_lawson@example.com',
  uid: 'duane_dawson',
  displayName: 'Duane Lawson',
  password: 'aa12345678',
  emailVerified: true,
  country: 'France',
  city: '6006 W Sherman Dr',
  info: 'Men are like steel. When they lose their temper, they lose their worth.',
  photoURL: 'https://randomuser.me/api/portraits/men/97.jpg',
  author: 'active'
}, {
  role: 'user',
  email: 'phillip.grant@example.com',
  uid: 'phillip_grant',
  displayName: 'Phillip Grant',
  password: 'aa12345678',
  emailVerified: true,
  country: 'UK',
  city: '8275 Hillcrest Rd',
  info: "Wisdom doesn't automatically come with old age. Nothing does - except wrinkles. It's true, some wines improve with age. But only if the grapes were good in the first place.",
  photoURL: 'https://randomuser.me/api/portraits/men/21.jpg'
}, {
  role: 'user',
  email: 'leon.carr@example.com',
  uid: 'leon_carr',
  displayName: 'Leon Carr',
  password: 'aa12345678',
  emailVerified: true,
  country: 'UK',
  city: '4818 Taylor St',
  info: "History teaches us that men and nations behave wisely once they have exhausted all other alternatives.",
  photoURL: 'https://randomuser.me/api/portraits/men/14.jpg'
}, {
  role: 'user',
  email: 'dean.robertson@example.com',
  uid: 'dean_robertson',
  displayName: 'Dean Robertson',
  password: 'aa12345678',
  emailVerified: true,
  country: 'UK',
  city: '6343 Oak Ridge Ln',
  info: "And the day came when the risk to remain tight in a bud was more painful than the risk it took to blossom.",
  photoURL: 'https://randomuser.me/api/portraits/men/65.jpg'
}];

const giveCourseRate = async (courseId, uid, rate, review) => {
  const userRef = admin.firestore().collection('_users').doc(uid);
  const userWatchedCourseRef = userRef.collection('watched_courses').doc(courseId);
  const courseRef = admin.firestore().collection('_courses').doc(courseId);
  const reviewCollection = admin.firestore().collection('_reviews');

  const batch = admin.firestore().batch();
  batch.set(reviewCollection.doc(), {
    review: review,
    course: 'angular_1123',
    rate: rate,
    createdBy: uid,
    createdAt: admin.firestore.FieldValue.serverTimestamp()
  });
  batch.set(userWatchedCourseRef, {
    review: review,
    rate: rate,
  }, { merge: true });
  batch.set(courseRef, {reviews: admin.firestore.FieldValue.arrayUnion({rate: rate})}, {merge: true});
  return await batch.commit();
};

const createUsers = async () => {
  await Promise.all(users.map(async (user, index) => {
    await admin.auth().createUser({
      email: user.email, 
      uid: user.uid, 
      displayName: user.displayName, 
      password: user.password, 
      emailVerified: true
    });

    const { id } = await stripe.customers.create({ email: user.email });
    await admin.firestore().collection('_users').doc(user.uid).set({
      ...user,
      customerId: id,
      createdAt: admin.firestore.FieldValue.serverTimestamp()
    });
    await giveCourseRate('angular_12345', user.uid, 5, reviews[index]);
  }));
  console.log('Fake users created');
}

module.exports = {
  createUsers
}