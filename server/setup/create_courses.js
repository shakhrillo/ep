const admin = require("firebase-admin");

const createCourses = async () => {
  const lessons = [{
    id: 'built-in-directives',
    videoDescription: `<p>Attribute directives listen to and modify the behavior of other HTML elements, attributes, properties, and components. You usually apply them to elements as if they were HTML attributes, hence the name.</p><p>Consider a setCurrentClasses() component method that sets a component property, currentClasses, with an object that adds or removes three classes based on the true/false state of three other component properties. Each key of the object is a CSS class name; its value is true if the class should be added, false if it should be removed.</p>`,
    videoTitle: 'Built-in directives',
    videoURL: 'https://ePlatform.cdn.spotlightr.com/publish/MTAzMDE3OA=='
  }, {
    id: 'two-way-data-binding',
    videoDescription: `<p>Two-way binding gives your app a way to share data between a component class and its template.</p><p>The [()] syntax is easy to demonstrate when the element has a settable property called x and a corresponding event named xChange. Here's a SizerComponent that fits this pattern. It has a size value property and a companion sizeChange event</p>`,
    videoTitle: 'Two-way binding',
    videoURL: 'https://ePlatform.cdn.spotlightr.com/publish/MTAzMDE3OQ=='
  }, {
    id: 'forms-overview',
    videoDescription: `<p>Handling user input with forms is the cornerstone of many common applications. Applications use forms to enable users to log in, to update a profile, to enter sensitive information, and to perform many other data-entry tasks.</p><p>Angular provides two different approaches to handling user input through forms: reactive and template-driven. Both capture user input events from the view, validate the user input, create a form model and data model to update, and provide a way to track changes.</p><p>This guide provides information to help you decide which type of form works best for your situation. It introduces the common building blocks used by both approaches. It also summarizes the key differences between the two approaches, and demonstrates those differences in the context of setup, data flow, and testing.</p>`,
    videoTitle: 'Introduction to forms in Angular',
    videoURL: 'https://ePlatform.cdn.spotlightr.com/publish/MTAzMDE4MA=='
  }, {
    id: 'angular-material',
    videoDescription: `<p>Material Design components for Angular</p>`,
    videoTitle: 'Angular Material',
    videoURL: 'https://ePlatform.cdn.spotlightr.com/publish/MTAzMDE4Mg=='
  }];

  const data = [{
    id: 'node_js_12345',
    image: 'https://nodejs.dev/static/nodejs-logo-light-mode-d8cbf6c670c6befc286bc5c456b20f39.svg',
    title: 'Introduction to Node.js',
    description: `<p>Node.js is an open-source and cross-platform JavaScript runtime environment. It is a popular tool for almost any kind of project!</p><p>Node.js runs the V8 JavaScript engine, the core of Google Chrome, outside of the browser. This allows Node.js to be very performant.</p><p>A Node.js app is run in a single process, without creating a new thread for every request. Node.js provides a set of asynchronous I/O primitives in its standard library that prevent JavaScript code from blocking and generally, libraries in Node.js are written using non-blocking paradigms, making blocking behavior the exception rather than the norm.</p><p>When Node.js performs an I/O operation, like reading from the network, accessing a database or the filesystem, instead of blocking the thread and wasting CPU cycles waiting, Node.js will resume the operations when the response comes back.</p><p>This allows Node.js to handle thousands of concurrent connections with a single server without introducing the burden of managing thread concurrency, which could be a significant source of bugs.</p><p>Node.js has a unique advantage because millions of frontend developers that write JavaScript for the browser are now able to write the server-side code in addition to the client-side code without the need to learn a completely different language.</p><p>In Node.js the new ECMAScript standards can be used without problems, as you don't have to wait for all your users to update their browsers - you are in charge of deciding which ECMAScript version to use by changing the Node.js version, and you can also enable specific experimental features by running Node.js with flags.</p>`,
    tags: ['nodejs', 'express'],
    topic: 'design',
    createdAt: admin.firestore.FieldValue.serverTimestamp(),
    creator: 'steve_thompson',
    active: true
  }, {
    id: 'angular_12345',
    image: 'https://angular.io/assets/images/logos/angular/angular.svg',
    title: 'Learn Angular',
    description: `<p>Angular is a development platform for building mobile and desktop web applications using TypeScript/JavaScript and other languages.</p><p>Learn one way to build applications with Angular and reuse your code and abilities to build apps for any deployment target. For web, mobile web, native mobile and native desktop.</p><p>Achieve the maximum speed possible on the Web Platform today, and take it further, via Web Workers and server-side rendering. Angular puts you in control over scalability. Meet huge data requirements by building data models on RxJS, Immutable.js or another push-model.</p><p>Build features quickly with simple, declarative templates. Extend the template language with your own components and use a wide array of existing components. Get immediate Angular-specific help and feedback with nearly every IDE and editor. All this comes together so you can focus on building amazing apps rather than trying to make the code work.</p><p>From prototype through global deployment, Angular delivers the productivity and scalable infrastructure that supports Google's largest applications.</p>`,
    tags: ['angular', 'material'],
    topic: 'design',
    createdAt: admin.firestore.FieldValue.serverTimestamp(),
    creator: 'phillip_grant',
    active: true
  }, {
    id: 'node_js_67932',
    image: 'https://nodejs.dev/static/nodejs-logo-light-mode-d8cbf6c670c6befc286bc5c456b20f39.svg',
    title: 'Introduction to Node.js',
    description: `<p>Node.js is an open-source and cross-platform JavaScript runtime environment. It is a popular tool for almost any kind of project!</p><p>Node.js runs the V8 JavaScript engine, the core of Google Chrome, outside of the browser. This allows Node.js to be very performant.</p><p>A Node.js app is run in a single process, without creating a new thread for every request. Node.js provides a set of asynchronous I/O primitives in its standard library that prevent JavaScript code from blocking and generally, libraries in Node.js are written using non-blocking paradigms, making blocking behavior the exception rather than the norm.</p><p>When Node.js performs an I/O operation, like reading from the network, accessing a database or the filesystem, instead of blocking the thread and wasting CPU cycles waiting, Node.js will resume the operations when the response comes back.</p><p>This allows Node.js to handle thousands of concurrent connections with a single server without introducing the burden of managing thread concurrency, which could be a significant source of bugs.</p><p>Node.js has a unique advantage because millions of frontend developers that write JavaScript for the browser are now able to write the server-side code in addition to the client-side code without the need to learn a completely different language.</p><p>In Node.js the new ECMAScript standards can be used without problems, as you don't have to wait for all your users to update their browsers - you are in charge of deciding which ECMAScript version to use by changing the Node.js version, and you can also enable specific experimental features by running Node.js with flags.</p>`,
    tags: ['nodejs', 'express'],
    topic: 'design',
    createdAt: admin.firestore.FieldValue.serverTimestamp(),
    creator: 'steve_thompson',
    active: true
  }, {
    id: 'angular_45345',
    image: 'https://angular.io/assets/images/logos/angular/angular.svg',
    title: 'Learn Angular',
    description: `<p>Angular is a development platform for building mobile and desktop web applications using TypeScript/JavaScript and other languages.</p><p>Learn one way to build applications with Angular and reuse your code and abilities to build apps for any deployment target. For web, mobile web, native mobile and native desktop.</p><p>Achieve the maximum speed possible on the Web Platform today, and take it further, via Web Workers and server-side rendering. Angular puts you in control over scalability. Meet huge data requirements by building data models on RxJS, Immutable.js or another push-model.</p><p>Build features quickly with simple, declarative templates. Extend the template language with your own components and use a wide array of existing components. Get immediate Angular-specific help and feedback with nearly every IDE and editor. All this comes together so you can focus on building amazing apps rather than trying to make the code work.</p><p>From prototype through global deployment, Angular delivers the productivity and scalable infrastructure that supports Google's largest applications.</p>`,
    tags: ['angular', 'material'],
    topic: 'design',
    createdAt: admin.firestore.FieldValue.serverTimestamp(),
    creator: 'phillip_grant',
    active: true
  }, {
    id: 'node_js_234234',
    image: 'https://nodejs.dev/static/nodejs-logo-light-mode-d8cbf6c670c6befc286bc5c456b20f39.svg',
    title: 'Introduction to Node.js',
    description: `<p>Node.js is an open-source and cross-platform JavaScript runtime environment. It is a popular tool for almost any kind of project!</p><p>Node.js runs the V8 JavaScript engine, the core of Google Chrome, outside of the browser. This allows Node.js to be very performant.</p><p>A Node.js app is run in a single process, without creating a new thread for every request. Node.js provides a set of asynchronous I/O primitives in its standard library that prevent JavaScript code from blocking and generally, libraries in Node.js are written using non-blocking paradigms, making blocking behavior the exception rather than the norm.</p><p>When Node.js performs an I/O operation, like reading from the network, accessing a database or the filesystem, instead of blocking the thread and wasting CPU cycles waiting, Node.js will resume the operations when the response comes back.</p><p>This allows Node.js to handle thousands of concurrent connections with a single server without introducing the burden of managing thread concurrency, which could be a significant source of bugs.</p><p>Node.js has a unique advantage because millions of frontend developers that write JavaScript for the browser are now able to write the server-side code in addition to the client-side code without the need to learn a completely different language.</p><p>In Node.js the new ECMAScript standards can be used without problems, as you don't have to wait for all your users to update their browsers - you are in charge of deciding which ECMAScript version to use by changing the Node.js version, and you can also enable specific experimental features by running Node.js with flags.</p>`,
    tags: ['nodejs', 'express'],
    topic: 'design',
    createdAt: admin.firestore.FieldValue.serverTimestamp(),
    creator: 'steve_thompson',
    active: true
  }, {
    id: 'angular_345435',
    image: 'https://angular.io/assets/images/logos/angular/angular.svg',
    title: 'Learn Angular',
    description: `<p>Angular is a development platform for building mobile and desktop web applications using TypeScript/JavaScript and other languages.</p><p>Learn one way to build applications with Angular and reuse your code and abilities to build apps for any deployment target. For web, mobile web, native mobile and native desktop.</p><p>Achieve the maximum speed possible on the Web Platform today, and take it further, via Web Workers and server-side rendering. Angular puts you in control over scalability. Meet huge data requirements by building data models on RxJS, Immutable.js or another push-model.</p><p>Build features quickly with simple, declarative templates. Extend the template language with your own components and use a wide array of existing components. Get immediate Angular-specific help and feedback with nearly every IDE and editor. All this comes together so you can focus on building amazing apps rather than trying to make the code work.</p><p>From prototype through global deployment, Angular delivers the productivity and scalable infrastructure that supports Google's largest applications.</p>`,
    tags: ['angular', 'material'],
    topic: 'design',
    createdAt: admin.firestore.FieldValue.serverTimestamp(),
    creator: 'phillip_grant',
    active: true
  }, {
    id: 'node_js_34534534',
    image: 'https://nodejs.dev/static/nodejs-logo-light-mode-d8cbf6c670c6befc286bc5c456b20f39.svg',
    title: 'Introduction to Node.js',
    description: `<p>Node.js is an open-source and cross-platform JavaScript runtime environment. It is a popular tool for almost any kind of project!</p><p>Node.js runs the V8 JavaScript engine, the core of Google Chrome, outside of the browser. This allows Node.js to be very performant.</p><p>A Node.js app is run in a single process, without creating a new thread for every request. Node.js provides a set of asynchronous I/O primitives in its standard library that prevent JavaScript code from blocking and generally, libraries in Node.js are written using non-blocking paradigms, making blocking behavior the exception rather than the norm.</p><p>When Node.js performs an I/O operation, like reading from the network, accessing a database or the filesystem, instead of blocking the thread and wasting CPU cycles waiting, Node.js will resume the operations when the response comes back.</p><p>This allows Node.js to handle thousands of concurrent connections with a single server without introducing the burden of managing thread concurrency, which could be a significant source of bugs.</p><p>Node.js has a unique advantage because millions of frontend developers that write JavaScript for the browser are now able to write the server-side code in addition to the client-side code without the need to learn a completely different language.</p><p>In Node.js the new ECMAScript standards can be used without problems, as you don't have to wait for all your users to update their browsers - you are in charge of deciding which ECMAScript version to use by changing the Node.js version, and you can also enable specific experimental features by running Node.js with flags.</p>`,
    tags: ['nodejs', 'express'],
    topic: 'design',
    createdAt: admin.firestore.FieldValue.serverTimestamp(),
    creator: 'steve_thompson',
    active: true
  }, {
    id: 'angular_1123',
    image: 'https://angular.io/assets/images/logos/angular/angular.svg',
    title: 'Learn Angular',
    description: `<p>Angular is a development platform for building mobile and desktop web applications using TypeScript/JavaScript and other languages.</p><p>Learn one way to build applications with Angular and reuse your code and abilities to build apps for any deployment target. For web, mobile web, native mobile and native desktop.</p><p>Achieve the maximum speed possible on the Web Platform today, and take it further, via Web Workers and server-side rendering. Angular puts you in control over scalability. Meet huge data requirements by building data models on RxJS, Immutable.js or another push-model.</p><p>Build features quickly with simple, declarative templates. Extend the template language with your own components and use a wide array of existing components. Get immediate Angular-specific help and feedback with nearly every IDE and editor. All this comes together so you can focus on building amazing apps rather than trying to make the code work.</p><p>From prototype through global deployment, Angular delivers the productivity and scalable infrastructure that supports Google's largest applications.</p>`,
    tags: ['angular', 'material'],
    topic: 'design',
    createdAt: admin.firestore.FieldValue.serverTimestamp(),
    creator: 'phillip_grant',
    active: true
  }];

  const usersCollection = admin.firestore().collection('_users');
  const coursesCollection = admin.firestore().collection('_courses');
  const batch = admin.firestore().batch();
  
  await Promise.all(data.map(async _ => {
    batch.set(usersCollection.doc(_.creator).collection('courses').doc(_.id), _, { merge: true });
    batch.set(coursesCollection.doc(_.id), _, { merge: true });
    await Promise.all(lessons.map(async lesson => {
      batch.set(coursesCollection.doc(_.id).collection('lessons').doc(lesson.id), {
        course: _.id,
        creator: _.creator,
        ...lesson
      }, { merge: true });
    }));
  }));

  await batch.commit();
  console.log('Fake courses created');
}

module.exports = {
  createCourses
}