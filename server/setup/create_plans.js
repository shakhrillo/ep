const config = require('../src/config/config');
const stripe = require('stripe')(config.stripe.secret_key);
const admin = require("firebase-admin");
const data = [{
  isPro: 'false',
  pros: 'Unlimited access for the free course'
}, {
  isPro: 'false',
  pros: 'Chat with authors and users on realtime'
}, {
  isPro: 'false',
  pros: 'Comments for the each video except premium videos'
}, {
  isPro: 'true',
  pros: 'Unlimited access for the premium course'
}];

const createPlans = async () => {
  const product = await stripe.products.create({
    name: config.plan.name
  });
  const planMonthly = await stripe.plans.create({
    amount: config.plan.month,
    currency: config.plan.currency,
    interval: 'month',
    product: product.id,
  });
  const planYearly = await stripe.plans.create({
    amount: config.plan.year,
    currency: config.plan.currency,
    interval: 'year',
    product: product.id,
  });
  await admin.firestore().collection('_plans').add(planMonthly);
  await admin.firestore().collection('_plans').add(planYearly);

  await Promise.all(data.map(async _ => {
    await admin.firestore().collection('membership').add(_);
  }));

  console.log('Plans created');
}

module.exports = {
  createPlans
}