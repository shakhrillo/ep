const config = require('./src/config/config');
const admin = require("firebase-admin");
admin.initializeApp({
  credential: admin.credential.cert(config.firebase.credential),
  databaseURL: config.firebase.databaseURL
});

// FUNCTIONS
const { deleteAllUsers } = require('./setup/delete_users');
const { deleteDB } = require('./setup/delete_db');
const { deleteFirestore } = require('./setup/delete_firestore');
const { createAdmin } = require('./setup/create_admin');
const { createUsers } = require('./setup/create_users');
const { createCourses } = require('./setup/create_courses');
const { createPlans } = require('./setup/create_plans');
const { createCategories } = require('./setup/create_categories');

// DEEP CLEAN
const deepClean = async () => {
  await deleteDB();
  await deleteFirestore();
  await deleteAllUsers()
}

const init = async () => {
  try {

    // DEEP CLEAN
    await deepClean();
    
    // CREATE ADMIN
    await createAdmin();
    
    // CREATE USERS
    await createUsers();
  
    // CREATE COURSES
    await createCourses();
  
    // CREATE PLANS
    await createPlans();
  
    // CREATE CATEGORIES
    await createCategories();

  } catch (error) {
    console.log(error);
  }

  process.exit();
}

init();