const express = require('express');
const helmet = require('helmet');
const xss = require('xss-clean');
const compression = require('compression');
const cors = require('cors');
const httpStatus = require('http-status');
const morgan = require('./config/morgan');
const routes = require('./routes');
const { errorConverter, errorHandler } = require('./middlewares/error');
const ApiError = require('./utils/ApiError');
const admin = require("firebase-admin");
const bodyParser = require('body-parser');
const config = require('./config/config');

admin.initializeApp({
  credential: admin.credential.cert(config.firebase.credential),
  databaseURL: config.firebase.databaseURL
});

const app = express();

app.use(morgan.successHandler);
app.use(morgan.errorHandler);

app.use(helmet());

app.use((req, res, next) => {
  if (req.originalUrl.indexOf('/webhook') > -1) {
    // bodyParser.raw({type: 'application/json'})(req, res, next);
    next();
  } else {
    bodyParser.json()(req, res, next);
  }
});

// parse urlencoded request body
app.use(express.urlencoded({ extended: !true }));

// sanitize request data
app.use(xss());

// gzip compression
app.use(compression());

// enable cors
app.use(cors());
app.options('*', cors());

app.use('/api', routes);

// send back a 404 error for any unknown api request
app.use((req, res, next) => {
  next(new ApiError(httpStatus.NOT_FOUND, 'Not found'));
});

// convert error to ApiError, if needed
app.use(errorConverter);

// handle error
app.use(errorHandler);

module.exports = app;
