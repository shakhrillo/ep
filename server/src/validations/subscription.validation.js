const Joi = require('@hapi/joi');

const updateCard = {
  body: Joi.object().keys({
    paymentMethod: Joi.string().required()
  })
}

module.exports = {
  updateCard
};
