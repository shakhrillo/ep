const Joi = require('@hapi/joi');

const updateUser = {
  body: Joi.object().keys({
    displayName: Joi.string(),
    country: Joi.any(),
    city: Joi.any(),
    info: Joi.any(),
    photoURL: Joi.any()
  })
}

const updateAuthorPayoutEmail = {
  body: Joi.object().keys({
    payoutEmail: Joi.string().required(),
  })
}

module.exports = {
  updateUser,
  updateAuthorPayoutEmail,
};
