const httpStatus = require('http-status');
const admin = require("firebase-admin");
const moment = require("moment");

const auth = (...userType) => async (req, res, next) => {
  if(req.headers.authorization) {
    const idToken = req.headers.authorization.split(" ")[1];
    try {
      const { uid } = await admin.auth().verifyIdToken(idToken);
      const user = await (await admin.firestore().collection('_users').doc(uid).get()).data();
      if(userType.indexOf(user.role) > -1 || user.role === 'admin') {
        // let isPro = user.role === 'admin' ? true : checkPro(user.subscription_next_billing);
        req.user = {...user, uid};
        next();
      } else {
        return res.status(httpStatus.FORBIDDEN).send('Request is forbidden');
      }
    } catch (error) {
      if(userType.indexOf('public') > -1) {
        next();
      } else {
        return res.status(httpStatus.UNAUTHORIZED).send('error'); 
      }
    }
  } else {
    if(userType.indexOf('public') > -1) {
      next();
    } else {
      return res.status(httpStatus.UNAUTHORIZED).send('Token not valid'); 
    }
  }
};

module.exports = auth;
