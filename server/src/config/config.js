const dotenv = require('dotenv');
const path = require('path');
const Joi = require('@hapi/joi');
const firebase = require('../../firebase.json');
dotenv.config({ path: path.join(__dirname, '../../.env') });

const envVarsSchema = Joi.object()
  .keys({
    PORT: Joi.number().default(3000).required(),
    ADMIN_EMAIL: Joi.string().required(),
    ADMIN_PASSWORD: Joi.string().required(),
    PRODUCT_NAME: Joi.string().required(),
    CURRENCY: Joi.string().required(),
    MONTHLY_AMOUNT: Joi.number().required(),
    YEARLY_AMOUNT: Joi.number().required(),
    AWS_BUCKET_NAME: Joi.string().required(),
    AWS_ACCESS_KEY_ID: Joi.string().required(),
    AWS_SECRET_ACCESS_KEY: Joi.string().required(),
    STRIPE_SECRET_KEY: Joi.string().required(),
    STRIPE_SECRET_KEY: Joi.string().required(),
    STRIPE_ENDPOINT_SECRET: Joi.string().required(),
    SUCCESS_URL: Joi.string().required(),
    CANCEL_URL: Joi.string().required(),
    VOO_KEY: Joi.string().required(),
  })
  .unknown();

const { value: envVars, error } = envVarsSchema.prefs({ errors: { label: 'key' } }).validate(process.env);

if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

module.exports = {
  env: envVars.NODE_ENV,
  port: envVars.PORT,
  plan: {
    name: envVars.PRODUCT_NAME,
    currency: envVars.CURRENCY,
    month: envVars.MONTHLY_AMOUNT,
    year: envVars.YEARLY_AMOUNT
  },
  admin: {
    email: envVars.ADMIN_EMAIL,
    password: envVars.ADMIN_PASSWORD
  },
  stripe: {
    secret_key: envVars.STRIPE_SECRET_KEY,
    endpoint_secret: envVars.STRIPE_ENDPOINT_SECRET,
    success_url: envVars.SUCCESS_URL,
    cancel_url: envVars.CANCEL_URL
  },
  aws: {
    access_key_id: envVars.AWS_ACCESS_KEY_ID,
    secret_access_key: envVars.AWS_SECRET_ACCESS_KEY,
    bucket_name: envVars.AWS_BUCKET_NAME
  },
  firebase,
  spotlightr: {
    voo_key: envVars.VOO_KEY
  }
};
