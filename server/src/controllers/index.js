module.exports.adminController = require('./admin.controller');
module.exports.authController = require('./auth.controller');
module.exports.authorController = require('./author.controller');
module.exports.courseController = require('./course.controller');
module.exports.userController = require('./user.controller');
module.exports.uploadController = require('./upload.controller');
module.exports.messagesController = require('./messages.controller');
module.exports.stripeController = require('./stripe.controller');
