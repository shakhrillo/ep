const catchAsync = require('../utils/catchAsync');
const admin = require("firebase-admin");
const fetch = require('node-fetch');
const FormData = require("form-data");
const config = require("../config/config");
const { v4: uuidv4 } = require("uuid");
const httpStatus = require('http-status');
const createUID = (string) => string.replace(/[^a-z0-9_]+/gi, '-').replace(/^-|-$/g, '').toLowerCase() + Math.ceil(Math.random()*1000000);

const checkPermission = (user, creator) => {
  if(user.uid === creator || user.role === 'admin') {
    return true
  }
  return false;
}

const spotlightrCreateVideo = async (videoURL) => {
  const form = new FormData();
  form.append('vooKey', config.spotlightr.voo_key);
  form.append('name', videoURL.split('amazonaws.com/')[1]);
  form.append('URL', videoURL);
  form.append('customS3', 0);
  form.append('create', 1);
  const response = await fetch('http://api.spotlightr.com/api/createVideo', {method: 'POST', body: form});
  const resText = await response.text();
  return resText.split('}')[1];
}
const getCollectionData = async (ref, limit, offset ) => {
  return await (await ref.orderBy('createdAt').limit(Number(limit)).offset(Number(offset)).get()).docs.map(_ => _.data());
}
const attachUserToCollection = async (array, userObj) => {
  const usersCollection = admin.firestore().collection('_users');
  let user = {};
  array = await Promise.all(array.map(async obj => {
    if(obj && obj[userObj]) {
      if(!user[obj[userObj]]) {
        const userRef = usersCollection.doc(obj[userObj]);
        user[obj[userObj]] = await (await userRef.get()).data();
      }

      return {
        ...obj, [userObj]: user[obj[userObj]]
      }
    }
  }));

  return array;
}
const attachCourseLessons = async (course, isPro = true) => {
  const collectionCourse = admin.firestore().collection('_courses');
  const coursesLessonsCollection = collectionCourse.doc(course.id).collection('lessons');
  const lessons = await (await coursesLessonsCollection.get()).docs.map(lesson => {
    lesson = lesson.data();

    if(!isPro) {
      delete lesson['videoURL'];
    }

    return lesson;
  });
  return {...course, lessons}
}

// PUBLISH NEW COURSES
const submitCourse = catchAsync(async (req, res) => {
  const { course_resource, course_image, video_files } = req.files;

  let { title, description, videos, imageBy = null, tags, topic } = req.body;
  tags = tags && JSON.parse(tags);
  videos = JSON.parse(videos);
  videos.map((video, index) => video.videoURL = video_files[index].location);
  const image = course_image[0].location;
  const resources = course_resource && course_resource.map(({location}) => location);

  // Batch action
  const courseId = createUID(title);
  const courseRef = admin.firestore().collection('_courses').doc(courseId);

  const batch = admin.firestore().batch();
  await Promise.all(videos.map(async (video) => {
    const lessonId = createUID(video['videoTitle']);
    const lessonRef = courseRef.collection('lessons').doc(lessonId);
    let data = {
      ...video,
      id: lessonId,
      course: courseId,
      creator: req.user.uid
    }

    if(req.user.role === 'admin') {
      const videoURL = await spotlightrCreateVideo(video.videoURL);
      data['videoURL'] = videoURL;
    }

    batch.set(lessonRef, data);

  }));

  const course = {
    id: courseId,
    image,
    ...imageBy && {imageBy},
    title, 
    description,
    tags,
    topic,
    ...resources && {resources},
    createdAt: admin.firestore.FieldValue.serverTimestamp(),
    creator: req.user.uid,
    active: req.user.role === 'admin'
  }

  if(req.user.role === 'admin') {
    // Update statistics
    const statisticsId = new Date().getMonth() + '_' + new Date().getFullYear();
    const statisticsRef = admin.firestore().collection('_statistics').doc(statisticsId);
    batch.set(statisticsRef, { accepted_courses: admin.firestore.FieldValue.increment(1) }, { merge: true });
  } else {
    batch.set(admin.firestore().collection('_users').doc('admin').collection('alerts').doc('data'), {
      notifications: admin.firestore.FieldValue.increment(1)
    }, { merge: true });
    batch.set(admin.firestore().collection('_users').doc('admin').collection('notifications').doc(), {
      content: 'pending_course',
      course,
      type: 'requested',
      createdAt: admin.firestore.FieldValue.serverTimestamp(),
    }, { merge: true });
  }

  batch.set(courseRef, course, {merge: true});
  
  await batch.commit();

  res.send({status: 'OK'})
});

// EDIT COURSE
const updateCourse = catchAsync(async (req, res) => {
  let { title, description, imageBy, videos, tags, topic, creator } = req.body;
  
  if(!checkPermission(req.user, creator)) {
    return res.status(httpStatus.FORBIDDEN).send('Request is forbidden');
  };

  tags = tags && JSON.parse(tags);
  videos = JSON.parse(videos);

  const image = req.file && req.file.location;

  // Batch action
  const courseId = req.params.courseId;
  const courseRef = admin.firestore().collection('_courses').doc(courseId);

  const batch = admin.firestore().batch();
  await Promise.all(videos.map(async (video) => {
    const lessonId = video.lessonId;
    const lessonRef = courseRef.collection('lessons').doc(lessonId);
    let data = {
      ...video,
      id: lessonId,
      course: courseId,
    }

    batch.set(lessonRef, data, {merge : true});
  }));

  const courseData = {
    id: courseId,
    ...image && {image},
    ...imageBy && {imageBy},
    title, 
    description,
    tags,
    topic,
    updatedAt: admin.firestore.FieldValue.serverTimestamp(),
  };

  batch.set(courseRef, courseData, {merge: true});
  
  await batch.commit();

  res.send({
    id: courseId,
    ...image && {image},
    ...imageBy && {imageBy},
    title, 
    description,
    tags,
    topic
  });
});

// ACCEPT COURSE
const acceptCourse = catchAsync(async (req, res) => {
  const courseRef = admin.firestore().collection('_courses').doc(req.params.id);
  const collections = courseRef.collection('lessons');
  const videos = await (await collections.get()).docs.map(_ => _.data());
  const course = await (await courseRef.get()).data();

  const batch = admin.firestore().batch();
  await Promise.all(videos.map(async (video) => {
    const videoURL = await spotlightrCreateVideo(video.videoURL);
    batch.set(collections.doc(video.id), { videoURL }, { merge: true });
  }));

  // Update statistics
  const statisticsId = new Date().getMonth() + '_' + new Date().getFullYear();
  const statisticsRef = admin.firestore().collection('_statistics').doc(statisticsId);
  batch.set(statisticsRef, { accepted_courses: admin.firestore.FieldValue.increment(1) }, { merge: true });

  batch.set(courseRef, { active: true }, { merge: true });

  // Update user notification
  batch.set(admin.firestore().collection('_users').doc(course.creator).collection('alerts').doc('data'), {
    notifications: admin.firestore.FieldValue.increment(1)
  }, { merge: true });
  batch.set(admin.firestore().collection('_users').doc(course.creator).collection('notifications').doc(), {
    content: 'course',
    course,
    type: 'accepted',
    createdAt: admin.firestore.FieldValue.serverTimestamp(),
  }, { merge: true });

  await batch.commit();

	res.send({status: 'OK'});
  
});

// ----------
// DELETE [ USER COURSE ]
// ----------
const deleteCourse = catchAsync(async (req, res) => {
  const coursesCollection = admin.firestore().collection('_courses');
  const courseRef = coursesCollection.doc(req.params.id);
  const course = (await courseRef.get()).data();

  if(!checkPermission(req.user, course.creator)) {
    return res.status(httpStatus.FORBIDDEN).send('Request is forbidden');
  };
  
  const batch = admin.firestore().batch();
  
  batch.delete(courseRef);
  const listDocuments = await courseRef.collection('lessons').listDocuments();
  listDocuments.map(async document => {
    batch.delete(document);
  });

  if(course.creator !== req.user.uid) {
    batch.set(admin.firestore().collection('_users').doc(course.creator).collection('alerts').doc('data'), {
      notifications: admin.firestore.FieldValue.increment(1)
    }, { merge: true });
    batch.set(admin.firestore().collection('_users').doc(course.creator).collection('notifications').doc(), {
      content: 'course',
      course,
      type: 'deleted',
      createdAt: admin.firestore.FieldValue.serverTimestamp(),
    }, { merge: true });
  }

  await batch.commit();

  res.send({id: req.params.id});
});

const attachReviews = async course => {
  let reviews = await getCollectionData(admin.firestore().collection('_reviews').where('course', '==', course.id), 5, 0);
  reviews = await attachUserToCollection(reviews, 'createdBy');
  return {...course, reviews}
}

// ACTIVE COURSES
const getAllActiveCourses = catchAsync(async (req, res) => {
  let isPro = false;
  if(req.user) {
    isPro = req.user.pro || req.user.role === 'admin';
  }
  const { limit = 10, offset = 0 } = req.query;
  const coursesCollection = admin.firestore().collection('_courses');
  let courses = await attachUserToCollection((await getCollectionData(coursesCollection.where('active', '==', true), limit, offset)), 'creator');
  courses = await Promise.all(courses.map(async course => {
    if(req.user && !isPro) {
      isPro = req.user.uid === course.creator.uid || req.user.role === 'admin';
    }
    course = await attachCourseLessons(course, isPro);
    return course;
  }));

  res.send(courses);
});

// PENDING COURSES
const getAllPendingCourses = catchAsync(async (req, res) => {
  const { limit = 10, offset = 0 } = req.query;
  const coursesCollection = admin.firestore().collection('_courses');
  let courses = await attachUserToCollection((await getCollectionData(coursesCollection.where('active', '==', false), limit, offset)), 'creator');
  courses = await Promise.all(courses.map(async course => {
    course = await attachCourseLessons(course);
    return course;
  }));

  res.send(courses);
});

// CATEGORY COURSES
const getCategoryCourses = catchAsync(async (req, res) => {
  const { limit = 10, offset = 0 } = req.query;
  const id = req.params.id;
  
  const coursesCollection = admin.firestore().collection('_courses');
  const courses = await attachUserToCollection((await getCollectionData(coursesCollection.where('topic', '==', id).where('active', '==', true), limit, offset)), 'creator');
  
  res.send(courses);
});

// USER COURSES
const getCoursesUser = catchAsync(async (req, res) => {
  const { limit = 10, offset = 0 } = req.query;
  const collectionCourse = admin.firestore().collection('_courses');
  let courses = await attachUserToCollection(await getCollectionData(collectionCourse.where('creator', '==', req.params.uid).where('active', '==', true), limit, offset), 'creator');
  courses = await Promise.all(courses.map(async course => {
    return await attachCourseLessons(course);
  }));
  
  res.send(courses);
});

// USER PENDING COURSES
const getPendingCoursesUser = catchAsync(async (req, res) => {
  const { limit = 10, offset = 0 } = req.query;
  const collectionCourse = admin.firestore().collection('_courses');
  let courses = await attachUserToCollection(await getCollectionData(collectionCourse.where('creator', '==', req.user.uid).where('active', '==', false), limit, offset), 'creator');
  courses = await Promise.all(courses.map(async course => {
    return await attachCourseLessons(course);
  }));
  
  res.send(courses);
});

// GET SINGLE COURSE
const getCourse = catchAsync(async (req, res) => {
  let isPro = false;
  if(req.user) {
    isPro = req.user.pro || req.user.role === 'admin';
  }

  const courseId = req.params.id;
  const courseRef = admin.firestore().collection('_courses').doc(courseId);
  let course = await (await courseRef.get()).data();
  if(req.user && !isPro) {
    isPro = course.creator === req.user.uid || req.user.role === 'admin';
  }
  course = await attachCourseLessons(course, isPro);

  if(course && course.creator) {
    const _userRef = admin.firestore().collection('_users').doc(course.creator);
    const creator = await (await _userRef.get()).data();
    course['creator'] = creator;
  }

  // Check lessons status
  if(req.user) {
    const userRef = admin.firestore().collection('_users').doc(req.user.uid);
    const userWatchedCourseRef = userRef.collection('watched_courses').doc(courseId);
    let coursesStatus = await (await userWatchedCourseRef.get()).data();
    if(coursesStatus && coursesStatus['completed_lessons']) {
      course['completed'] = coursesStatus['courseCompleted'];
      course['lessons'] = course['lessons'].map(lesson => {
        lesson['completed'] = coursesStatus['completed_lessons'].findIndex(_ => _.id === lesson.id) !== -1;
        return lesson;
      })
    }
  }

  res.send(course);
});

// COMPLETE LESSON
const completeLesson = catchAsync(async (req, res) => {
  if(!req.user.pro && req.user.role !== 'admin') {
    return res.status(httpStatus.FORBIDDEN).send('Request is forbidden');
  };
  const status = req.body.status;
  const courseId = req.params.courseId;
  const videoId = req.params.videoId;
  const courseRef = admin.firestore().collection('_courses').doc(courseId);
  const lessonsCollection = courseRef.collection('lessons');
  const userRef = admin.firestore().collection('_users').doc(req.user.uid);
  const userWatchedCourseRef = userRef.collection('watched_courses').doc(courseId);

  // Set lessons status
  const batch = admin.firestore().batch();

  let courseCompleted = false;
  const userWatchedCourses = await (await userWatchedCourseRef.get()).data();
  if(userWatchedCourses && userWatchedCourses['completed_lessons'] && Array.isArray(userWatchedCourses['completed_lessons'])) {
    let _index = userWatchedCourses['completed_lessons'].findIndex(cl => cl.id === videoId);
    if(_index > -1) {
      if(status) {
        userWatchedCourses['completed_lessons'][_index] = { id: videoId }
      } else {
        userWatchedCourses['completed_lessons'] = userWatchedCourses['completed_lessons'].filter(({id}) => id !== videoId);
      }
    } else {
      userWatchedCourses['completed_lessons'].push({ id: videoId });
    }

    courseCompleted = true;
    if(!userWatchedCourses[courseCompleted]) {
      const courseLessons = await (await lessonsCollection.get()).docs.map(e => ({...e.data(), id: e.id}));
      console.log(userWatchedCourses['completed_lessons']);
      courseLessons.map(({id}) => { if(userWatchedCourses['completed_lessons'].findIndex(cl => cl.id === id) === -1) courseCompleted = false; });

      if(courseCompleted) {
        batch.set(courseRef, {'course_completed_users': admin.firestore.FieldValue.increment(1)}, { merge: true });
      }
    }
  } else {
    userWatchedCourses['completed_lessons'] = [{ id: videoId }];
  }

  batch.set(userWatchedCourseRef, {
    completed_lessons: userWatchedCourses['completed_lessons'],
    courseCompleted,
    lastUpdated: admin.firestore.FieldValue.serverTimestamp()
  }, {merge: true});

  await batch.commit();

  res.send({status: 'OK'});
});

// COMPLETED COURSES
const getCompletedCourses = catchAsync(async (req, res) => {
  const userRef = admin.firestore().collection('_users').doc(req.user.uid);
  const userWatchedCourseCollection = userRef.collection('watched_courses');
  const { limit = 10, offset = 0 } = req.query;
  let courses = await (await userWatchedCourseCollection.where('courseCompleted', '==', true).orderBy('lastUpdated').limitToLast(Number(limit)).offset(Number(offset)).get()).docs.map(_ => ({..._.data(), id: _.id}));
  courses = await Promise.all(courses.map(async course => {
    const courseRef = admin.firestore().collection('_courses').doc(course.id);
    const data = await (await courseRef.get()).data();
    return {...data, status: course};
  }));
  courses = await attachUserToCollection(courses, 'creator');
  
  res.send(courses);
});

// IN-PROGRESS COURSES
const getUnCompletedCourses = catchAsync(async (req, res) => {
  const userRef = admin.firestore().collection('_users').doc(req.user.uid);
  const userWatchedCourseCollection = userRef.collection('watched_courses');
  const { limit = 10, offset = 0 } = req.query;
  let courses = await (await userWatchedCourseCollection.where('courseCompleted', '==', false).orderBy('lastUpdated').limitToLast(Number(limit)).offset(Number(offset)).get()).docs.map(_ => ({..._.data(), id: _.id}));
  courses = await Promise.all(courses.map(async course => {
    const courseRef = admin.firestore().collection('_courses').doc(course.id);
    const data = await (await courseRef.get()).data();
    return {...data, status: course};
  }));
  courses = await attachUserToCollection(courses, 'creator');

  res.send(courses);
});

// RATE COURSE
const giveCourseRate = catchAsync(async (req, res) => {
  const courseId = req.params.courseId;
  const userRef = admin.firestore().collection('_users').doc(req.user.uid);
  const userWatchedCourseRef = userRef.collection('watched_courses').doc(courseId);
  const courseRef = admin.firestore().collection('_courses').doc(courseId);
  const reviewCollection = admin.firestore().collection('_reviews');

  const batch = admin.firestore().batch();
  batch.set(reviewCollection.doc(), {
    course: courseId,
    review: req.body.review,
    rate: req.body.rate,
    createdBy: req.user.uid,
    createdAt: admin.firestore.FieldValue.serverTimestamp()
  });
  batch.set(userWatchedCourseRef, {
    review: req.body.review,
    rate: req.body.rate,
  }, { merge: true });
  batch.set(courseRef, {review: admin.firestore.FieldValue.increment(1)}, {merge: true});
  await batch.commit();

  res.send({status: 'OK'});
});

// GET ALL RATES
const getAllRates = catchAsync(async (req, res) => {
  const { limit = 10, offset = 0 } = req.query;
  const reviewCollection = admin.firestore().collection('_reviews');
  let reviews = await attachUserToCollection(await getCollectionData(reviewCollection, limit, offset), 'createdBy');
  reviews = await Promise.all(reviews.map(async review => {
    const course = await (await admin.firestore().collection('_courses').doc(review.course).get()).data();
    return {...review, course};
  }));

  res.send(reviews);
});

// GET COURSE ALL RATES
const getCourseAllRates = catchAsync(async (req, res) => {
  const { limit = 10, offset = 0 } = req.query;
  const reviewCollection = admin.firestore().collection('_reviews').where('course','==', req.params.courseId);
  let reviews = await attachUserToCollection(await getCollectionData(reviewCollection, limit, offset), 'createdBy');
  reviews = await Promise.all(reviews.map(async review => {
    const course = await (await admin.firestore().collection('_courses').doc(review.course).get()).data();
    return {...review, course};
  }));

  res.send(reviews);
});

const getComments = async (collection, limit, offset) => {
  return await (await collection.orderBy('createdAt').limitToLast(Number(limit)).offset(Number(offset)).get()).docs.map(_ => ({..._.data(), commentId: _.id}));
}
const applyRepliesToComment = async (commentsCollection, comments) => {
  let _comment = {};
  let _users = {};
  comments = await Promise.all(comments.map(async (comment, _i) => {
    const repliedTo = comment['repliedTo'];
    if(repliedTo && !_comment[repliedTo]) {

      const commentRef = commentsCollection.doc(repliedTo);
      _comment[repliedTo] = await (await commentRef.get()).data();

      const createdBy = _comment[repliedTo]['createdBy'];
      if(_comment[repliedTo] && createdBy) {
        if(!_users[createdBy]) {
          const authorRef = admin.firestore().collection('_users').doc(createdBy);
          _users[createdBy] = await (await authorRef.get()).data();
        }
  
        return {
          ...comment,
          repliedToComment: {
            ..._comment[repliedTo],
            createdBy: _users[createdBy]
          }
        }
      } else {
        return {
          ...comment
        }
      }

    }
    return {
      ...comment
    }
  }));

  return comments;
}
const checkUserLikedComments = async (commentsCollection, uid, comments) => {
  comments = await Promise.all(comments.map(async (comment) => {
    const liked = (await (commentsCollection.doc(comment.commentId).collection('likes').doc(uid)).get()).data();
    return {...comment, ...liked}
  }));

  return comments;
}

// GET COURSE COMMENTS
const getCourseComments = catchAsync(async (req, res) => {
  const courseCollection = admin.firestore().collection('_courses');
  const courseRef = courseCollection.doc(req.params.courseId);
  const lessonCollection = courseRef.collection('lessons');
  const lessonRef = lessonCollection.doc(req.params.videoId);
  const commentsCollection = lessonRef.collection('comments');

  const { limit = 10, offset = 0 } = req.query;
  let comments = await attachUserToCollection(await getComments(commentsCollection, limit, offset), 'createdBy');
  comments = await applyRepliesToComment(commentsCollection, comments);
  if(req.user) {
    comments = await checkUserLikedComments(commentsCollection, req.user.uid, comments);
  }

  res.send(comments);
});

const attachUser = async (data, uid, obj) => {
  const usersCollection = admin.firestore().collection('_users');
  const userRef = usersCollection.doc(uid);
  const user = (await userRef.get()).data();
  return {...data, [obj]: user}
}
// GET SINGLE COMMENT
const getCourseComment = catchAsync(async (req, res) => {
  const courseCollection = admin.firestore().collection('_courses');
  const courseRef = courseCollection.doc(req.params.courseId);
  const lessonCollection = courseRef.collection('lessons');
  const lessonRef = lessonCollection.doc(req.params.videoId);
  const commentsCollection = lessonRef.collection('comments');

  const commentRef = commentsCollection.doc(req.params.id);
  let comment = await (await commentRef.get()).data();
  comment = await attachUser(comment, comment['createdBy'], 'creator');

  if(comment['repliedTo']) {
    comment['repliedToData'] = await (await commentsCollection.doc(comment['repliedTo']).get()).data();
    if(comment['repliedToData'] && comment['repliedToData']['createdBy']) {
      comment['repliedToData'] = await attachUser(comment['repliedToData'], comment['repliedToData']['createdBy'], 'creator');
    }
  }

  const { limit = 10, offset = 0 } = req.query;
  comment['replies'] = await attachUserToCollection(await getComments(commentsCollection.where('repliedTo', '==', req.params.id), limit, offset), 'createdBy');

  res.send(comment);
});

// CREATE COMMENT
const createCourseComment = catchAsync(async (req, res) => {
  const courseCollection = admin.firestore().collection('_courses');
  const courseRef = courseCollection.doc(req.params.courseId);
  const lessonCollection = courseRef.collection('lessons');
  const lessonRef = lessonCollection.doc(req.params.videoId);
  const commentsCollection = lessonRef.collection('comments');

  const commentId = uuidv4();
  const course = await (await courseRef.get()).data();
  const comment = {
    comment: req.body.comment,
    createdBy: req.user.uid,
    createdAt: admin.firestore.FieldValue.serverTimestamp(),
    commentId
  }

  const batch = admin.firestore().batch();
  batch.set(commentsCollection.doc(commentId), comment);

  batch.set(lessonRef, {
    comments: admin.firestore.FieldValue.increment(1)
  }, { merge: true });

  if(req.user.uid !== course.creator) {
    batch.set(admin.firestore().collection('_users').doc(course.creator).collection('alerts').doc('data'), {
      notifications: admin.firestore.FieldValue.increment(1)
    }, { merge: true });
    batch.set(admin.firestore().collection('_users').doc(course.creator).collection('notifications').doc(commentId), {
      content: 'comment',
      type: 'new',
      course,
      comment: {...comment, commentId},
      courseId: req.params.courseId,
      videoId: req.params.videoId,
      createdAt: admin.firestore.FieldValue.serverTimestamp(),
    }, { merge: true });
  }

  await batch.commit();

  res.send({ status: 'OK' });
});

// REPLY COMMENT
const replyCourseComment = catchAsync(async (req, res) => {
  const courseCollection = admin.firestore().collection('_courses');
  const courseRef = courseCollection.doc(req.params.courseId);
  const lessonCollection = courseRef.collection('lessons');
  const lessonRef = lessonCollection.doc(req.params.videoId);
  const commentsCollection = lessonRef.collection('comments');

  const commentId = uuidv4();
  const course = await (await courseRef.get()).data();
  const comment = {
    comment: req.body.comment,
    createdBy: req.user.uid,
    createdAt: admin.firestore.FieldValue.serverTimestamp(),
    repliedTo: req.params.id,
    commentId
  }

  const batch = admin.firestore().batch();
  batch.set(commentsCollection.doc(commentId), comment);
  batch.set(lessonRef, {
    comments: admin.firestore.FieldValue.increment(1)
  }, { merge: true });
  batch.set(commentsCollection.doc(req.params.id), {
    replies: admin.firestore.FieldValue.increment(1)
  }, { merge: true });

  if(req.user.uid !== course.creator) {
    batch.set(admin.firestore().collection('_users').doc(course.creator).collection('alerts').doc('data'), {
      notifications: admin.firestore.FieldValue.increment(1)
    }, { merge: true });
    batch.set(admin.firestore().collection('_users').doc(course.creator).collection('notifications').doc(commentId), {
      content: 'comment',
      type: 'new',
      course,
      comment: {...comment, commentId},
      courseId: req.params.courseId,
      videoId: req.params.videoId,
      createdAt: admin.firestore.FieldValue.serverTimestamp(),
    }, { merge: true });
  }

  await batch.commit();
  
  res.send({ status: 'OK' });
});

// EDIT COMMENT
const editCourseComment = catchAsync(async (req, res) => {
  const courseCollection = admin.firestore().collection('_courses');
  const courseRef = courseCollection.doc(req.params.courseId);
  const lessonCollection = courseRef.collection('lessons');
  const lessonRef = lessonCollection.doc(req.params.videoId);
  const commentsCollection = lessonRef.collection('comments');
  const commentRef = commentsCollection.doc(req.params.id);
  const comment = await (await commentRef.get()).data();

  if(!checkPermission(req.user, comment.createdBy)) {
    return res.status(httpStatus.FORBIDDEN).send('Request is forbidden');
  };

  await commentRef.set({
    comment: req.body.comment,
    updatedAt: admin.firestore.FieldValue.serverTimestamp()
  }, { merge: true });

  res.send({ status: 'OK' });
});

// DELETE COMMENT
const deleteCourseComment = catchAsync(async (req, res) => {
  const courseCollection = admin.firestore().collection('_courses');
  const courseRef = courseCollection.doc(req.params.courseId);
  const lessonCollection = courseRef.collection('lessons');
  const lessonRef = lessonCollection.doc(req.params.videoId);
  const commentsCollection = lessonRef.collection('comments');
  const commentRef = commentsCollection.doc(req.params.id);
  const comment = await (await commentRef.get()).data();

  if(!checkPermission(req.user, comment.createdBy)) {
    return res.status(httpStatus.FORBIDDEN).send('Request is forbidden');
  };

  await commentRef.delete();

  res.send({ status: 'OK' });
});

// LIKE COMMENT
const likeCourseComment = catchAsync(async (req, res) => {
  const courseCollection = admin.firestore().collection('_courses');
  const courseRef = courseCollection.doc(req.params.courseId);
  const lessonCollection = courseRef.collection('lessons');
  const lessonRef = lessonCollection.doc(req.params.videoId);
  const commentsCollection = lessonRef.collection('comments');
  const commentRef = commentsCollection.doc(req.params.id);
  const likesCollection = commentRef.collection('likes');
  const likeRef = likesCollection.doc(req.user.uid);
  
  const batch = admin.firestore().batch();
  batch.set(commentRef, { like: admin.firestore.FieldValue.increment(1) }, { merge: true })
  batch.set(likeRef, { liked: true }, { merge: true })
  await batch.commit();

  res.send({ status: 'OK' });
});

// UNLIKE COMMENT
const unLikeCourseComment = catchAsync(async (req, res) => {
  const courseCollection = admin.firestore().collection('_courses');
  const courseRef = courseCollection.doc(req.params.courseId);
  const lessonCollection = courseRef.collection('lessons');
  const lessonRef = lessonCollection.doc(req.params.videoId);
  const commentsCollection = lessonRef.collection('comments');
  const commentRef = commentsCollection.doc(req.params.id);
  const likesCollection = commentRef.collection('likes');
  const likeRef = likesCollection.doc(req.user.uid);

  const batch = admin.firestore().batch();
  batch.set(commentRef, { like: admin.firestore.FieldValue.increment(-1) }, { merge: true })
  batch.delete(likeRef);
  await batch.commit();

  res.send({ status: 'OK' });
});

module.exports = {
  getAllActiveCourses,
  getAllPendingCourses,
  submitCourse,
  updateCourse,
  getCourse,
  deleteCourse,
  acceptCourse,
  completeLesson,
  getCompletedCourses,
  getUnCompletedCourses,
  giveCourseRate,
  getAllRates,
  getCourseAllRates,
  getCoursesUser,
  getPendingCoursesUser,
  getCategoryCourses,
  getCourseComments,
  getCourseComment,
  createCourseComment,
  replyCourseComment,
  editCourseComment,
  deleteCourseComment,
  likeCourseComment,
  unLikeCourseComment
};
