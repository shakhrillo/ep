var AES = require("crypto-js/aes");

const catchAsync = require('../utils/catchAsync');
const admin = require("firebase-admin");

const createMessage = catchAsync(async (req, res) => {
  const encryptedMsg = AES.encrypt(req.body.message, "**").toString();

  const data = {
    creator: req.user.uid,
    receiver: req.params.uid,
    message: encryptedMsg,
    created: admin.firestore.FieldValue.serverTimestamp()
  }

  const refSenderMessages = admin.firestore().collection('_users').doc(req.user.uid).collection('messages');
  const refSenderMessagesDoc = refSenderMessages.doc(req.params.uid);
  const refSender = refSenderMessagesDoc.collection('messages');

  const refReceiverMessages = admin.firestore().collection('_users').doc(req.params.uid).collection('messages');
  const refReceiverMessagesDoc = refReceiverMessages.doc(req.user.uid);
  const refReceiver = refReceiverMessagesDoc.collection('messages');

  const batch = admin.firestore().batch();

  // Set unreads to reciever
  batch.set(admin.firestore().collection('_users').doc(req.params.uid).collection('alerts').doc('data'), {
    messages: admin.firestore.FieldValue.increment(1)
  }, { merge: true });
  
  // Set latest messages
  batch.set(refSenderMessagesDoc, {...data, id: req.params.uid}, {merge: true});
  batch.set(refReceiverMessagesDoc, {...data, unreads: admin.firestore.FieldValue.increment(1), id: req.user.uid}, {merge: true});
  
  // Add messages to sender
  batch.set(refSender.doc(), data);
  // Add messages to reciever if reciever not current user
  if(req.user.uid !== req.params.uid) {
    batch.set(refReceiver.doc(), data);
  }

  await batch.commit();

  res.send({status: 'OK'});
});

const asRead = catchAsync(async (req, res) => {

  const { unreads } = await (await admin.firestore().collection('_users').doc(req.user.uid).collection('messages').doc(req.params.uid).get()).data();
  if(unreads > 0) {
    const batch = admin.firestore().batch();
    batch.set(admin.firestore().collection('_users').doc(req.user.uid).collection('messages').doc(req.params.uid), {unreads: 0}, {merge: true});
    batch.set(admin.firestore().collection('_users').doc(req.user.uid).collection('alerts').doc('data'), {messages: admin.firestore.FieldValue.increment(-unreads)}, {merge: true});
    await batch.commit();
  }

  res.send({status: 'OK'})
});

module.exports = {
  createMessage,
  asRead
};
