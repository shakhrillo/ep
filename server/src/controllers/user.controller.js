const catchAsync = require('../utils/catchAsync');
const admin = require("firebase-admin");

const getCollectionData = async (collection, limit, offset ) => {
  return await (await collection.orderBy('createdAt', 'desc').limit(Number(limit)).offset(Number(offset)).get()).docs.map(_ => _.data());
}

const getRefData = async (ref) => await (await ref.get()).data();

const attachUserToCollection = async (array, userObj) => {
  const usersCollection = admin.firestore().collection('_users');
  let user = {};
  array = await Promise.all(array.map(async obj => {
    if(obj && obj[userObj]) {
      if(!user[obj[userObj]]) {
        const userRef = usersCollection.doc(obj[userObj]);
        user[obj[userObj]] = await (await userRef.get()).data();
      }

      return {
        ...obj, [userObj]: user[obj[userObj]]
      }
    }
  }));

  return array;
}

// NOTIFICATIONS
const getNotifications = catchAsync(async (req, res) => {
  const { limit = 10, offset = 0 } = req.query;
  const userRef = admin.firestore().collection('_users').doc(req.user.uid);
  const notificationsCollection = userRef.collection('notifications');

  await userRef.collection('alerts').doc('data').set({notifications: 0}, {merge: true});
  const notifications = await getCollectionData(notificationsCollection, limit, offset);
  
  res.send(notifications);
});

// GET USERS
const getUsers = catchAsync(async (req, res) => {
  const { limit = 10, offset = 0 } = req.query;
  const userCollection = admin.firestore().collection('_users');
  const users = await getCollectionData(userCollection, limit, offset);;

  res.send(users);
});

// GET USER
const getUser = catchAsync(async (req, res) => {
  const userRef = admin.firestore().collection('_users').doc(req.params.id);
  const users = await getRefData(userRef);

  res.send(users);
});

// DELETE USER
const deleteUser = catchAsync(async (req, res) => {
  const userRef = admin.firestore().collection('_users').doc(req.params.id);
  await userRef.delete();

  res.send({id: req.params.id});
});

// DELETE PHOTO USER
const deleteUserPhoto = catchAsync(async (req, res) => {
  const userRef = admin.firestore().collection('_users').doc(req.user.uid);
  await admin.auth().updateUser(req.user.uid, { photoURL: 'https://eplatform.club/image'});
  await userRef.set({ photoURL: '' }, {merge: true});

  res.send({ status: 'OK' });
});

// UPDATE USER
const updateUser = catchAsync(async (req, res) => {
  let { uid, photoURL } = req.user;
  const userRef = admin.firestore().collection('_users').doc(uid);
  if(req.file && Object.keys(req.file).length > 0) { photoURL = req.file.location };
  await userRef.set({...req.body, photoURL}, { merge: true });

  res.send({ status: 'OK' });
});

// USER PAYOUT EMAIL SET
const updateUserPayoutEmail = catchAsync(async (req, res) => {
  let { uid } = req.user;
  const userRef = admin.firestore().collection('_users').doc(uid);
  await userRef.set({ payoutEmail: req.body.payoutEmail }, {merge: true});

  res.send({ status: 'OK' });
});

// INITIAL WATCH TIME
const initialWatchTime = catchAsync(async (req, res) => {
  const userRef = admin.firestore().collection('_users').doc(req.user.uid);
  const watchedCourses = getRefData(userRef.collection('watched_courses').doc(req.params.courseId));

  if(!watchedCourses || !watchedCourses['lastUpdated']) {
    const batch = admin.firestore().batch();
    
    batch.set(userRef.collection('watched_courses').doc(req.params.courseId), { courseCompleted: false, lastUpdated: admin.firestore.FieldValue.serverTimestamp() }, { merge: true });
    await batch.commit();
  }


  res.send({status: 'OK'});
});

// UPDATE WATCH TIME
const updateWatchTime = catchAsync(async (req, res) => {

  const statisticsId = new Date().getMonth() + '_' + new Date().getFullYear();
  const statisticsRef = admin.firestore().collection('_statistics').doc(statisticsId);
  const watchedLessonsAuthorRef = admin.firestore().collection('_watched_lessons').doc(statisticsId);

  const authorRef = admin.firestore().collection('_users').doc(req.params.author);
  const authorWatchedLessons = authorRef.collection('statistics').doc(statisticsId);

  const batch = admin.firestore().batch();


  // Update statistics
  batch.set(statisticsRef, { watched_lessons: admin.firestore.FieldValue.increment(1) }, { merge: true });
  batch.set(watchedLessonsAuthorRef, { [req.params.author]: admin.firestore.FieldValue.increment(1) }, { merge: true });
  batch.set(authorWatchedLessons, { watched_lessons: admin.firestore.FieldValue.increment(1) }, { merge: true });

  await batch.commit();

  res.send({status: 'OK'});
});

// LANDING PAGE GET
const getLandingPageData = catchAsync(async (req, res) => {
  let completed_courses = [];
  let in_progress_courses = [];

  if(req.user) {
    const userRef = admin.firestore().collection('_users').doc(req.user.uid);
    const userWatchedCourseCollection = userRef.collection('watched_courses');

    const completedCourses = await (await userWatchedCourseCollection.where('courseCompleted', '==', true).orderBy('lastUpdated').limitToLast(5).get()).docs.map(_ => ({..._.data(), id: _.id}));
    const inProgressCourses = await (await userWatchedCourseCollection.where('courseCompleted', '==', false).orderBy('lastUpdated').limitToLast(5).get()).docs.map(_ => ({..._.data(), id: _.id}));

    completed_courses = await Promise.all(completedCourses.map(async course => {
      const courseRef = admin.firestore().collection('_courses').doc(course.id);
      const data = await (await courseRef.get()).data();
      return {...data, status: course};
    }));
    completed_courses = await attachUserToCollection(completed_courses, 'creator');

    in_progress_courses = await Promise.all(inProgressCourses.map(async course => {
      const courseRef = admin.firestore().collection('_courses').doc(course.id);
      const data = await (await courseRef.get()).data();
      return {...data, status: course};
    }));
    in_progress_courses = await attachUserToCollection(in_progress_courses, 'creator');
  }

  res.send({completed_courses, in_progress_courses})
});

// GET SUBSCRIPTION CURRENT USER
const getUserSubscription = catchAsync(async (req, res) => {
  const userRef = admin.firestore().collection('_users').doc(req.user.uid);
  const userSubscriptionRef = userRef.collection('subscription').doc('current');
  const userPaymentMethodRef = userRef.collection('payment_method').doc('card');
  const subscription = await (await userSubscriptionRef.get()).data();
  const paymenth_method = await (await userPaymentMethodRef.get()).data();

  res.send({subscription, paymenth_method});
});

// GET CURRENT USER EARNINGS
const getUserEarning = catchAsync(async (req, res) => {
  const royalty_percent = 30;
  const watchedLessonsRef = admin.firestore().collection('_watched_lessons').doc(req.params.id);
  const statisticsRef = admin.firestore().collection('_statistics').doc(req.params.id);

  const calculate = (monthly_revenue, author_segments, total_segments) => {
    return monthly_revenue  * ( author_segments / total_segments ) * ((100 - royalty_percent)/100);
  }

  let result = {
    earnings: 0
  };
  
  const watchedLessonsData = await (await watchedLessonsRef.get()).data();
  const statisticsData = await (await statisticsRef.get()).data();
  if(statisticsData && typeof statisticsData === 'object' && watchedLessonsData) {
    const earnings_amount = statisticsData.earnings_amount / 100;
    const watched_lessons = statisticsData.watched_lessons;

    const watchedLessonsDataKeys = Object.keys(watchedLessonsData);
    watchedLessonsDataKeys.map(w_key => {
      if(w_key === req.user.uid) {
        result = {
          earnings: calculate(earnings_amount, watchedLessonsData[w_key], watched_lessons)
        }
      }
    });
  }

  res.send(result);
});

module.exports = {
  getUserEarning,
  getNotifications,
  deleteUserPhoto,
  getUsers,
  getUser,
  deleteUser,
  updateUser,
  updateUserPayoutEmail,
  updateWatchTime,
  initialWatchTime,
  getLandingPageData,
  getUserSubscription,
};
