const catchAsync = require('../utils/catchAsync');
const admin = require("firebase-admin");
const getCollectionData = async (ref, limit, offset ) => {
  return await (await ref.orderBy('createdAt').limit(Number(limit)).offset(Number(offset)).get()).docs.map(_ => _.data());
}
const createID = (string) => string.replace(/[^a-z0-9_]+/gi, '-').replace(/^-|-$/g, '').toLowerCase();

// STATISTICS
const getStatistics = catchAsync(async (req, res) => {
  const statisticsCollection = admin.firestore().collection('_statistics');
  const statistics = await (await statisticsCollection.get()).docs.map(_ => ({..._.data(), id: _.id}));
  
  res.send(statistics);
});

// MEMBERSHIP
const createMembership = catchAsync(async (req, res) => {
  const collection = admin.firestore().collection('membership');
  await collection.add(req.body);
  res.send({status: 'OK'});
});
const getMembership = catchAsync(async (req, res) => {
  const collection = admin.firestore().collection('membership');
  const membership = await (await collection.get()).docs.map(_ => ({..._.data(), id: _.id}));
  res.send(membership);
});
const deleteMembership = catchAsync(async (req, res) => {
  const ref = admin.firestore().collection('membership').doc(req.params.id);
  await ref.delete();
  res.send({status: 'OK'});
});

// CATEGORIES
const createCategory = catchAsync(async (req, res) => {
  const image = req.file.location;
  let id = createID(req.body.category);
  await admin.firestore().collection('categories').doc(id).set({
    image,
    category: req.body.category,
    description: req.body.description,
    id
  });

  res.send({
    id,
    image,
    category: req.body.category,
    description: req.body.description
  });
});
const deleteCategory = catchAsync(async (req, res) => {
  await admin.firestore().collection('categories').doc(req.params.id).delete();
  res.send({status: 'OK'});
});
const getCategories = catchAsync(async (req, res) => {
  const categories = await (await admin.firestore().collection('categories').get()).docs.map(_ => ({..._.data(), id: _.id}));
  res.send(categories);
});

// AUTHORS
const getAuthors = catchAsync(async (req, res) => {
  const { limit = 10, offset = 0 } = req.query;
  const userRef = admin.firestore().collection('_users');
  const authors = await getCollectionData(userRef.where('author', '==', 'active'), limit, offset);;

  const users = await Promise.all(authors.map(async user => {
    const author_data = await (await userRef.doc(user.uid).collection('author_data').doc('data').get()).data();

    return {
      ...user, author_data
    }
  }))

  res.send(users);
});

// PENDING AUTHORS
const getPendingAuthors = catchAsync(async (req, res) => {
  const { limit = 10, offset = 0 } = req.query;
  const userRef = admin.firestore().collection('_users');
  const authors = await getCollectionData(userRef.where('author', '==', 'pending'), limit, offset);;

  const users = await Promise.all(authors.map(async user => {
    const author_data = await (await userRef.doc(user.uid).collection('author_data').doc('data').get()).data();

    return {
      ...user, author_data
    }
  }))

  res.send(users);
});

// CALCULATE EARNINGS OF THE AUTHORS
const calculateEarningsOfAuthors = catchAsync(async (req, res) => {
  const royalty_percent = 30;
  const watchedLessonsRef = admin.firestore().collection('_watched_lessons').doc(req.params.id);
  const statisticsRef = admin.firestore().collection('_statistics').doc(req.params.id);

  const calculate = (monthly_revenue, author_segments, total_segments) => {
    return monthly_revenue  * ( author_segments / total_segments ) * ((100 - royalty_percent)/100);
  }

  let result = [];
  
  const watchedLessonsData = await (await watchedLessonsRef.get()).data();
  const statisticsData = await (await statisticsRef.get()).data();
  if(statisticsData && typeof statisticsData === 'object') {
    const earnings_amount = statisticsData.earnings_amount / 100;
    const watched_lessons = statisticsData.watched_lessons;

    const watchedLessonsDataKeys = Object.keys(watchedLessonsData);
    watchedLessonsDataKeys.map(w_key => {
      result.push({
        author: w_key,
        earnings: calculate(earnings_amount, watchedLessonsData[w_key], watched_lessons)
      });
    });
  }

  result = await Promise.all(result.map(async _ => {
    const userRef = admin.firestore().collection('_users').doc(_.author);
    const { email, displayName } = await (await userRef.get()).data();
    return {..._, email, displayName}
  }));

  res.send(result);
});

module.exports = {
  calculateEarningsOfAuthors,
  getStatistics,
  getCategories,
  createCategory,
  deleteCategory,
  getAuthors,
  getPendingAuthors,
  createMembership,
  getMembership,
  deleteMembership
};
