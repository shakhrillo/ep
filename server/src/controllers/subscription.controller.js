const catchAsync = require('../utils/catchAsync');
const admin = require("firebase-admin");
const config = require('../config/config');
const stripe = require('stripe')(config.stripe.secret_key);

const createCheckoutSession = async (req, res) => {
  try {
    const { id } = await stripe.checkout.sessions.create({
      payment_method_types: ['card'],
      customer: req.user.customerId,
      line_items: [{
        price: req.body.plan,
        quantity: 1,
      }],
      mode: 'subscription',
      success_url: config.stripe.success_url,
      cancel_url: config.stripe.cancel_url
    });
    res.send({sessionId: id});
  } catch (error) {
    res.status(400).send(error && ( error.raw || error ));
  }
};

const cancelSubscription = async (req, res) => {
  try {
    await stripe.subscriptions.update(req.params.id, { cancel_at_period_end: true });
    res.send({status: 'OK'});
  } catch (error) {
    res.status(400).send(error && ( error.raw || error ));
  }
};

const reActivateSubscription = async (req, res) => {
  try {
    await stripe.subscriptions.update(req.params.id, { cancel_at_period_end: false });
    res.send({status: 'OK'});
  } catch (error) {
    res.status(400).send(error && ( error.raw || error ));
  }
};

const updateCard = async (req, res) => {
  const default_payment_method = req.body.paymentMethod;
  const customerId = req.user.customerId;
  try {
    await stripe.paymentMethods.attach(default_payment_method, { customer: customerId });
    await stripe.subscriptions.update(req.params.id, { default_payment_method });
    res.send({status: 'OK'});
  } catch (error) {
    res.status(400).send(error && ( error.raw || error ));
  }
}

const getPlans = catchAsync(async (req, res) => {
  const plansCollection = admin.firestore().collection('_plans');
  const plans = await (await plansCollection.get()).docs.map(_ => _.data());
  res.send(plans);
});

const getInvoices = catchAsync(async (req, res) => {
  const { limit = 10, offset = 0 } = req.query;
  const userRef = admin.firestore().collection('_users').doc(req.user.uid);
  const invoicesRef = userRef.collection('invoices');
  const invoices = await (await invoicesRef.orderBy('created').limit(Number(limit)).offset(Number(offset)).get()).docs.map(_ => _.data());

  res.send(invoices);
});

module.exports = {
  createCheckoutSession,
  cancelSubscription,
  reActivateSubscription,
  updateCard,
  getPlans,
  getInvoices
};
