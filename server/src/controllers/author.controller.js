const catchAsync = require('../utils/catchAsync');
const admin = require("firebase-admin");

const createAuthor = catchAsync(async (req, res) => {
  const { uid, author } = req.user;
  
  const userRef = admin.firestore().collection('_users').doc(uid);
  const dataRef = userRef.collection('author_data').doc('data');
  const statisticsRef = admin.firestore().collection('board').doc('statistics');

  if(author !== 'pending' && author !== 'active') {
    const { taxDocument, userIdentity } = req.files;
    
    const batch = admin.firestore().batch();
    
    // Update user
    await batch.set(userRef, {
      author: 'pending'
    }, {merge: true});
    
    // Update statistics
    await batch.set(statisticsRef, {
      authors_pending: admin.firestore.FieldValue.increment(1)
    }, {merge: true});

    // Create author data
    await batch.set(dataRef, {
      taxDocument: taxDocument[0].location,
      userIdentity: userIdentity[0].location,
      authorRequestInfo: req.body.info,
      createdAt: admin.firestore.FieldValue.serverTimestamp(),
      status: 'pending'
    });

    batch.set(admin.firestore().collection('_users').doc('admin').collection('alerts').doc('data'), {
      notifications: admin.firestore.FieldValue.increment(1)
    }, { merge: true });

    batch.set(admin.firestore().collection('_users').doc('admin').collection('notifications').doc(), {
      content: 'author',
      user: uid,
      type: 'requested',
      createdAt: admin.firestore.FieldValue.serverTimestamp(),
    }, { merge: true });

    await batch.commit();

    res.send({ status: 'OK'});
  } else {
    res.status(401).send({ error: 'Your request is under review or already accepted'});
  }
});

const acceptAuthor = catchAsync(async (req, res) => {

  const { uid } = req.params;
  
  const userRef = admin.firestore().collection('_users').doc(uid);

  const batch = admin.firestore().batch();
    
  // Update user
  await batch.set(userRef, {
    author: 'active'
  }, {merge: true});
  
  // Update statistics
  const statisticsId = new Date().getMonth() + '_' + new Date().getFullYear();
  const statisticsRef = admin.firestore().collection('_statistics').doc(statisticsId);
  batch.set(statisticsRef, { authors: admin.firestore.FieldValue.increment(1) }, { merge: true });

  // Update user notification
  await batch.set(userRef.collection('board').doc('statistics'), {
    notifications: admin.firestore.FieldValue.increment(1),
  }, {merge: true});

  // User send notification
  await batch.set(userRef.collection('notifications').doc(), {
    type: 'success',
    content: 'author'
  }, {merge: true});

  await batch.commit();
  
  res.send({ status: 'OK'});
});

const rejectAuthor = catchAsync(async (req, res) => {
  const { uid } = req.params;
  
  const userRef = admin.firestore().collection('_users').doc(uid);
  const statisticsRef = admin.firestore().collection('board').doc('statistics');

  const batch = admin.firestore().batch();
    
  // Update user
  await batch.set(userRef, {
    author: 'rejected'
  }, {merge: true});
  
  // Update statistics
  await batch.set(statisticsRef, {
    authors_pending: admin.firestore.FieldValue.increment(-1),
  }, {merge: true});

  // Update user notification
  await batch.set(userRef.collection('board').doc('statistics'), {
    notifications: admin.firestore.FieldValue.increment(1),
  }, {merge: true});

  // User send notification
  await batch.set(userRef.collection('notifications').doc(), {
    type: 'warning',
    content: 'author'
  }, {merge: true});

  await batch.commit();
  
  res.send({ status: 'OK'});
});


module.exports = {
  createAuthor,
  acceptAuthor,
  rejectAuthor
};
