var aws = require('aws-sdk');
var multer = require('multer');
var multerS3 = require('multer-s3');
const config = require('../config/config');

var s3 = new aws.S3({
  accessKeyId: config.aws.access_key_id,
  secretAccessKey: config.aws.secret_access_key,
});

const uploadImage = multer({
  storage: multerS3({
    s3: s3,
    bucket: config.aws.bucket_name,
    acl: 'public-read',
    metadata: function (req, file, cb) {
      cb(null, {fieldName: file.fieldname});
    },
    key: function (req, file, cb) {
      cb(null, Date.now().toString() + file.originalname);
    },
  })
});

module.exports = {
  uploadImage
};