const admin = require("firebase-admin");
const { firebase } = require("../config/config");
const config = require('../config/config');
const stripe = require('stripe')(config.stripe.secret_key);
const endpointSecret = config.stripe.endpoint_secret;


const getUID = async (customer) => {
  let uid;
  await (await admin.firestore().collection('_users').where('customerId', '==', customer).get()).docs.map(e => uid = e.id);
  return uid;
}

const subscriptionUpdated = async (event) => {
  const { customer } = event.data.object;
  let uid = await getUID(customer);
  
  await admin.firestore().collection('_users').doc(uid).collection('subscription').doc('current').set(event.data.object);
}

const subscriptionCanceled = async (event) => {
  const { customer } = event.data.object;
  let uid = await getUID(customer);
  
  let batch = admin.firestore().batch();
  batch.set(admin.firestore().collection('_users').doc(uid), {pro: false}, {merge: true});
  batch.set(admin.firestore().collection('_users').doc(uid).collection('subscription').doc('current'), event.data.object, {merge: true});

  batch.set(admin.firestore().collection('_users').doc(uid).collection('alerts').doc('data'), {
    notifications: admin.firestore.FieldValue.increment(1)
  }, { merge: true });
  batch.set(admin.firestore().collection('_users').doc(uid).collection('notifications').doc(), {
    content: 'subscription',
    type: 'canceled',
    createdAt: admin.firestore.FieldValue.serverTimestamp(),
  }, { merge: true });

  await batch.commit(); 
}

const paymentMethodUpdated = async (event) => {
  const { customer, card } = event.data.object;
  let uid = await getUID(customer);
  
  let batch = admin.firestore().batch();
  batch.set(admin.firestore().collection('_users').doc(uid).collection('payment_method').doc('card'), {
    brand: card.brand,
    exp_year: card.exp_year,
    last4: card.last4,
    country: card.country,
  }, { merge: true });
  batch.set(admin.firestore().collection('_users').doc(uid).collection('alerts').doc('data'), {
    notifications: admin.firestore.FieldValue.increment(1)
  }, { merge: true });
  batch.set(admin.firestore().collection('_users').doc(uid).collection('notifications').doc(), {
    content: 'card',
    type: 'added',
    createdAt: admin.firestore.FieldValue.serverTimestamp(),
  }, { merge: true });

  await batch.commit();
}

const paymentMethodDetached = async (event) => {
  const { customer } = event.data.object;
  let uid = await getUID(customer);

  let batch = admin.firestore().batch();
  batch.delete(admin.firestore().collection('_users').doc(uid).collection('payment_method').doc('card'));
  batch.set(admin.firestore().collection('_users').doc(uid).collection('alerts').doc('data'), {
    notifications: admin.firestore.FieldValue.increment(1)
  }, { merge: true });
  batch.set(admin.firestore().collection('_users').doc(uid).collection('notifications').doc(), {
    content: 'card',
    type: 'deleted',
    createdAt: admin.firestore.FieldValue.serverTimestamp(),
  }, { merge: true });

  await batch.commit();
}

const invoicePaid = async (event) => {
  const { customer } = event.data.object;
  let uid = await getUID(customer);
  const userRef = admin.firestore().collection('_users').doc(uid);
  const invoicesRef = userRef.collection('invoices').doc();
  const notificationsRef = userRef.collection('notifications').doc();
  const alertsRef = userRef.collection('alerts').doc('data');
  const statisticsId = new Date().getMonth() + '_' + new Date().getFullYear();
  const statisticsRef = admin.firestore().collection('_statistics').doc(statisticsId);
  const currentTime = admin.firestore.FieldValue.serverTimestamp();

  const batch = admin.firestore().batch();

  // Update statistics
  batch.set(statisticsRef, { 
    earnings_amount: admin.firestore.FieldValue.increment(event.data.object.amount_paid), 
    earning_currency: event.data.object.currency,
    become_pro: admin.firestore.FieldValue.increment(1)
  }, { merge: true });

  // Update user account
  batch.set(userRef, {pro: true}, {merge: true});
  
  // Invoices
  batch.set(invoicesRef, event.data.object, {merge: true});
  
  // Notifications
  batch.set(alertsRef, { notifications: admin.firestore.FieldValue.increment(1) }, { merge: true });
  batch.set(notificationsRef, { content: 'payment', type: 'success', createdAt: currentTime }, { merge: true });
  batch.set(notificationsRef, { content: 'subscription', type: 'active', createdAt: currentTime }, { merge: true });

  await batch.commit();
}

const invoicePaidFail = async (event) => {
  const { customer } = event.data.object;
  let uid = await getUID(customer);

  let batch = admin.firestore().batch();
  batch.set(admin.firestore().collection('_users').doc(uid), {pro: false}, {merge: true});
  batch.set(admin.firestore().collection('_users').doc(uid).collection('invoices'), event.data.object);
  batch.set(admin.firestore().collection('_users').doc(uid).collection('alerts').doc('data'), {
    notifications: admin.firestore.FieldValue.increment(1)
  }, { merge: true });
  batch.set(admin.firestore().collection('_users').doc(uid).collection('notifications').doc(), {
    content: 'subscription',
    type: 'canceled',
    createdAt: admin.firestore.FieldValue.serverTimestamp(),
  }, { merge: true });
  batch.set(admin.firestore().collection('_users').doc(uid).collection('notifications').doc(), {
    content: 'payment',
    type: 'failed',
    createdAt: admin.firestore.FieldValue.serverTimestamp(),
  }, { merge: true });

  await batch.commit();

}

const webhook = async (request, response, next) => {
  try {
    const sig = request.headers['stripe-signature'];
    let event = await stripe.webhooks.constructEvent(request.body, sig, endpointSecret);

    if(event && event.type === 'customer.subscription.updated') {
      await subscriptionUpdated(event);
    }
    
    if(event && (event.type === 'subscription_schedule.canceled' || event.type === 'customer.subscription.deleted')) {
      await subscriptionCanceled(event);
    }

    if(event && event.type === 'payment_method.updated' || event.type === 'payment_method.attached') {
      await paymentMethodUpdated(event);
    }

    if(event && event.type === 'payment_method.detached') {
      await paymentMethodDetached(event);
    }

    if(event && event.type === 'invoice.paid') {
      await invoicePaid(event);
    }

    if(event && event.type === 'invoice.payment_failed') {
      await invoicePaidFail(event);
    }
    
    response.json({received: true});
  
  } catch (error) {
    response.status(401).json({ error });
  }
}

module.exports = {
  webhook
};
