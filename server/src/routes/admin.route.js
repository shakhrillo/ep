const express = require('express');
const auth = require('../middlewares/auth');
const adminController = require('../controllers/admin.controller');
const { uploadImage } = require('../controllers/upload.controller');
const router = express.Router();

// Earnings
router.route('/earnings/:id').get(auth('admin'), adminController.calculateEarningsOfAuthors);

// Statistics
router.route('/statistics').get(auth('admin'), adminController.getStatistics);

// MEMBERSHIP
router.route('/membership').get(auth('public', 'user'), adminController.getMembership);
router.route('/membership').post(auth('admin'), adminController.createMembership);
router.route('/membership/:id').delete(auth('admin'), adminController.deleteMembership);

// CATEGORIES
router.route('/categories').get(auth('public', 'user'), adminController.getCategories);
router.route('/category').post(auth('admin'), uploadImage.single('image'), adminController.createCategory);
router.route('/category/:id').delete(auth('admin'), adminController.deleteCategory);

// AUTHORS
router.route('/authors').get(auth('public', 'user'), adminController.getAuthors);
router.route('/authors-pending').get(auth('admin'), adminController.getPendingAuthors);

module.exports = router;