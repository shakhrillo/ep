const express = require('express');
const auth = require('../middlewares/auth');
const authorController = require('../controllers/author.controller');
const router = express.Router();
const { uploadImage } = require('../controllers/upload.controller');
var cpUpload = uploadImage.fields([{ name: 'taxDocument', maxCount: 1 }, { name: 'userIdentity', maxCount: 1 }])

// CREATE AUTHOR
router.route('/author').post(auth('user'), cpUpload, authorController.createAuthor);

// ACCEPT AUTHOR
router.route('/author/:uid').post(auth('user'), authorController.acceptAuthor);

// DELETE AUTHOR
router.route('/author/:uid').delete(auth('user'), authorController.rejectAuthor);

module.exports = router;