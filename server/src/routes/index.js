const express = require('express');
const userRoute = require('./user.route');
const authorRoute = require('./author.route');
const adminRoute = require('./admin.route');
const messagesRoute = require('./messages.route');
const subscriptionRoute = require('./subscription.route');
const courseRoute = require('./course.route');
const webhookRoute = require('./webhook.route');

const router = express.Router();
router.use('/', userRoute);
router.use('/', authorRoute);
router.use('/', adminRoute);
router.use('/', messagesRoute);
router.use('/', subscriptionRoute);
router.use('/', courseRoute);
router.use('/webhook', webhookRoute);

module.exports = router;
