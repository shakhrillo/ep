const express = require('express');
const router = express.Router();
const validate = require('../middlewares/validate');
const auth = require('../middlewares/auth');

const subscriptionController = require('../controllers/subscription.controller');
const { updateCard } = require('../validations/subscription.validation');

// PLANS
router.route('/subscription/plans').get(auth('user'), subscriptionController.getPlans);

// INVOICES
router.route('/subscription/invoices').get(auth('user'), subscriptionController.getInvoices);

// CHECKOUT
router.route('/subscription/checkout').post(auth('user'), subscriptionController.createCheckoutSession);

// CARD
router.route('/subscription/:id/card').put(validate(updateCard), auth('user'), subscriptionController.updateCard);

// RE-ACTIVATE SUBSCRIPTION
router.route('/subscription/:id').put(auth('user'), subscriptionController.reActivateSubscription);

// CANCEL SUBSCRIPTION
router.route('/subscription/:id').delete(auth('user'), subscriptionController.cancelSubscription);

module.exports = router;