const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const stripeWebhookController = require('../webhooks/stripe.webhook');

// WATCH STRIPE WEBHOOK
router.route('/stripe').post(bodyParser.raw({type: 'application/json'}), stripeWebhookController.webhook);

module.exports = router;