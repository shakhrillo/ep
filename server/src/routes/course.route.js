const express = require('express');
const auth = require('../middlewares/auth');
const courseController = require('../controllers/course.controller');
var aws = require('aws-sdk');
var multer = require('multer');
let upload = multer();
const { uploadImage } = require('../controllers/upload.controller');
var multerS3 = require('multer-s3');
const config = require('../config/config');
var s3 = new aws.S3({
  accessKeyId: config.aws.access_key_id,
  secretAccessKey: config.aws.secret_access_key
});
const router = express.Router();
const uploadVideo = multer({
  storage: multerS3({
    s3: s3,
    contentType: multerS3.AUTO_CONTENT_TYPE,
    bucket: config.aws.bucket_name,
    acl: 'public-read',
    metadata: function (req, file, cb) {
      cb(null, {fieldName: file.fieldname});
    },
    key: function (req, file, cb) {
      cb(null, Date.now().toString())
    }
  })
})

// PUBLISH COURSE
const courseUpload = uploadVideo.fields([{ name: 'course_resource', maxCount: 5 }, { name: 'course_image', maxCount: 1 }, { name: 'video_files', maxCount: 20 }]);
router.route('/course').post(auth('user'), courseUpload, courseController.submitCourse);

// GET COURSES
router.route('/courses').get(auth('user', 'public'), courseController.getAllActiveCourses)

// UPDATE COURSE
router.route('/course/:courseId').put(uploadImage.single('course_image'), auth('user'), courseController.updateCourse)

// PENDING COURSES
router.route('/courses-pending').get(auth('admin'), courseController.getAllPendingCourses);

// USER PENDING COURSES
router.route('/pending-courses/user').get(auth('user'), courseController.getPendingCoursesUser);

// CATEGORY COURSES
router.route('/category/:id/courses').get(auth('user', 'public'), courseController.getCategoryCourses);

// COMPLETE LESSONS
router.route('/courses/lesson/:courseId/:videoId').put(auth('user'), courseController.completeLesson);

// LESSON REVIEW
router.route('/rate/:courseId').post(auth('user'), courseController.giveCourseRate);
router.route('/rates/:courseId').get(auth('public', 'user'), courseController.getCourseAllRates);
router.route('/rates').get(auth('public', 'user'), courseController.getAllRates);

// ACCEPT COURSE
router.route('/courses/:id/accept').post(auth('admin'), courseController.acceptCourse);

// VIEW COURSE
router.route('/course/:id').get(auth('user', 'public'), courseController.getCourse);

// DELETE COURSE
router.route('/course/:id').delete(auth('user', 'admin'), courseController.deleteCourse);

// COURSE COMMENTS
router.route('/courses/lesson/:courseId/:videoId/comments').post(upload.fields([]), auth('user'), courseController.createCourseComment);
router.route('/courses/lesson/:courseId/:videoId/comments').get(auth('user','public'), courseController.getCourseComments);
router.route('/courses/lesson/:courseId/:videoId/comments/:id')
  .post(upload.fields([]), auth('user'), courseController.replyCourseComment)
  .put(upload.fields([]), auth('user'), courseController.editCourseComment)
  .get(auth('user','public'), courseController.getCourseComment)
  .delete(upload.fields([]), auth('user'), courseController.deleteCourseComment);
router.route('/courses/lesson/:courseId/:videoId/comments/:id/like').post(upload.fields([]), auth('user'), courseController.likeCourseComment);
router.route('/courses/lesson/:courseId/:videoId/comments/:id/unlike').post(upload.fields([]), auth('user'), courseController.unLikeCourseComment);

// UNCOMPLETED COURSES
router.route('/courses/watching').get(auth('user'), courseController.getUnCompletedCourses);

// COMPLETED COURSE
router.route('/courses/watched').get(auth('user'), courseController.getCompletedCourses);

// USER COURSES
router.route('/courses/:uid').get(auth('user', 'public'), courseController.getCoursesUser);

module.exports = router;