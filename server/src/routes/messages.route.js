const express = require('express');
const auth = require('../middlewares/auth');
let multer = require('multer');
let upload = multer();
const router = express.Router();
const messagesController = require('../controllers/messages.controller');

// CREATE MESSAGE
router.route('/message/:uid').post(upload.fields([]), auth('user'), messagesController.createMessage);

// READ MESSAGE
router.route('/message/:uid').put(upload.fields([]), auth('user'), messagesController.asRead);

module.exports = router;