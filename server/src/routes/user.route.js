const express = require('express');
const auth = require('../middlewares/auth');
const validate = require('../middlewares/validate');
const router = express.Router();
const { uploadImage } = require('../controllers/upload.controller');
const userController = require('../controllers/user.controller');
const { updateUser, updateAuthorPayoutEmail } = require('../validations/user.validation');

// UPDATE USER
router.route('/user').put(validate(updateUser), auth('user'), uploadImage.single('image'), userController.updateUser);
router.route('/user/payout-email').post(validate(updateAuthorPayoutEmail), auth('user'), userController.updateUserPayoutEmail);
router.route('/user/photo').delete(auth('user'), userController.deleteUserPhoto);

// GET USER SUBSCRIPTION
router.route('/user/subscription').get(auth('user'), userController.getUserSubscription);

// GET USERS
router.route('/users').get(userController.getUsers);

// GET USER
router.route('/user/:id')
  .delete(auth('admin'), userController.deleteUser)
  .get(userController.getUser);

// GET LANDING PAGE
router.route('/landing-page').get(auth('user', 'public'), userController.getLandingPageData);

// GET NOTIFICATIONS
router.route('/feeds').get(auth('user'), userController.getNotifications);

// UPDATE WATCH TIME
router.route('/watch_time/:author/:courseId').post(auth('user'), userController.initialWatchTime);
router.route('/watch_time/:author/:courseId').put(auth('user'), userController.updateWatchTime);

// GET EARNING
router.route('/earning/:id').get(auth('user'), userController.getUserEarning);

module.exports = router;