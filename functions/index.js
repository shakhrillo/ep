const functions = require('firebase-functions');
const stripe = require('stripe')('sk_test_JkJ81uFJzAmzz5yBKkgwpC9e');
const admin = require('firebase-admin');
var AES = require("crypto-js/aes");

admin.initializeApp();

// On create new user
exports.createdNewUser = functions.auth.user().onCreate(async ({email, displayName, uid}) => {
  const userRef = admin.firestore().collection('_users').doc(uid);
  const messagesCollection = userRef.collection('messages');
  const currentTime = admin.firestore.FieldValue.serverTimestamp();
  const statisticsId = new Date().getMonth() + '_' + new Date().getFullYear();
  const statisticsRef = admin.firestore().collection('_statistics').doc(statisticsId);

  const welcomeMessage = 'Thank you for registering! If there any questions feel free to ask. Best ePlatform team';
  const encryptedMsg = AES.encrypt(welcomeMessage, "**").toString();

  if(uid !== 'admin'){
    const { id } = await stripe.customers.create({ email });
    
    const batch = admin.firestore().batch();
    batch.set(userRef, {
      email, 
      displayName, 
      uid, 
      role: 'user', 
      customerId: id, 
      createdAt: currentTime
    });

    // Send message to new user
    batch.set(messagesCollection.doc('admin'), {
      creator: 'admin',
      receiver: uid,
      message: encryptedMsg,
      created: currentTime,
      unreads: admin.firestore.FieldValue.increment(1),
      id: 'admin'
    });
    batch.set(messagesCollection.doc('admin').collection('messages').doc(), {
      creator: 'admin',
      receiver: uid,
      message: encryptedMsg,
      created: currentTime
    });

    // Set unreads to reciever
    batch.set(userRef.collection('alerts').doc('data'), { messages: admin.firestore.FieldValue.increment(1) }, { merge: true });

    // Update statistics
    batch.set(statisticsRef, { registered_users: admin.firestore.FieldValue.increment(1) }, { merge: true });
  
    await batch.commit();
  }

  return { status: 'OK' }
});

// Update offline&online status
exports.onUserStatusChanged = functions.database.ref('/status/{uid}').onUpdate(
  async (change, context) => {
    const eventStatus = change.after.val();
    const ref = admin.firestore().collection('_users').doc(context.params.uid);
    const statusSnapshot = await change.after.ref.once('value');
    const status = statusSnapshot.val();
    if (status.last_changed > eventStatus.last_changed) {
      return null;
    }
    await ref.set(eventStatus, {merge: true});
    return { status: 'OK' }
  }
);